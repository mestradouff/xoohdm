package fp.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.infra.core.services.ServiceException;
import org.eclipse.papyrus.infra.services.openelement.service.OpenElementService;
import org.eclipse.papyrus.infra.ui.editor.IMultiDiagramEditor;
import org.eclipse.papyrus.infra.ui.util.EditorUtils;
import org.eclipse.ui.PartInitException;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;

public class DiagramOOHDMUtil {
	
	private UmlUtil umlUtil = new UmlUtil();
	
	public void openDiagram(IMultiDiagramEditor editor, Element semanticElement) {
		OpenElementService openService;
		try {
			openService = editor.getServicesRegistry().getService(OpenElementService.class);
			//openService.openElement(viewElement); //If you already have the representation (notation::View)
			openService.openSemanticElement(semanticElement); //If you only know the semantic (UML) element
		} catch (ServiceException | PartInitException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void openNavigationalDiagram(Element semanticElement) {
		//List<Diagram> lista = this.getDiagrams();
		IMultiDiagramEditor editor = EditorUtils.getMultiDiagramEditor();
		this.openDiagram(editor, semanticElement);
	}

	
	public void openNavigationalDiagram(EObject eObject) {
		//List<Diagram> lista = this.getDiagrams();
		Model mNav = (Model) umlUtil.createOrSelectModelElement("Navigational", eObject);
		IMultiDiagramEditor editor = EditorUtils.getMultiDiagramEditor();
		this.openDiagram(editor, mNav);
	}
}
