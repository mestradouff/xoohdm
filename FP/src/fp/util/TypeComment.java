package fp.util;

public enum TypeComment {
	
	PARAMETERS(1,"parameters"), ELEMENTS(2,"elements"),	ATTRIBUTS(3,"attributs"),
	SORT(4,"sort"), NAVIGATION(5,"navigation"), OBJECTS(6,"objects"), TESTS(7,"tests"), ANCHORS(8,"anchors");
	
	public int value;
	public String strValue;
	
	TypeComment(int value,String str){
		this.value=value;
		this.strValue = str;
	}
	
	public int getValue() {
		return value;
	}
	
	public String getName() {
		return strValue;
	}
	
	public int getValueByName(String name) {
		return getEnumByString(name);
	}
	
	public static Integer getEnumByString(String codeName){
        for(TypeComment e : TypeComment.values()){
            if(codeName.equals(e.strValue.toLowerCase())) return e.value;
        }
       return null;
	
    }

}
