package fp.util;

import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.papyrus.uml.alf.transaction.commit.CommitScenario;
import org.eclipse.papyrus.uml.alf.transaction.job.AlfCompilationJob;
import org.eclipse.uml2.uml.NamedElement;

public class CommitScenarioOOHDM extends CommitScenario{

	private boolean isCommitRequired;
	
	public CommitScenarioOOHDM() {
		super();
		this.isCommitRequired = true;
		// TODO Auto-generated constructor stub
	}

	/**
	 * Propagate the model state specified in text within the model
	 * 
	 * @param - the model element to update with the specified changes
	 */
	@Override
	public void execute(NamedElement target, final String lastEditedVersion) {
		/* 1. Load the states of the target */
		this.init(target);
		if (!this.userModelState.getContent().equals(lastEditedVersion)) {
			this.userModelState.setText(lastEditedVersion);
		}
		/* 2. Realize before actions */
		this.before();
		if (this.isCommitRequired) {
			/* 3.1. Schedule a job in charge of propagated the changes in the model */
			Job job = new AlfCompilationJob(this.modelStateToBeCommitted);
			job.setPriority(Job.BUILD);
			job.addJobChangeListener(new JobChangeAdapter() {
				@Override
				public void done(IJobChangeEvent event) {
					CommitScenarioOOHDM.this.after();
				}
			});
			job.schedule();
		} else {
			this.isCommitRequired = true;
		}
	}
	
}
