package fp.util;

import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.papyrus.uml.alf.transaction.commit.SaveScenario;
import org.eclipse.papyrus.uml.alf.transaction.job.SaveTextualRepresentationJob;
import org.eclipse.uml2.uml.NamedElement;

public class SaveScenarioOOHDM extends SaveScenario{
	
	private boolean isBackupRequired;

	public SaveScenarioOOHDM() {
		super();
		this.isBackupRequired = true;
	}
	
	/**
	 * Persist in the model the state of the target as a comment
	 * 
	 * @param target
	 *            - the model element state to persist
	 */
	@Override
	public void execute(NamedElement target, final String lastEditedVersion) {
		/* 1. Load the states of the target */
		this.init(target);
		if (!this.userModelState.getContent().equals(lastEditedVersion)) {
			this.userModelState.setText(lastEditedVersion);
		}
		/* 2. Realize before actions */
		this.before();
		/* 3. Is a backup required */
		if (this.isBackupRequired) {
			/* 3.1. Schedule a job in charge of saving target state */
			Job job = new SaveTextualRepresentationJob(this.modelStateToBeCommitted);
			job.setPriority(Job.SHORT);
			job.addJobChangeListener(new JobChangeAdapter() {
				@Override
				public void done(IJobChangeEvent event) {
					SaveScenarioOOHDM.this.after();
				}
			});
			job.schedule();
		} else {
			this.isBackupRequired = true;
		}
	}


}
