package fp.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.infra.core.resource.NotFoundException;
import org.eclipse.papyrus.uml.tools.model.UmlUtils;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.UMLFactory;

public class CommentUtil {

	private EObject eObject;
	public CommentUtil(EObject eObject) {
		this.eObject = eObject;
	}
	
	private EObject getEObject(int i) {
		
//		if(i == TypeComment.OBJECTS.getValue()) {
//			try {
//				return UmlUtils.getUmlModel().lookupRoot();
//			} catch (NotFoundException e) {
//				return null;
//			}
//			
//		}else {
			return this.eObject;
//		}
		
		
	}
	
	public EList<Comment> getComments(int value) {
		return ((Element) getEObject(value)).getOwnedComments();
	}
	public EList<Comment> getComments(int value, EObject eobject) {
		return ((Element) eobject).getOwnedComments();
	}

	public void removeComment(int value) {

		Iterator<Comment> it = getComments(value).iterator();
		List<Comment> aux = new ArrayList<Comment>();
		while (it.hasNext()) {
			Comment co = it.next();

			if ((co.getBody().split("://")[0].equals("parameters")) && (value == TypeComment.PARAMETERS.getValue())) { 
				aux.add(co);
			}
			if ((co.getBody().split("://")[0].equals("elements")) && (value == TypeComment.ELEMENTS.getValue())) { 
				aux.add(co);
			}
			if ((co.getBody().split("://")[0].equals("attributs")) && (value == TypeComment.ATTRIBUTS.getValue())) {
				aux.add(co);
			}
			if ((co.getBody().split("://")[0].equals("sort")) && (value == TypeComment.SORT.getValue())) {
				aux.add(co);
			}
			if ((co.getBody().split("://")[0].equals("navigation")) && (value == TypeComment.NAVIGATION.getValue())) {
				aux.add(co);
			}
			if ((co.getBody().split("://")[0].equals("objects")) && (value == TypeComment.OBJECTS.getValue())) {
				aux.add(co);
				((Element) getEObject(TypeComment.OBJECTS.getValue())).getOwnedComments().removeAll(aux);
				return;
			}
			if ((co.getBody().split("://")[0].equals("tests")) && (value == TypeComment.TESTS.getValue())) { 
				aux.add(co);
			}
			if ((co.getBody().split("://")[0].equals("anchors")) && (value == TypeComment.ANCHORS.getValue())) { 
				aux.add(co);
			}
		}

		((Element) getEObject(0)).getOwnedComments().removeAll(aux);

	}
	
	public void removeComment(int value,Element element) {

		Iterator<Comment> it = getComments(value,element).iterator();
		List<Comment> aux = new ArrayList<Comment>();
		while (it.hasNext()) {
			Comment co = it.next();

			if ((co.getBody().split("://")[0].equals("parameters")) && (value == TypeComment.PARAMETERS.getValue())) { 
				aux.add(co);
			}
			if ((co.getBody().split("://")[0].equals("elements")) && (value == TypeComment.ELEMENTS.getValue())) { 
				aux.add(co);
			}
			if ((co.getBody().split("://")[0].equals("attributs")) && (value == TypeComment.ATTRIBUTS.getValue())) {
				aux.add(co);
			}
			if ((co.getBody().split("://")[0].equals("sort")) && (value == TypeComment.SORT.getValue())) {
				aux.add(co);
			}
			if ((co.getBody().split("://")[0].equals("navigation")) && (value == TypeComment.NAVIGATION.getValue())) {
				aux.add(co);
			}
			if ((co.getBody().split("://")[0].equals("objects")) && (value == TypeComment.OBJECTS.getValue())) {
				aux.add(co);
				(element).getOwnedComments().removeAll(aux);
				return;
			}
			if ((co.getBody().split("://")[0].equals("tests")) && (value == TypeComment.TESTS.getValue())) { 
				aux.add(co);
			}
			if ((co.getBody().split("://")[0].equals("anchors")) && (value == TypeComment.ANCHORS.getValue())) { 
				aux.add(co);
			}
		}

		(element).getOwnedComments().removeAll(aux);

	}
	
	public void saveCommentModel(String text, int typeComment) {
		List<Comment> commentList = new ArrayList<Comment>();

		Comment comentElementos2 = UMLFactory.eINSTANCE.createComment();
		removeComment(typeComment);
		comentElementos2.setBody(compileComment(text, typeComment));
		commentList.add(comentElementos2);
		((Element) getEObject(typeComment)).getOwnedComments().add(comentElementos2);
	}
	
	public void saveCommentModel(String text, int typeComment,Element element) {
		List<Comment> commentList = new ArrayList<Comment>();

		Comment comentElementos2 = UMLFactory.eINSTANCE.createComment();
	//removeComment(typeComment);
		removeComment(typeComment,element);
		comentElementos2.setBody(compileComment(text, typeComment));
		commentList.add(comentElementos2);
		(element).getOwnedComments().add(comentElementos2);
	}
	
	
	public String resolvComment(String str) {

		String ret = "";
		// switch (typeComment) {
		// case 1:
		// TypeComment.PARAMETERS.getValue()
		String aux[] = str.split("://");
		if (aux[0] == TypeComment.PARAMETERS.getName()) {// TODO criar constante
			ret = aux[1];
		} else
		// break;

		// case 2:
		// TypeComment.ELEMENTS.getValue()

		// String aux2[] = str.split("://");
		if (aux[0] == TypeComment.ELEMENTS.getName()) {
			ret = aux[1];
		}

		if (aux[0] == TypeComment.ATTRIBUTS.getName()) {
			ret = aux[1];
		}

		if (aux[0] == TypeComment.SORT.getName()) {
			ret = aux[1];
		}
		if (aux[0] == TypeComment.NAVIGATION.getName()) {
			ret = aux[1];
		}
		if (aux[0] == TypeComment.OBJECTS.getName()) {
			ret = aux[1];
		}
		if (aux[0] == TypeComment.TESTS.getName()) {
			ret = aux[1];
		}
		if (aux[0] == TypeComment.ANCHORS.getName()) {
			ret = aux[1];
		}
		// break;
		// }

		return ret;
	}
	
	public String getCommentType(String typeComment) {
		
		try {
			for (Comment co : getComments(TypeComment.getEnumByString(typeComment))) {

				String aux[] = co.getBody().split("://");
				if (aux[0].equals(typeComment)) {// TODO criar constante
					if (aux.length == 2)
						return aux[1];
				}

			}
		} catch (Exception e) {
			return null;
		}
		
		return null;

	}
	
	public String getCommentType(String typeComment, EObject eobject) {
		
		try {
			for (Comment co : getComments(TypeComment.getEnumByString(typeComment),eobject)) {

				String aux[] = co.getBody().split("://");
				if (aux[0].equals(typeComment)) {// TODO criar constante
					if (aux.length == 2)
						return aux[1];
				}

			}
		} catch (Exception e) {
			return null;
		}
		
		return null;

	}

	
	public String compileComment(String str, int typeComment) {

		String ret = "";
		String postFix = "://";
		switch (typeComment) {
		case 1:
			// TypeComment.PARAMETERS.getValue()
			String aux = TypeComment.PARAMETERS.getName()+postFix;
			ret = aux + str;
			break;

		case 2:
			// TypeComment.ELEMENTS.getValue()

			String aux2 = TypeComment.ELEMENTS.getName()+postFix;
			ret = aux2 + str;
			break;

		case 3:
			// TypeComment.ELEMENTS.getValue()

			String aux3 = TypeComment.ATTRIBUTS.getName()+postFix;
			ret = aux3 + str;
			break;
		case 4:
			// TypeComment.SORT.getValue()

			String aux4 = TypeComment.SORT.getName()+postFix;
			ret = aux4 + str;
			break;

		case 5:
			// TypeComment.NAVIGATION.getValue()

			String aux5 = TypeComment.NAVIGATION.getName()+postFix;
			ret = aux5 + str;
			break;
		case 6:
			// TypeComment.OBJECTS.getValue()

			String aux6 = TypeComment.OBJECTS.getName()+postFix;
			ret = aux6 + str;
			break;
		case 7:
			// TypeComment.TESTS.getValue()

			String aux7 = TypeComment.TESTS.getName()+postFix;
			ret = aux7 + str;
			break;
		
	case 8:
		// TypeComment.TESTS.getValue()

		String aux8 = TypeComment.ANCHORS.getName()+postFix;
		ret = aux8 + str;
		break;
	}

		return ret;
	}
	
	
	
}
