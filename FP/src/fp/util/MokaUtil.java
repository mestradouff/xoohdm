package fp.util;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.ui.DebugUITools;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.papyrus.moka.utils.constants.MokaConstants;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.TextConsole;
import org.eclipse.uml2.uml.Element;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

import fp.propertySection.TestExecutionPropertySection;

public class MokaUtil{

	private TextConsole[] console;
	private IConsoleManager manager;
	public void clearConsole(String name) {

	      ConsolePlugin plugin = ConsolePlugin.getDefault();
	      IConsoleManager conMan = plugin.getConsoleManager();

	      IConsole[] existing = conMan.getConsoles();
	      //if console exists, clear it 
	      for (int i = 0; i < existing.length; i++)
	          if (name.equals(existing[i].getName())){
	        	  
	              ((org.eclipse.ui.console.IOConsole) existing[i]).clearConsole(); //this is the important part
	          }
	   }
	
	public String getTextConsole(String name) {

	      ConsolePlugin plugin = ConsolePlugin.getDefault();
	      IConsoleManager conMan = plugin.getConsoleManager();

	      IConsole[] existing = conMan.getConsoles();
	      //if console exists, clear it 
	      for (int i = 0; i < existing.length; i++)
	          if (name.equals(existing[i].getName())){
	        	  
	             return ((org.eclipse.ui.console.IOConsole) existing[i]).getDocument().get();
//	              console[0] = ((TextConsole) existing[0]);
//	      		
//	      		return console[0].getDocument().get();
	          }
	  	return "";
	   }
	


	
	public synchronized void executeMoka() {
		clearConsole("fUML Console");
		
		
	    	IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
	 		IEditorPart editor = page.getActiveEditor();
	 		page.saveEditor(editor, false /* confirm */);
	    
		

		ILaunchManager manager = DebugPlugin.getDefault().getLaunchManager();

		ILaunchConfigurationType type = manager
				.getLaunchConfigurationType("org.eclipse.papyrus.moka.launchConfiguration");// TODO criar contante
		ILaunchConfiguration[] lcs;
		try {
			lcs = manager.getLaunchConfigurations(type);
			for (ILaunchConfiguration iLaunchConfiguration : lcs) {
				if (iLaunchConfiguration.getName().equals("xOOHDMTest")) {// TODO criar constante
					ILaunchConfigurationWorkingCopy t = iLaunchConfiguration.getWorkingCopy();
					ILaunchConfiguration config = t.doSave();
					if (config != null) {
						 //config.launch(ILaunchManager.RUN_MODE, null);
						DebugUITools.launch(config, ILaunchManager.RUN_MODE);
					}
				}
			}
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("passou 4");
	}
	public synchronized String updateConsole() {
		return updateConsole(true);
	}
	public synchronized String updateConsole(boolean dly) {

//		//
		if(dly) {
			delay(2000);
			waitForJobs();
		}
		

		manager = ConsolePlugin.getDefault().getConsoleManager();
		
		console = new TextConsole[1];
		if (console == null || console.length == 0 || console[0] == null) {
			//return "Ver Console View";
		}
	
		IConsole[] existing = manager.getConsoles();
		console[0] = ((TextConsole) existing[0]);
		
		String output = console[0].getDocument().get();
		
//		while(output == null || output.equals("")) {
//			delay(100);
//			output = getTextConsole("fUML Console");
//		}
//		System.out.println("passou e pegou o console.");
//		output = "{\r\n" + 
//				"    \"name\": \"professor por area\",\r\n" + 
//				"    \"idx\": [\r\n" + 
//				"		{\r\n" + 
//				"		\"anchors\":[\r\n" + 
//				"			{\r\n" + 
//				"				\"anchorName\": \"Jo�o Paulo\",\r\n" + 
//				"				\"target\": \"ctx por area\"\r\n" + 
//				"			},\r\n" + 
//				"			{\r\n" + 
//				"				\"anchorName\": \"Mestre\",\r\n" + 
//				"				\"target\": \"\"\r\n" + 
//				"			}\r\n" + 
//				"			]\r\n" + 
//				"		},\r\n" + 
//				"		{\r\n" + 
//				"		\"anchors\":[\r\n" + 
//				"			{\r\n" + 
//				"				\"anchorName\": \"Patrick\",\r\n" + 
//				"				\"target\": \"ctx por area\"\r\n" + 
//				"			},\r\n" + 
//				"			{\r\n" + 
//				"				\"anchorName\": \"Mestre\",\r\n" + 
//				"				\"target\": \"teste\"\r\n" + 
//				"			}\r\n" + 
//				"			]\r\n" + 
//				"		}\r\n" + 
//				"    ]\r\n" + 
//				"}";
		
		
		
//		JSONObject obj = new JSONObject(output);
//		String n = obj.getString("name");
		
//		JSONArray arr = obj.getJSONArray("idx");
//		for (int i = 0; i < arr.length(); i++) {
//		    System.out.println(arr.getString(i));
//		    JSONObject obj2 = new JSONObject(arr.getString(i));
//		    JSONArray arr2 = obj2.getJSONArray("anchors");
//			for (int i2 = 0; i2 < arr2.length(); i2++) {
//				 System.out.println(arr2.getString(i));
//			}
//		}
		
		
		
//		System.out.println(output);
//		int j = 0;
//		for (j = 0; j < existing.length; j++) {
//		    try {
//		    	TextConsole myconsole=(TextConsole)existing[j];
//		        System.out.println(myconsole.getDocument().get());
//		    } catch(Exception exc) {
//		        exc.printStackTrace();
//		    }
//		}
		
		return output;

	}
	
	public IConsole getConsole(String consoleName, IConsoleManager conMan) {
		IConsole[] existing = conMan.getConsoles();
		MessageConsole messageConsole = null;
		for (int i = 0; i < existing.length; i++) {
			if (existing[i].getName().equals(consoleName)) {
				messageConsole = (MessageConsole) existing[i];

				return messageConsole;
			}
		}
		return null;
	}
	
	private void delay(long durationInMilliseconds) {
		Display display = Display.getCurrent();
		if (display != null) {
			long t2 = System.currentTimeMillis() + durationInMilliseconds;
			while (System.currentTimeMillis() < t2) {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			}
			display.update();
		} else {
			try {
				Thread.sleep(durationInMilliseconds);
			} catch (InterruptedException e) {
			}
		}
	}

	@SuppressWarnings("deprecation")
	private void waitForJobs() {
		while (Platform.getJobManager().currentJob() != null)
			delay(1000);
	}
	
	private void executeMokaExecuteJob( TestExecutionPropertySection idx, Composite cp,boolean isIndex) {

try {
			
//			new Thread(new Runnable() {
//				public void run() {
//
//					Display.getDefault().syncExec(new Runnable() {
//						public void run() {
							MokaUtil mu = new MokaUtil();
							MokaConstants.MOKA_AUTOMATIC_ANIMATION = false;
							mu.executeMoka();
							
						    	 String resultJson = mu.updateConsole(true);	
								if(isIndex) {						
								    	 idx.gerarNovoIndice(resultJson, cp);
								}
//						}
//
//					});

//				}
//			}).start();
			
			
		     
			
			
		} catch (Exception e) {
			e.printStackTrace();
			
		}
//		MokaExecuteJob sJob = new MokaExecuteJob(tEditDomain,idx, cp, isIndex);
//		sJob.setPriority(Job.BUILD);
//		sJob.schedule();
		
		
	}
	
	public void executeMokaExecuteJobWithNewIndex( TestExecutionPropertySection idx, Composite cp) {

		
		executeMokaExecuteJob(idx, cp, true);
		
	}
	
}
