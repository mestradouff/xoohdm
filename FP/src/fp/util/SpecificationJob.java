package fp.util;

import javax.inject.Inject;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.uml2.uml.Element;

public class SpecificationJob extends Job {

	public static final String NAME = "Specification";

	private TransactionalEditingDomain ted;
	private Element classGerente;
	private Element opGerente; 
	private UmlUtil uutil = new UmlUtil();
	@Inject Shell shell;
	
	public SpecificationJob(TransactionalEditingDomain tEditDomain, Element classGerente, Element opGerente) {
		super(NAME);
		this.ted = tEditDomain;
		this.classGerente = classGerente;
		this.opGerente = opGerente;
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		IStatus jobStatus = Status.OK_STATUS;
		TransactionalEditingDomain domain = this.ted;
		if (domain != null) {
			/* Protect the resource in case of concurrent jobs */
//			Resource resource = this.modelElementState.getOwner().eResource();
//			synchronized (resource) {
				/* 1. Do not listen to modifications that occur on the resource during compilation */
//			IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
//			IEditorPart editor = page.getActiveEditor();
//			page.saveEditor(editor, false /* confirm */);
//				resource.setTrackingModification(false);
				/* 2. Do compilation phase */
				try {
//					domain.getCommandStack().execute(new RecordingCommand(domain) {
//						
//						@Override
//						protected void doExecute() {
							//DESCOMENTAR
							uutil.setSpecification(classGerente, opGerente, domain) ;
//							}
//					});
//						
				} catch (Exception e) {
					MessageBox messageDialog = new MessageBox(shell, SWT.ERROR);
					messageDialog.setText("Erro");
					messageDialog.setMessage("Problemas durante o Set Specification!!!");
					messageDialog.open();
					e.printStackTrace();
					jobStatus = Status.CANCEL_STATUS;
				}
				/* 3. Save the textual representation within the model */
//				if (jobStatus.equals(Status.OK_STATUS)) {
//					this.modelElementState.setText(new DefaultEditStringRetrievalStrategy().getGeneratedEditString(this.modelElementState.getOwner()));
//					/* 3. Execute the commands */
//					try {
//						domain.getCommandStack().execute(AlfCommandFactory.getInstance().creatSaveCommand(this.modelElementState));
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//				}
				/* 4. Restore the modification tracking */
//				resource.setTrackingModification(true);
//			}
		} else {
			jobStatus = Status.CANCEL_STATUS;
		}
		return jobStatus;
	}

}
