package fp.util;

public class Parameter {
	
	private int order;
	private String param;
	private String classParam;
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public String getParam() {
		return param;
	}
	public void setParam(String param) {
		this.param = param;
	}
	public String getClassParam() {
		return classParam;
	}
	public void setClassParam(String classParam) {
		this.classParam = classParam;
	}
	
	
}
