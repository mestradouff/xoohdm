package fp.util;

import org.eclipse.papyrus.uml.alf.transaction.commit.IChangeScenario;

public class ScenarioFactoryOOHDM {

	private static ScenarioFactoryOOHDM INSTANCE;

	private ScenarioFactoryOOHDM() {
	}

	public static ScenarioFactoryOOHDM getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new ScenarioFactoryOOHDM();
		}
		return INSTANCE;
	}

	public IChangeScenario createSaveScenario() {
		return new SaveScenarioOOHDM();
	}

	public IChangeScenario createCommitScenario() {
		return new CommitScenarioOOHDM();
	}

//	public ISyncScenario createSyncScenario() {
//		return new SyncScenario();
//	}
//	
}
