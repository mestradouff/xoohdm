package fp.util;

import org.eclipse.papyrus.uml.alf.transaction.commit.ScenarioFactory;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;

import fp.propertySection.AlfEditionPropertySection;

public class CommitButtonSelectionListener  extends SelectionAdapter {


	private AlfEditionPropertySection propertySection;

	public CommitButtonSelectionListener(AlfEditionPropertySection propertySection) {
		this.propertySection = propertySection;
	}

	/**
	 * @see org.eclipse.swt.events.SelectionListener#widgetSelected(org.eclipse.swt.events.SelectionEvent)
	 */
	public void widgetSelected(SelectionEvent event) {
		/* 1. Retrieved the current model element */
		
		Element ele = (Element) this.propertySection.getContextObject();
		NamedElement semanticObject = (NamedElement) ele;
		
		/* 2. Retrieved its currently edited representation */
		StyledText editor = this.propertySection.getEditor();
		String txt = editor.getText();
		System.out.println(txt);
		/* 3. Compile without blocking URI */
		if (semanticObject != null && editor != null) {
			ScenarioFactory.getInstance().createCommitScenario().execute(semanticObject, txt);
		}
	}
}
