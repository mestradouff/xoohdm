package fp.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.papyrus.infra.core.resource.NotFoundException;
import org.eclipse.papyrus.moka.composites.utils.handlers.Utils;
import org.eclipse.papyrus.uml.alf.text.representation.AlfTextualRepresentation;
import org.eclipse.papyrus.uml.alf.transaction.commit.ScenarioFactory;
import org.eclipse.papyrus.uml.alf.transaction.job.AlfCompilationJob;
import org.eclipse.papyrus.uml.alf.ui.internal.AlfActivator;
import org.eclipse.papyrus.uml.alf.validation.ModelNamespaceFacade;
import org.eclipse.papyrus.uml.tools.model.UmlUtils;
import org.eclipse.papyrus.uml.xtext.integration.StyledTextXtextAdapter;
import org.eclipse.papyrus.uml.xtext.integration.core.ContextElementAdapter;
import org.eclipse.papyrus.uml.xtext.integration.core.ContextElementAdapter.IContextElementProvider;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.ResourceUtil;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.LiteralUnlimitedNatural;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.ParameterDirectionKind;
import org.eclipse.uml2.uml.ParameterEffectKind;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.VisibilityKind;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.xbase.lib.StringExtensions;

import com.google.inject.Injector;

public class UmlUtil extends UmlUtils implements IContextElementProvider {

	private StyledTextXtextAdapter styledTextAdapter;
	private StyledText textControl;
	private EObject eObject;
	private Injector alfToolingInjector;

	final private ContextElementAdapter contextElementAdapter = new ContextElementAdapter(this);
	
	
	public void recursoAlfXtext(Element ele) {
//		text.setText(alfCode);
		updateXtextAdapters( ele);
	}
	
	public void updateXtextAdapters(Element element) {
		//fundamental
		this.eObject = (EObject) element;
		
		
//		this.alfToolingInjector = AlfActivator.getInstance().getInjector(AlfActivator.ORG_ECLIPSE_PAPYRUS_UML_ALF_ALF);
//		
//		if (styledTextAdapter != null) {
//			styledTextAdapter.getFakeResourceContext().getFakeResource().eAdapters().remove(contextElementAdapter);
//		}
//		styledTextAdapter = new StyledTextXtextAdapter(this.alfToolingInjector);
//		styledTextAdapter.getFakeResourceContext().getFakeResource().eAdapters().add(contextElementAdapter);
		this.installValidationContextFor((Element)this.eObject);
//		styledTextAdapter.adapt((StyledText) styledText);
	}

	private void installValidationContextFor(Element element) {
		
		
//		if (this.styledTextAdapter != null) {
//			XtextResource resource = this.styledTextAdapter.getFakeResourceContext().getFakeResource();
//			if (resource != null) {
//				ModelNamespaceFacade.getInstance().createValidationContext(resource, this.getNamespace(element));
//			}
//		}
//		ResourceSet resourceSet = element.eResource().getResourceSet();
//		String str = ResourceUtil.getResource(element).getLocationURI().getPath();
//		URI modelUri = URI.createURI(str); // TODO criar constante
//		Resource modelResource = resourceSet.getResource(modelUri, true);
		//(Resource)ResourceUtil.getResource(element).getLocationURI()
		if(element.eResource() == null) return;
		
		ModelNamespaceFacade.getInstance().createValidationContext(element.eResource(), this.getNamespace(element));
//		try {
//			//Thread.sleep(100);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
	
	private Namespace getNamespace(Element element){
		if(element!=null && element instanceof NamedElement){
			return ((NamedElement)element).getNamespace();
		}
		return null;
	}
	
	public Element createOrSelectModelElement(String modelName, EObject rootEObject) {
		return this.createOrSelectModelElement(modelName,rootEObject,null);
	}
	
	public Element createOrSelectModelElement(String modelName, EObject rootEObject,List<Element> daoClasses) {
		Element e1 = ((Element) rootEObject).getModel().getOwner();
		List<Element> elementos = e1.getOwnedElements();
		List<Element> elementos2 = new ArrayList<Element>();
		for (Element element : elementos) {
			if (element.getModel().getName().equals(modelName)) { // TODO criar constante
				if (element instanceof org.eclipse.uml2.uml.Model) {
					return element;
				}
			} else if (element instanceof Model) {
				elementos2.add(element);
			}
		}

		Model mGen = UMLFactory.eINSTANCE.createModel();
		mGen.setName(modelName);
		if(daoClasses != null) {
			//mGen.allOwnedElements().addAll(daoClasses);
			mGen.eSet(mGen.eClass().getEStructuralFeature("packagedElement"), daoClasses);
		}
		
		elementos2.add(mGen);

		Element ele = ((Element) rootEObject).getModel().getOwner();
		if (ele.getModel().getName().equals(getNameRootModel())) {
			ele.eSet(ele.eClass().getEStructuralFeature("packagedElement"), elementos2);
		} else {
			// ((Element)
			// getEObject()).getModel().getOwner().getModel().getOwner().eSet(((Element)
			// getEObject()).getModel().getOwner().eClass().getEStructuralFeature("packagedElement"),elementos2);
		}
		return mGen;
	}

	public String getNameRootModel() {
		EObject model = null;
		try {
			model = UmlUtils.getUmlModel().lookupRoot();
			return ((Element) model).getModel().getName();
		} catch (NotFoundException e) {
			e.printStackTrace();
		}

		return "";
	}

	public Element generateClass(Element mGenerate, String name) {

		List<Element> elementos2 = new ArrayList<Element>();
		if (mGenerate.getOwnedElements() != null) {
			for (Element element : mGenerate.getOwnedElements()) {
				if (element instanceof org.eclipse.uml2.uml.Class) {
					if (((Class) element).getName().equals(name)) { // TODO criar constante
						return element;
					} else/* if(element instanceof Model) */ {
						elementos2.add(element);
					}
				} // else/* if(element instanceof Model) */ {
					// elementos2.add(element);
					// }
			}
		}
		org.eclipse.uml2.uml.Class cGen = UMLFactory.eINSTANCE.createClass();
		cGen.setName(name);
		elementos2.add(cGen);

		mGenerate.eSet(mGenerate.eClass().getEStructuralFeature("packagedElement"), elementos2);


		return cGen;
	}

	public Element generateOperation(Element cGerente, Class clasTypeRet, String params, EObject eObjectSelected,
			String name, boolean isMultivalored) {
		
		if (name.equals("") || name == null) {
			if(eObjectSelected instanceof Class) {
				name = "gerar" + ((Class) eObjectSelected).getName();// TODO criar constante
			}else if(eObjectSelected instanceof org.eclipse.uml2.uml.Property) {
				name = "gerar" + ((Property) eObjectSelected).getName();// TODO criar constante
			}
		}
		
		
		
		
		List<Element> elementos2 = new ArrayList<Element>();
		for (Element element : cGerente.getOwnedElements()) {
			if (element instanceof org.eclipse.uml2.uml.Operation) {
				if (((Operation) element).getName().equals(name)) {
					element = (Operation) setParametersOperation((Operation) element, clasTypeRet, params,
							eObjectSelected, isMultivalored);
					return element; // TODO quando existir verificar se h� mudan�a nos parametros.
				} else/* if(element instanceof Model) */ {
					elementos2.add(element);
				}
			}
		}

		org.eclipse.uml2.uml.Operation opGen = UMLFactory.eINSTANCE.createOperation();
		opGen.setName(name);

		opGen = setParametersOperation(opGen, clasTypeRet, params, eObjectSelected, isMultivalored);

		elementos2.add(opGen);
		cGerente.eSet(cGerente.eClass().getEStructuralFeature("ownedOperation"), elementos2);

		return opGen;
	}

	private Operation setParametersOperation(Operation opGen, Class clasTypeRet, String params, EObject eObjectSelected,
			boolean isMultivalored) {

		if (clasTypeRet != null) {
			opGen.getOwnedParameters().clear();
			org.eclipse.uml2.uml.Parameter opr = UMLFactory.eINSTANCE.createParameter();
			opr.setName("ret");
			opr.setVisibility(VisibilityKind.PUBLIC_LITERAL);
			opr.setDirection(ParameterDirectionKind.RETURN_LITERAL);
			opr.setEffect(ParameterEffectKind.CREATE_LITERAL);
			Type tp = clasTypeRet;
			opr.setType(tp);
			if (isMultivalored) {
				// multivalorado
				opr.setLower(0);
				opr.setUpper(LiteralUnlimitedNatural.UNLIMITED);
				
			}else {
				opr.setLower(1);
				opr.setUpper(1);
			}
			opGen.getOwnedParameters().add(opr);
		}

		// --

		org.eclipse.uml2.uml.Parameter opIn = UMLFactory.eINSTANCE.createParameter();
		Parameter p = new Parameter();
		// opIn.setName("listObjects");
		// opIn.setDirection(ParameterDirectionKind.IN_LITERAL);
		// opIn.setEffect(ParameterEffectKind.CREATE_LITERAL);
		// opIn.setVisibility(VisibilityKind.PUBLIC_LITERAL);
		// opIn.setIsUnique(true);
		// opIn.setIsException(false);
		// opIn.setIsOrdered(false);
		// opIn.setIsStream(false);
		// opIn.setUpper(1);
		Type t = getClassStringUml(eObjectSelected);
		// opIn.setType(t);
		// opGen.getOwnedParameters().add(opIn);

		// String str = getCommentParameters();
		String str = params;// Parameters
		if (str != null && !str.equals("")) {
			for (String s : str.split(",")) {
				p = new Parameter();
				if (s.length() > 0) {
					String[] tr = s.split("\\.");
					p.setClassParam(tr[0]);
					p.setParam(tr[1]);

					opIn = UMLFactory.eINSTANCE.createParameter();
					opIn.setName(p.getParam());
					opIn.setDirection(ParameterDirectionKind.IN_LITERAL);
					opIn.setEffect(ParameterEffectKind.CREATE_LITERAL);
					opIn.setVisibility(VisibilityKind.PUBLIC_LITERAL);
					//t = getClassStringUml(opGen);
					opIn.setType(t);
					opGen.getOwnedParameters().add(opIn);
				}

			}
		}
		// --

		return opGen;
	}

	public Resource getResourceUmlModel(EObject opGen) {
		ResourceSet resourceSet = opGen.eResource().getResourceSet();
		URI modelUri = URI.createURI("pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml"); // TODO criar constante
		Resource modelResource = resourceSet.getResource(modelUri, true);
		return modelResource;
	}

	public PrimitiveType getClassStringUml(EObject opGen) {

		Resource modelResource = getResourceUmlModel(opGen);
		Model m = ((Model) modelResource.getContents().get(0));
		for (Element ele : m.getOwnedElements()) {
			if (ele instanceof PrimitiveType) {
				if (((PrimitiveType) ele).getName().equals("String")) { // TODO CRIAR CONSTANTE
					return (PrimitiveType) ele;
				}
			}
		}
		return null;
	}

	public Activity setParametersActivity(Activity opGen, Class clasTypeRet, String params, EObject eObjectSelected,
			boolean isMultivalored) {

		if (clasTypeRet != null) {
			opGen.getOwnedParameters().clear();
			org.eclipse.uml2.uml.Parameter opr = UMLFactory.eINSTANCE.createParameter();
			opr.setName("ret");
			opr.setVisibility(VisibilityKind.PUBLIC_LITERAL);
			opr.setDirection(ParameterDirectionKind.RETURN_LITERAL);
			opr.setEffect(ParameterEffectKind.CREATE_LITERAL);
			Type tp = clasTypeRet;
			opr.setType(tp);
			if (isMultivalored) {
				// multivalorado
				opr.setUpper(LiteralUnlimitedNatural.UNLIMITED);
				opr.setLower(0);
			}else {
				opr.setLower(1);
				opr.setUpper(1);
			}
			opGen.getOwnedParameters().add(opr);
		}
		// --

		org.eclipse.uml2.uml.Parameter opIn = UMLFactory.eINSTANCE.createParameter();
		Parameter p = new Parameter();
		// opIn.setName("listObjects");
		// opIn.setDirection(ParameterDirectionKind.IN_LITERAL);
		// opIn.setEffect(ParameterEffectKind.CREATE_LITERAL);
		// opIn.setVisibility(VisibilityKind.PUBLIC_LITERAL);
		// opIn.setIsUnique(true);
		// opIn.setIsException(false);
		// opIn.setIsOrdered(false);
		// opIn.setIsStream(false);
		// opIn.setUpper(1);
		Type t = getClassStringUml(eObjectSelected);
		// opIn.setType(t);
		// opGen.getOwnedParameters().add(opIn);

		// --
		String str = params;// commentUtil.getCommentType("parameters");//Parameters
		if (str != null && !str.equals("")) {
			for (String s : str.split(",")) {
				p = new Parameter();
				if (s.length() > 0) {
					String[] tr = s.split("\\.");
					p.setClassParam(tr[0]);
					p.setParam(tr[1]);

					opIn = UMLFactory.eINSTANCE.createParameter();
					opIn.setName(StringExtensions.toFirstLower(p.getClassParam()) + p.getParam());
					opIn.setDirection(ParameterDirectionKind.IN_LITERAL);
					opIn.setEffect(ParameterEffectKind.CREATE_LITERAL);
					opIn.setVisibility(VisibilityKind.PUBLIC_LITERAL);
					//t = getClassStringUml(opGen);
					opIn.setType(t);
					opGen.getOwnedParameters().add(opIn);
				}

			}
		}
		// --

		return opGen;
	}

	public Element generateBehaviorOperation(Element classGerente, Element opGerente, Class clas, String params,
			String nameBehavior, EObject eObjectSelected, boolean isMultivalored) {
		List<Activity> elementos2 = new ArrayList<Activity>();
		String name = "";
		if (nameBehavior != null && !nameBehavior.equals("")) {
			name = nameBehavior;
		} else {
			name = ((Operation) opGerente).getName() + "Impl";
		}
		Element retorno = null;

		if (classGerente instanceof Class) {
			for (Element ele : ((Class) classGerente).getOwnedElements()) {
				if (ele instanceof Activity) {
					if (((Activity) ele).getName().equals(name)) {
						retorno = ele; // TODO quando existir verificar se h� mudan�a nos parametros.
						//retorno = setParametersActivity((Activity) ele, clas, params, eObjectSelected, isMultivalored);
						 ((Activity) retorno).setSpecification((Operation) opGerente);
						// // elementos2.add(ele);
					} else {
						elementos2.add((Activity) ele);
					}
				}
			}
		}
		if (retorno == null) {

			//Activity act = Utils.getMethod((Class) classGerente, (Operation) opGerente);
			Activity act = UMLFactory.eINSTANCE.createActivity();
			act.setName(name);
			// TODO CRIAR CONSTANTE
			act.setIsActive(true);
			//act = setParametersActivity(act, clas, params, eObjectSelected, isMultivalored);
			act.setSpecification((Operation) opGerente);
			elementos2.add(act);
			retorno = act;
		} else {
			// modificado
			// retorno.getOwnedComments().clear();
			// retorno.getOwnedComments().add(comentElementos2);
			elementos2.add((Activity) retorno);
		}

		classGerente.eSet(classGerente.eClass().getEStructuralFeature("ownedBehavior"), elementos2);

		return retorno;
	}

	public Element generateBehaviorModel(Element classGerente, Class clas, String params, String nameBehavior,
			EObject eObjectSelected, boolean isMultivalored) {
		
		try {
			
		
		List<Element> elementos2 = new ArrayList<Element>();
		String name = "";
		if (nameBehavior != null && !nameBehavior.equals("")) {
			name = nameBehavior;
		} else {
			name = "Impl";
		}
		Element retorno = null;

		if (classGerente instanceof Class) {
			for (Element ele : ((Class) classGerente).getOwnedElements()) {
				if (ele instanceof Activity) {
					if (((Activity) ele).getName().equals(name)) {
						//retorno = ele; // TODO quando existir verificar se h� mudan�a nos parametros.
						//retorno = setParametersActivity((Activity) ele, clas, params, eObjectSelected, isMultivalored);
						// ((Activity) retorno).setSpecification((Operation) opGerente);
						 elementos2.add(retorno);
					} else {
						elementos2.add((Activity) ele);
					}
				}
			}
		}

		if (classGerente instanceof Model) {
			for (Element ele : ((Model) classGerente).getOwnedElements()) {
				if (ele instanceof Activity) {
					if (((Activity) ele).getName().equals(name)) {
						elementos2.remove(ele);
						//retorno = ele; // TODO quando existir verificar se h� mudan�a nos parametros.
						//retorno = setParametersActivity((Activity) ele, clas, params, eObjectSelected, isMultivalored);
						// ((Activity) retorno).setSpecification((Operation) opGerente);
						//elementos2.add(retorno);
					} else {
						elementos2.add((Activity) ele);
					}
				} else if (ele instanceof Class) { 
					elementos2.add(ele);
				}
			}
		}

		if (retorno == null) {

			// Activity act = Utils.getFactory((Class)classGerente);
			Activity act = UMLFactory.eINSTANCE.createActivity();
			act.setName(name);
			act.setIsActive(false);
			act.setIsReentrant(true);
			elementos2.add(act);
			retorno = act;
		}
		// } else {
		// // modificado
		// // retorno.getOwnedComments().clear();
		// // retorno.getOwnedComments().add(comentElementos2);
		// elementos2.add((Activity) retorno);
		// }

		if (classGerente instanceof Class) {
			classGerente.eSet(classGerente.eClass().getEStructuralFeature("ownedBehavior"), elementos2);
		} else if (classGerente instanceof Model) {
			classGerente.eSet(classGerente.eClass().getEStructuralFeature("packagedElement"), elementos2);
		}

		return retorno;
		}catch (Exception e) {
			// TODO: handle exception
e.printStackTrace();
		}
		return null;
	}

	public boolean executeAlfCode(Element element, String alfCode) {
		// executar alf
		NamedElement semanticObject = ((NamedElement) element);
		if (semanticObject != null && alfCode != null) {
			ScenarioFactoryOOHDM.getInstance().createCommitScenario().execute(semanticObject, alfCode);
		}
		return true;
	}
//	public boolean executeAlfCode(EObject element, String alfCode) {
//		// executar alf
//		NamedElement semanticObject = ((NamedElement) element);
//		if (semanticObject != null && alfCode != null) {
//			ScenarioFactory.getInstance().createCommitScenario().execute(semanticObject, alfCode);
//		}
//		return true;
//	}
//
//	public void cla() {
//
//	}

	public Resource getResourceOohdmModel(EObject eobj) {
		ResourceSet resourceSet = eobj.eResource().getResourceSet();
		URI modelUri = URI.createURI("platform:/plugin/PL/resources/oohdm/oohdmModel.uml"); // TODO criar constante
		Resource modelResource = resourceSet.getResource(modelUri, true);
		return modelResource;
	}
	
	public Resource getResourceAlfModel(EObject eobj) {
		ResourceSet resourceSet = eobj.eResource().getResourceSet();
		URI modelUri = URI.createURI("pathmap://PAPYRUS_ALF_LIBRARY/Alf.library.uml"); // TODO criar constante
		Resource modelResource = resourceSet.getResource(modelUri, true);
		return modelResource;
	}
	
	public Resource getResourcePrimitiveTypesModel(EObject eobj,URI modelUri) {
		ResourceSet resourceSet = eobj.eResource().getResourceSet();
//		URI modelUri = URI.createURI("pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml"); // TODO criar constante
		Resource modelResource = resourceSet.getResource(modelUri, true);
		return modelResource;
	}
	
	public Resource getResourceFoundationalModelLibraryModel(EObject eobj,URI modelUri) {
		ResourceSet resourceSet = eobj.eResource().getResourceSet();
//		URI modelUri = URI.createURI("pathmap://PAPYRUS_fUML_LIBRARY/fUML.library.uml"); // TODO criar constante
		Resource modelResource = resourceSet.getResource(modelUri, true);
		return modelResource;
	}
	
	public Resource getResourceEcorePrimitiveTypesModel(EObject eobj,URI modelUri) {
		ResourceSet resourceSet = eobj.eResource().getResourceSet();
//		URI modelUri = URI.createURI("pathmap://UML_LIBRARIES/EcorePrimitiveTypes.library.um"); // TODO criar constante
		Resource modelResource = resourceSet.getResource(modelUri, true);
		return modelResource;
	}
	
	public Resource getResourceAssertionLibraryModel(EObject eobj ,URI modelUri) {
		ResourceSet resourceSet = eobj.eResource().getResourceSet();
//		URI modelUri = URI.createURI("pathmap://PAPYRUS_ASSERTION_LIBRARY/AssertionLibrary.uml"); // TODO criar constante
		Resource modelResource = resourceSet.getResource(modelUri, true);
		return modelResource;
	}
	
	public Resource getResourceCollectionClassesImplModel(EObject eobj,URI modelUri) {
		ResourceSet resourceSet = eobj.eResource().getResourceSet();
//		URI modelUri = URI.createURI("pathmap://PAPYRUS_ALF_LIBRARY/CollectionClassesImpl.library.uml"); // TODO criar constante
		Resource modelResource = resourceSet.getResource(modelUri, true);
		return modelResource;
	}

	public Class getClassIndex(EObject eobj) {

		Resource modelResource = getResourceOohdmModel(eobj);
		Model m = ((Model) modelResource.getContents().get(0));
		for (Element ele : m.getOwnedElements()) {
			if (ele instanceof Class) {
				if (((Class) ele).getName().equals("SimpleIndex")) { // TODO CRIAR CONSTANTE
					return (Class) ele;
				}
			}
		}
		return null;
	}

	public Class getClassOOHDMByName(EObject eobj, String name) {

		Resource modelResource = getResourceOohdmModel(eobj);
		Model m = ((Model) modelResource.getContents().get(0));
		for (Element ele : m.getOwnedElements()) {
			if (ele instanceof Class) {
				if (((Class) ele).getName().equals(name)) { // TODO CRIAR CONSTANTE
					return (Class) ele;
				}
			}
		}
		return null;
	}

	public Element generateAttributs(Element daoClass, Type classType, String name, EObject eObject,
			boolean isMultivalored) {
		// String name = "gerar" + ((Class) daoClass).getName();// TODO criar constante
		List<Element> elementos2 = new ArrayList<Element>();
		for (Element element : daoClass.getOwnedElements()) {
			if (element instanceof org.eclipse.uml2.uml.Property) {
				if (((Property) element).getName().equals(name)) {
					return element;
				} else/* if(element instanceof Model) */ {
					elementos2.add(element);
				}
			}
		}

		org.eclipse.uml2.uml.Property pGen = UMLFactory.eINSTANCE.createProperty();
		pGen.setName(name);
		pGen.setType(classType);
		if (isMultivalored) {
			// multivalorado
			pGen.setLower(0);
			pGen.setUpper(LiteralUnlimitedNatural.UNLIMITED);
			
		}
		elementos2.add(pGen);

		daoClass.eSet(daoClass.eClass().getEStructuralFeature("ownedAttribute"), elementos2);

		return pGen;
	}

	public Element setSpecification(Element classGerente, Element opGerente, TransactionalEditingDomain domain) throws Exception {

		List<Activity> elementos2 = new ArrayList<Activity>();
		Element retorno = null;
		Element act = getActivityToSpecification(classGerente, opGerente);
		while (act == null) {
			act = getActivityToSpecification(classGerente, opGerente);
		}
		Element act2 = act;
		Resource resource = act.getOwner().eResource();
		if(resource == null) {
			throw new Exception("Erro ao setar Specification.");
		}
		synchronized (resource) {
			resource.setTrackingModification(false);
			domain.getCommandStack().execute(new RecordingCommand(domain) {

				@Override
				protected void doExecute() {
					
					Element re = vincularOperacaoEAtividade(act2, opGerente, domain);
					while(((Activity) re).getSpecification() == null) {
						re = vincularOperacaoEAtividade(act2, opGerente, domain);
					}
					
				}
			});

			//classGerente.eSet(classGerente.eClass().getEStructuralFeature("ownedBehavior"), elementos2);
			resource.setTrackingModification(true);
		}
		return retorno;
	}

	public Element vincularOperacaoEAtividade(Element ele, Element opGerente, TransactionalEditingDomain domain) {
		// RESOURCE RESOURCE = ELE.GETOWNER().ERESOURCE();
		// RESOURCE.SETTRACKINGMODIFICATION(FALSE);
		// Element ret = null;
		// synchronized (resource) {

		// domain.getCommandStack().execute(new RecordingCommand(domain) {
		//
		// @Override
		// protected void doExecute() {
		((Activity) ele).setSpecification((Operation) opGerente);
		// resource.setTrackingModification(true);
		// ret = ele;
		// }
		// });
		//
		// }
		return ele;
	}

	protected Element getActivityToSpecification(Element classGerente, Element opGerente) {

		// List<Activity> elementos2 = new ArrayList<Activity>();
		String name = ((Operation) opGerente).getName() + "Impl";
		Element retorno = null;

		for (Element ele : ((Class) classGerente).getOwnedElements()) {
			if (ele instanceof Activity) {
				if (((Activity) ele).getName().equals(name)) {
					retorno = ele;
					// elementos2.add(ele);
					// } else {
					// elementos2.add((Activity) ele);
					// }
				}
			}
		}
		// classGerente.eSet(classGerente.eClass().getEStructuralFeature("ownedBehavior"),
		// elementos2);

		return retorno;
	}

	public void executeSpecificationJob(TransactionalEditingDomain tEditDomain, Element classGerente,
			Element operationGerente) {

		SpecificationJob sJob = new SpecificationJob(tEditDomain,classGerente, operationGerente);
		sJob.setPriority(Job.DECORATE);
		sJob.schedule();
		
		
	}
	// TODO Auto-generated method stub

	public List<Element> removeModelElement(String modelName, EObject rootEObject) {
		Element e1 = ((Element) rootEObject).getModel().getOwner();
		List<Element> elementos = e1.getOwnedElements();
		List<Element> elementos2 = new ArrayList<Element>();
		List<Element> elementos3 = new ArrayList<Element>();
		for (Element element : elementos) {
			if (element.getModel().getName().equals(modelName)) { // TODO criar constante
				if (element instanceof org.eclipse.uml2.uml.Model) {
					
					elementos3.addAll(element.getOwnedElements());
					elementos2.remove(element);
					
				}
			} else if (element instanceof Model) {
				elementos2.add(element);
			}
		}

		Element ele = ((Element) rootEObject).getModel().getOwner();
		if (ele.getModel().getName().equals(getNameRootModel())) {
			ele.eSet(ele.eClass().getEStructuralFeature("packagedElement"), elementos2);
		}
		return elementos3;
	}

	@Override
	public EObject getContextObject() {
		return this.eObject;
	}

	public Element generateAssociation(Association association, EObject eObject2) {
		
		org.eclipse.uml2.uml.Association pGen = UMLFactory.eINSTANCE.createAssociation();
//		pGen.setName(association.getName());
//		pGen.createOwnedEnd(name, type);
//		pGen.crea
//
//		daoClass.eSet(daoClass.eClass().getEStructuralFeature("ownedAttribute"), elementos2);

		return pGen;
	}

}