package fp.util;

public class Attribut {
private int order;
private String attr;
private String target;
private String classParam;
public int getOrder() {
	return order;
}
public void setOrder(int order) {
	this.order = order;
}
public String getAttr() {
	return attr;
}
public void setAttr(String attr) {
	this.attr = attr;
}
public String getTarget() {
	return target;
}
public void setTarget(String target) {
	this.target = target;
}
public String getClassParam() {
	return classParam;
}
public void setClassParam(String classParam) {
	this.classParam = classParam;
}


}
