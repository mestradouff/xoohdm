package fp.util;

public class Sort {

	private int order;
	private String attr;
	private String typeSort;
	private String classParam;
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
	public String getAttr() {
		return attr;
	}
	public void setAttr(String attr) {
		this.attr = attr;
	}
	public String getTypeSort() {
		return typeSort;
	}
	public void setTypeSort(String typeSort) {
		this.typeSort = typeSort;
	}
	public String getClassParam() {
		return classParam;
	}
	public void setClassParam(String classParam) {
		this.classParam = classParam;
	}
	
	
	
}
