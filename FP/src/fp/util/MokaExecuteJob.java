package fp.util;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.papyrus.moka.utils.constants.MokaConstants;
import org.eclipse.papyrus.uml.alf.text.generation.DefaultEditStringRetrievalStrategy;
import org.eclipse.papyrus.uml.alf.text.representation.AlfTextualRepresentation;
import org.eclipse.papyrus.uml.alf.transaction.job.AlfAbstractJob;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.uml2.uml.Element;

import fp.propertySection.IndexPropertySection;
import fp.propertySection.TestExecutionPropertySection;

public class MokaExecuteJob extends Job {

	public static final String NAME = "Moka Execute";

	private TransactionalEditingDomain ted;
	private TestExecutionPropertySection ips;
	private Composite ctn;
	private boolean isIndex=false;
	
	public MokaExecuteJob(TransactionalEditingDomain tEditDomain, TestExecutionPropertySection idx, Composite cp, boolean isIndex) {
		super(NAME);
		this.ted = tEditDomain;
		this.ips = idx;
		this.ctn = cp;
		this.isIndex = isIndex;
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		IStatus jobStatus = Status.OK_STATUS;
		TransactionalEditingDomain domain = this.ted;
		if (domain != null) {

				try {
					
					new Thread(new Runnable() {
						public void run() {

							Display.getDefault().syncExec(new Runnable() {
								public void run() {
									MokaUtil mu = new MokaUtil();
									MokaConstants.MOKA_AUTOMATIC_ANIMATION = false;
									mu.executeMoka();
									
								    	 String resultJson = mu.updateConsole(true);	
										if(isIndex) {						
										    	 ips.gerarNovoIndice(resultJson, ctn);
										}
								}

							});

						}
					}).start();
					
					
				     
					
					
				} catch (Exception e) {
					e.printStackTrace();
					jobStatus = Status.CANCEL_STATUS;
				}

		} else {
			jobStatus = Status.CANCEL_STATUS;
		}
		return jobStatus;
	}

}
