package fp.xtext

import java.util.List

class AlfCreator {
	
	static def createAlfCodeIndexTest(String[] test) 
	'''namespace Tests;
		private import Alf::Library::CollectionClasses::List;
		private import Alf::Library::PrimitiveBehaviors::IntegerFunctions::ToString;
		private import Alf::Library::BasicInputOutput::WriteLine;
		private import oohdmModel::Index;		
		private import oohdmModel::SimpleIndex;
		private import oohdmModel::IndexEntry;
		private import oohdmModel::Anchor;
		//private import oohdmModel::Dao;
		private import Generated::*;
		activity 'ActivityOOHDMTest'() {
			
						�test.get(0)� gi = new �test.get(0)�();
						SimpleIndex idx = gi.'�test.get(1)�'(�test.get(2)�);
		WriteLine("{");
		WriteLine("\"name\":");
						WriteLine("\""+idx.getName()+"\",");
						
						i = 1;
				WriteLine("\"idx\":[");
						while(i <=  idx.indexentry->size()){
							WriteLine("{\"anchors\":[");
						j = 1;
						IndexEntry ie = idx.indexentry->at(i);
						while(j <=  ie.anchor->size()){
							Anchor a = ie.anchor->at(j);
							WriteLine("{\"anchorName\":");
							WriteLine("\""+a.getName()+"\",");
						
							WriteLine("\"target\":");
							WriteLine("\""+a.getUrl()+"("+a.getName()+")"+"\"}");
								if(j != ie.anchor->size()){
							WriteLine(",");
								}
						j++;
						}
				WriteLine("]}");
						if(i != idx.indexentry->size()){
				WriteLine(",");
						}
						i++;
						
						}
		WriteLine("]}");
		}
		'''
		static def createAlfCodeObjects(String test, List<String[]> listObjs, String[][] listObjsAux,String newObj) 
	'''
	namespace Tests::�test�Dao;
	//private import Conceptual::�test�;
	private import Navigational::*;
	�IF(newObj !== null)�
	private import Navigational::�newObj�;
	
				�FOR tmpI : listObjsAux�
					�FOR tmpJ : tmpI�
					�IF((tmpI.indexOf(tmpJ) == tmpI.size()-1))�
						private import Navigational::�tmpJ�;
					�ENDIF�
					�ENDFOR�
				�ENDFOR�
	�ENDIF�
	
	activity 'getObjectsImpl'() : �test�[0..*] {
			
			�IF(newObj !== null)�
			�FOR tmpI : listObjsAux�
			�newObj� a�tmpI.get(0)� = new �newObj�();
				�FOR tmpJ : tmpI�
				�IF((tmpI.indexOf(tmpJ) !== tmpI.size()-1) && (tmpI.indexOf(tmpJ) !== 0))�
					�IF(!tmpJ.contains("=\"\""))�
						a�tmpI.get(0)�.�tmpJ�;
					�ENDIF�
					
				�ENDIF�
				�ENDFOR�
			�ENDFOR�
			�ENDIF�
			�FOR tmp : listObjs�
				�test� e�listObjs.indexOf(tmp)� = new �test�();
				�FOR tmp2 : tmp�
				�IF(tmp2 !== null && !tmp2.contains("="))�
				�FOR tmpI : listObjsAux�
									�FOR tmpJ : tmpI�
									�IF((tmpI.indexOf(tmpJ) == tmpI.size()-1))�
									//	�tmpJ�.createLink(a�tmp2�,e�listObjs.indexOf(tmp)�);
									�ENDIF�
									�ENDFOR�
								�ENDFOR�
				
					
				�ENDIF�
				�IF(tmp2 !== null && tmp2.contains("="))�
					�IF(!tmp2.contains("=\"\""))�
						e�listObjs.indexOf(tmp)�.�tmp2�;
					�ENDIF�
					
				�ENDIF�

				�ENDFOR�
			
				
				this.lista->add(e�listObjs.indexOf(tmp)�);
			�ENDFOR�
		return this.lista;
	}
	'''
	
	static def createAlfCodeContextTest(String[] test,String[] test2) 
	'''namespace Tests;
���		private import Alf::Library::CollectionClasses::List;
���		private import Alf::Library::PrimitiveBehaviors::IntegerFunctions::ToString;
���		private import Alf::Library::BasicInputOutput::WriteLine;
���		private import oohdmModel::Context;		
���		private import oohdmModel::SimpleIndex;
���		private import oohdmModel::IndexEntry;
���		private import oohdmModel::Anchor;
		
		private import Alf::Library::CollectionClasses::*;
					private import Alf::Library::PrimitiveBehaviors::IntegerFunctions::*;
					private import Alf::Library::BasicInputOutput::*;		
		private import oohdmModel::*;		
		private import Generated::*;
		private import Navigational::*;
		
		activity 'ActivityOOHDMTest'() {
						�test.get(0)� gi = new �test.get(0)�();
						ctx = gi.'�test.get(1)�'(�test.get(2)�);
						ct = "";
						WriteLine("{");
								WriteLine("\"name\":");
								WriteLine("\""+ctx.getName()+"\",");
								//lst = ctx.objs->select c(c.�test.get(3)� == "�test.get(4)�");
								WriteLine("\"objs\":[");
								lista = ctx.objs;
								j = ctx.objs->size();
								
								while(j >=  1){
									a = (�test.get(5)�)lista->at(j);
									//if(a.�test.get(3)� == "�test.get(4)�"){
									//	ct = "�test.get(4)�";
									//}
									WriteLine("{");
									�FOR t :test2�
									
									WriteLine("\"�t�\":\""+a.�t�+"\"");
									
									�IF test2.indexOf(t) == (test2.size-1)�
										WriteLine("}");
										�ELSE�
										WriteLine(",");
									�ENDIF�
									�ENDFOR�
									if(j != 1){
										WriteLine(",");
									}
									j--;
								}
								
						WriteLine("],");
						WriteLine("\"start\":");
						WriteLine("\"\'�test.get(3)�\':\'�test.get(4)�\'\",");
						
						WriteLine("\"internalNavigation\":");
						WriteLine("\""+ctx.getInternalNavigation()+"\"");
						
						WriteLine("}");
		}
		'''
		
}