package fp.xtext;

import java.util.List
import fp.util.Parameter
import fp.adapter.GerenteIndexTransition
import fp.adapter.GerenteContextTransition

class GerenteCreator {

	static def createGerenteIndex(GerenteIndexTransition git) 
	'''namespace �git.namespace�;
		�fixedImports(git.classOfIndex)�
		private import Navigational::*;
		//private import Navigational::�git.classOfIndex�;
		
		
		activity �git.behaviorName� (�getInParameters(git.parameters)�) : SimpleIndex {
		//WriteLine("inicio gerar �git.indexName�");
		SimpleIndex si = new SimpleIndex();
		si.setName("�git.indexName�");
		//WriteLine(si.getName());
		//WriteLine("passou 1");
		�git.classOfIndex�Dao dao = new �git.classOfIndex�Dao();
		//WriteLine("passou 1");
		lista = dao.getObjects();
		//�git.classOfIndex�[] lista = dao.getObjects();
		//WriteLine("passou 1");
		lista2 = lista->�git.elements�;// elementos + parametros
		//WriteLine("passou 1");
		//WriteLine(ToString(objs->size()));
		i = 1;
		while(i <= lista2->size()){
			IndexEntry ie = new IndexEntry();
			�FOR tmp : git.attributs�
			�git.classOfIndex� e�tmp.getOrder�  = (�git.classOfIndex�)lista2->at(i);
			Anchor a�tmp.getOrder� = new Anchor();
			//WriteLine(e�tmp.getOrder�.�tmp.getAttr�);
			a�tmp.getOrder�.setName(e�tmp.getOrder�.�tmp.getAttr�);
			a�tmp.getOrder�.setUrl("�tmp.getTarget�");
			ie.addAnchor(a�tmp.getOrder�);
			�ENDFOR�
			si.addIndexEntry(ie);
			i++;
			//WriteLine("Gerou indice ");
		}
		//WriteLine("fim gerar �git.indexName�");
		return si;
		}'''

	static def getInParameters(List<Parameter> params) {

		//var aux = 'in listObjects: String';
		var aux = '';
		var index = 1;
		for (prt : params) {
			aux += " in "+ prt.param+" : String";
			//aux += ", in "+ prt.classParam.toFirstLower+prt.param+" : String";
			if (index != params.size) {
				//aux += " ";
				aux += ", ";
			}
			index++;
		}

		return aux;

	}
	
	static def createGerenteContext(GerenteContextTransition git) 
	
	'''namespace �git.namespace�;
	private import oohdmModel::*;
���	private import Alf::Library::PrimitiveBehaviors::IntegerFunctions::ToString;
���	private import Alf::Library::BasicInputOutput::WriteLine;
private import Alf::Library::CollectionClasses::*;
			private import Alf::Library::PrimitiveBehaviors::IntegerFunctions::*;
			private import Alf::Library::BasicInputOutput::*;
	//private import oohdmModel::Dao;
	private import Tests::�git.classOfContext�Dao;
	
	//private import Navigational::�git.classOfContext�;
	private import Navigational::*;
	activity '�git.behaviorName�'(�getInParameters(git.parameters)�): Context {
		
		ctx = new Context();
		ctx.setName("�git.contextName�");
		ctx.setInternalNavigation("�git.navigation�");
		//dao = new Dao();
		�git.classOfContext�Dao dao = new �git.classOfContext�Dao();
		lista = dao.getObjects();
		
		//�git.classOfContext�[] lista = dao.getObjects();
		lista2 = lista->�git.elements�;
		ctx.setObjs(lista2);// elementos + parametros
		//WriteLine(ToString(ctx.objs->size()));
		return ctx;
}'''


static def createClassText(String name) 
	'''
	class �name�{
		
	}
	'''
	
	
	static def fixedImports(String classOfIndex) 
	'''
			private import Alf::Library::CollectionClasses::*;
			private import Alf::Library::PrimitiveBehaviors::IntegerFunctions::*;
			private import Alf::Library::BasicInputOutput::*;
���			private import oohdmModel::Index;		
���			private import oohdmModel::SimpleIndex;
���			private import oohdmModel::IndexEntry;
���			private import oohdmModel::Anchor;
���			private import Tests::�classOfIndex�Dao;
���			private import Alf::Library::*;
			
			private import oohdmModel::*;		
			
			private import Tests::�classOfIndex�Dao;
			
	'''
}
