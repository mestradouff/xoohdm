package fp.dialog;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import org.apache.commons.lang.StringUtils;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.designer.languages.common.base.GenUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.SelectionStatusDialog;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.Property;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

import fp.util.CommentUtil;
import fp.util.DiagramOOHDMUtil;
import fp.util.TypeComment;
import fp.util.UmlUtil;

import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;


public class InstanceDialog extends OOHDMSelectionStatusDialog {

	public InstanceDialog(Shell parent, EObject eObject) {
		super(parent,eObject);
		// TODO Auto-generated constructor stub
		//this.eo = eObject;
		comUtil = new CommentUtil(eObject);
		this.removeLooping = false;
	}
	
	public InstanceDialog(Shell parent, EObject eObject, String className) {
		super(parent,eObject);
		// TODO Auto-generated constructor stub
		//this.eo = eObject;
		this.iClass = className;
		comUtil = new CommentUtil(eObject);
		this.removeLooping = true;
	}

	
	private DiagramOOHDMUtil dmUtil = new DiagramOOHDMUtil();
	private CommentUtil comUtil;
	private boolean removeLooping;
//	private Tree fTree;
//
//	private Label fLabel;
//
//	private Combo fAlloc;
//
//	private InstanceSpecification currentIS;
//
//	private EList<InstanceSpecification> nodeOrThreadList;
//	private TreeViewer fTreeViewer;
	protected Combo c1;
	protected Combo c2;
	protected Combo c3;
	protected List<Element> elementosClasses;
	protected List<Element> elementosPropriedades;
	protected List<Element> elementosPropriedadesERelacionamentos;
	protected List<Element> elementosClassesDestino;
	private ArrayList<Text> textos = new ArrayList<Text>();
	private String iClass = "";

	@Override
	protected void computeResult() {
		// TODO Auto-generated method stub
		
		
		List<String> aux = new ArrayList<String>();
		String ret = "";
		for (int i = 0; i < textos.size(); i++) {
			if(removeLooping) {
				//aux.add("\""+((Property) elementosPropriedades.get(i)).getName()+"\":\""+textos.get(i).getText()+"\"");
//				if(((Property) elementosPropriedades.get(i)).getType() instanceof PrimitiveType) {
					ret+="\""+((Property) elementosPropriedades.get(i)).getName()+"\":\""+textos.get(i).getText()+"\"";
//				}
				
			}else {
				//aux.add("\""+((Property) elementosPropriedadesERelacionamentos.get(i)).getName()+"\":"+textos.get(i).getText());
				ret+="\""+((Property) elementosPropriedadesERelacionamentos.get(i)).getName()+"\":\""+ (textos.get(i).getText().equals("") ? "\"": textos.get(i).getText()) +"\"";
			}
			if(i!=textos.size()-1) {
				ret+=", ";
			}
		}
		//str = str.replace("\"\"", "\"");
		ret = ret.replace("\"\"", "\"");
		ret = ret.replace("\"{", "{");
		ret = ret.replace("}\"", "}");
		aux.add(ret);
		this.setResult(aux);
	}

	@Override
	public Control createDialogArea(Composite parent) {
		Composite contents = (Composite) super.createDialogArea(parent);
		contents.setLayout(new GridLayout(2, false));
		if(iClass.equals("")) {
			String string = comUtil.getCommentType(TypeComment.ATTRIBUTS.getName());
			String aux[] = string.split(",");
			iClass = aux[0].split("\\.")[0];
		}
		
		this.setTitle("new "+iClass);
		parent.getShell().setText("new "+iClass);
		
		
		//Element e1 = ((Element) eo).getModel().getOwner();
	//List<Element> elementos = getModelNavigationalElements(e1.getOwnedElements());
		
		//Element e2 = elementos.get(0);
		//elementosClasses = getClassElements(e2.getOwnedElements());
		elementosPropriedadesERelacionamentos = new ArrayList<Element>();
		Element el = null;
		el = findClassInPackage(iClass, "Navigational");
		if(el == null) {
			el = findClassInPackage(iClass, "Conceptual");
		}
		
		elementosPropriedades = getPropertyElements(el.getOwnedElements());
		for (int i = 0; i < elementosPropriedades.size(); i++) {
			
			Property ppt = (Property) elementosPropriedades.get(i);
			if(ppt.getType().getName().equals("Anchor") || ppt.getType().getName().equals("Index") && ppt.getType().getName().equals("Context") ) {
				//do nothing.
			}else {
				if(!elementosPropriedadesERelacionamentos.contains(ppt)) {
					elementosPropriedadesERelacionamentos.add(ppt);
				}
				
			}
		}
		
		
		
		
		//c2.setItems(getArrayElementos(elementosPropriedades));
		//c2.setEnabled(true);
		
		for (int i = 0; i < elementosPropriedades.size(); i++) {
			//String aux2[] = aux[i].split("\\.");
			Property ppt = (Property) elementosPropriedades.get(i);
//			if(ppt.getType().getName().equals("Anchor") || ppt.getType().getName().equals("Index") && ppt.getType().getName().equals("Context") ) {
//				//do nothing.
//			}else 
			if(ppt.getType() instanceof PrimitiveType) {

			Label l1 = new org.eclipse.swt.widgets.Label(contents, 1);
			l1.setText(StringUtils.capitalize(ppt.getName())+":");
			Text tc = new Text(contents, SWT.BORDER);
			tc.setSize(500, 10);
			textos.add(tc);
			}
		}
		
		if(!removeLooping) {
			
			el = findClassInPackage(iClass, "Navigational");
			if(el == null) {
				el = findClassInPackage(iClass, "Conceptual");
			}
			
			List<Element> elementoPropriedadeRelacionamento = new ArrayList<Element>();
			List<Element> elementoPropriedadeRelacionamento2 = findRelationsInPackage(iClass, "Navigational");
				
			
			if(elementoPropriedadeRelacionamento2.size() == 0) {
				elementoPropriedadeRelacionamento2 = findRelationsInPackage(iClass, "Conceptual");
			}
		
			
			for (int i = 0; i < elementosPropriedades.size(); i++) {
				Property ppt = (Property) elementosPropriedades.get(i);
				if(ppt.getType().getName().equals("Anchor") || ppt.getType().getName().equals("Index") && ppt.getType().getName().equals("Context") ) {
					//do nothing.
				}else if(!(ppt.getType() instanceof PrimitiveType) ) {
					
					elementoPropriedadeRelacionamento.add(ppt);
				}
			}
			elementoPropriedadeRelacionamento.addAll(elementoPropriedadeRelacionamento2);
			
			//elementosPropriedades.addAll(elementoPropriedadeRelacionamento);
			
			addAllWithOutRepetitions(elementoPropriedadeRelacionamento);
			
		//	elementosPropriedadesERelacionamentos.addAll(elementoPropriedadeRelacionamento);
			
		
			
			
			for (int i = 0; i < elementoPropriedadeRelacionamento.size(); i++) {
				//String aux2[] = aux[i].split("\\.");
				Property ppt = (Property) elementoPropriedadeRelacionamento.get(i);
				
				
				String assoc = findAssociationNameInPackage(iClass, "Navigational", ppt.getName());
				
				if( assoc == null ) {
					assoc = findAssociationNameInPackage(iClass, "Conceptual", ppt.getName());
				}
				//Association ass = (Association) elementoPropriedadeRelacionamento.get(i);
				Label l1 = new org.eclipse.swt.widgets.Label(contents, 1);
				l1.setText(StringUtils.capitalize(ppt.getName())+":");
				Button bt = new Button(contents, 1);
				bt.setImage(new Image(Display.getDefault(), getClass().getResourceAsStream("/icons/Plus.gif")));

				Label lb = new Label(contents, 1);
				
				lb.setVisible(false);
//				Label lb2 = new Label(contents, 1);
//				
//				lb2.setVisible(false);
				
				
				Text tc = new Text(contents, SWT.BORDER);
				tc.setSize(500, 10);
				textos.add(tc);
				tc.setVisible(false);
				
				String assocEfetive = assoc;
				bt.addSelectionListener(new SelectionListener() {

					@Override
					public void widgetSelected(SelectionEvent e) {
						Element el = findClassInPackage(ppt.getType().getName(), "Navigational");
						if(el == null) {
							el = findClassInPackage(ppt.getType().getName(), "Conceptual");
						}
						
						InstanceSelectDialog pd = new InstanceSelectDialog(contents.getShell(),el,ppt.getType().getName());
						//InstanceDialog pd = new InstanceDialog(contents.getShell(),findClassInPackage(ppt.getType().getName(), "Navigational"),ppt.getType().getName());
						pd.open();
						Object[] obj = pd.getResult();
						
						//Text tc = new Text(contents, SWT.BORDER);
						//tc.setSize(500, 10);
						int si = obj.length;
						String aux = "{";
						String aux2 = "";
						for(int i = 0; i<si; i++) {
							//aux+= ((Property) elementosPropriedades.get(i)).getName()+":";
							aux+=obj[i];
							aux2+=obj[i];
							if(i!= si-1) {
								aux+=",";
								aux2+=",";
							}else {
								aux+=",\"relation\":\""+assocEfetive+"\"";
							}
						}
						aux+="}";
						
						String id_instance =((String) obj[0]).split(":")[1].replaceAll("\"", "");
						int k = 0 ;
						String texto = getObjectbyInstanceId(StringUtils.capitalize(ppt.getName()),id_instance);
						
						tc.setText(aux);
						tc.setVisible(false);
						//textos.get(tc);
						bt.setVisible(false);
						
						lb.setText(texto);
						lb.setVisible(true);
//						
						contents.layout();
//						contents.layout(true);
						//contents.layout(true,true);
						contents.getParent().layout(true,true);
						parent.layout(true,true);
						parent.getShell().layout();
					}

				@Override
					public void widgetDefaultSelected(SelectionEvent e) {

					}
				});
				
			}
		}

		
		
//		 
//		 
//		Element e1 = ((Element) eo).getModel().getOwner();
//		List<Element> elementos = getModelNavigationalElements(e1.getOwnedElements());
//		Element e2 = elementos.get(0);
//		elementosClasses = getClassElements(e2.getOwnedElements());
//
//		
//		Link link = new Link(contents, SWT.NONE);
//	    link.setText("Classe: <A href=\"#\">+</A>");
//	    link.setSize(140, 40);
//	    
//	    link.addListener (SWT.Selection, new Listener () {
//	      public void handleEvent(Event event) {
//	        System.out.println("Selection: " + event.text);
//	        dmUtil.openNavigationalDiagram(eo);
//	        close();
//	       
//	      }
//	    });    
//	    

	    
//		Label l = new org.eclipse.swt.widgets.Label(contents, 1);
//		l.setText("Classe:");

//		c1 = new Combo(contents, SWT.READ_ONLY);
//		c1.setBounds(50, 50, 150, 65);
//
//		Label l2 = new org.eclipse.swt.widgets.Label(contents, 1);
//		l2.setText("Par�metro:");
//
//		c2 = new Combo(contents, SWT.READ_ONLY);
//		c2.setBounds(50, 85, 150, 65);
//		c2.setEnabled(false);
//
//		c1.setItems(getArrayElementos(elementosClasses));
//		c1.addSelectionListener(new SelectionAdapter() {
//			public void widgetSelected(SelectionEvent e) {
//
//				int index = c1.getSelectionIndex();
//
//				elementosPropriedades = getPropertyElements(elementosClasses.get(index).getOwnedElements());
//				c2.setItems(getArrayElementos(elementosPropriedades));
//				c2.setEnabled(true);
//
//			}
//		});
//
//		Label l3 = new org.eclipse.swt.widgets.Label(contents, 1);
//		l3.setText("Destino:");
//
//		List<Element> elementosCtx = getModelContextElements(e1.getOwnedElements());
//		Element e3 = elementosCtx.get(0);
//		elementosClassesDestino = getCtxIdxElements(e3.getOwnedElements(), this.eo);
//
//		c3 = new Combo(contents, SWT.READ_ONLY);
//		c3.setBounds(50, 50, 150, 65);
//		c3.setItems(getArrayElementos(elementosClassesDestino));
		// fTree = new Tree(contents, SWT.CHECK | SWT.BORDER | SWT.V_SCROLL |
		// SWT.H_SCROLL |SWT.MULTI);
		// fTree.setSize(990, 999);
		// contents.setSize(999, 999);
		// contents.setLayout(new GridLayout(1, false));
		// fTreeViewer = new TreeViewer(fTree);
		// fTreeViewer.setContentProvider(new ParamsDialogProvider());
		// fTreeViewer.setInput(elementos2);
		// fTreeViewer.setLabelProvider(new ColumnLabelProvider());
		//
		// //fTreeViewer.setContentProvider(new ParamsDialogProvider());
		//// Element e2 = elementos.get(loopIndex0);
		//// List<Element> elementos2 = getClassElements(e2.getOwnedElements());
		// // fTreeViewer.setSelection(new StructuredSelection(elementos), true);

		// for (int loopIndex0 = 0; loopIndex0 < elementos.size(); loopIndex0++) {
		// TreeItem treeItem0 = new TreeItem(fTree, 1);
		// Element e2 = elementos.get(loopIndex0);
		// List<Element> elementos2 = getClassElements(e2.getOwnedElements());
		// treeItem0.setText(elementos.get(loopIndex0).getModel().getName().toString());
		// treeItem0.setData(elementos.get(loopIndex0).getModel());
		// for (int loopIndex1 = 0; loopIndex1 < elementos2.size(); loopIndex1++) {
		// TreeItem treeItem1 = new TreeItem(treeItem0, 0);
		// treeItem1.setText(((ClassImpl)elementos2.get(loopIndex1)).getName().toString());
		//
		// }
		// }

		// (parent, "Container rules", "Avail. extensions/interceptors");

		// fTree = new Tree(contents, SWT.H_SCROLL | SWT.BORDER);
		// fTree.setHeaderVisible(true);
		// GridData data = new GridData(GridData.FILL_BOTH);
		// fTree.setLayoutData(data);
		// data.heightHint = 150;
		// // data.widthHint = 200;
		//
		// // Turn off drawing to avoid flicker
		// fTree.setRedraw(false);
		//
		// TreeColumn instances = new TreeColumn(fTree, SWT.LEFT);
		// instances.setText("Instance");
		// instances.setWidth(200);
		// TreeColumn explicitAlloc = new TreeColumn(fTree, SWT.LEFT);
		// explicitAlloc.setText("explicit allocation");
		// explicitAlloc.setWidth(150);
		// TreeColumn implicitAlloc = new TreeColumn(fTree, SWT.LEFT);
		// implicitAlloc.setText("implicit allocation");
		// implicitAlloc.setWidth(150);
		//
		//// fillTree(fTree, null, m_cdp.getMainInstance());
		//
		// // Turn drawing back on!
		// fTree.setRedraw(true);
		//
		// fTree.addSelectionListener(new SelectionAdapter() {
		//
		// @Override
		// public void widgetSelected(SelectionEvent e) {
		// if (fTree.getSelection().length > 0) {
		//// selectInstance(fTree.getSelection()[0]);
		// }
		// }
		// });
		//
		// Label label = new Label();
		// label.setText("seletected instance:");
		// fLabel = new Label();

		// createAllocInfo(contents);
		return contents;
	}

	
private void addAllWithOutRepetitions(List<Element> elementoPropriedadeRelacionamento) {
	
	for (int i = 0; i < elementoPropriedadeRelacionamento.size(); i++) {
		if(!elementosPropriedadesERelacionamentos.contains(elementoPropriedadeRelacionamento.get(i))) {
			elementosPropriedadesERelacionamentos.add(elementoPropriedadeRelacionamento.get(i));
		}
	}
	}

private String getObjectbyInstanceId(String classOfObj, String id_instance) {
		
		String stringObjs = getObjects(StringUtils.capitalize(classOfObj));
		
		JsonObject objMor = new JsonObject();
		objMor = Json.parse(stringObjs).asObject();
		JsonArray items = objMor.get("objs").asArray();
		String ret = "";
		for (JsonValue item1 : items) {
			if(item1.asObject().get("id_instance").asString().equals(id_instance)) {
				
				
				for(String str: item1.asObject().names()) {
					if(item1.asObject().get(str).isString()) {
						if(!str.equals("id_instance")){
							ret += item1.asObject().get(str).asString()+" \n ";
						
						}
					}
				}
				return ret;
			}
			
		}
		return null;
	}

	@Override
	protected void okPressed() {
		
//		for (int i = 0; i < textos.size(); i++) {
//			if( textos.get(i).getText().equals("")) {
//				MessageBox messageDialog = new MessageBox(super.getShell(), SWT.ERROR);
//				messageDialog.setText("Erro");
//				messageDialog.setMessage("Todos os campos s�o obrigat�rios!");
//				messageDialog.open();
//				return;
//			}
//}
		
			super.okPressed();

	}
}
