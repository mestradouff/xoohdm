package fp.dialog;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.designer.languages.common.base.GenUtils;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.SelectionStatusDialog;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Property;

import fp.util.CommentUtil;
import fp.util.TypeComment;

public abstract class OOHDMSelectionStatusDialog extends SelectionStatusDialog{

	protected EObject eo;
	
	public OOHDMSelectionStatusDialog(Shell parent, EObject eObject) {
		super(parent);
		// TODO Auto-generated constructor stub
		comUtil = new CommentUtil(eObject);
		this.eo = eObject;
	}
	private CommentUtil comUtil;

	protected List<Element> getCtxIdxElements(List<Element> classElements, EObject eo) {
		
		String name = "";
		if(eo instanceof Class) {
			 name = ((Class)eo).getName();
		} else if(eo instanceof Property) {
			 name = ((Property)eo).getName();
		}
		List<Element> aux = new ArrayList<Element>();
		// aux.removeAll(ownedElements);
		
		for (Element element : classElements) {

			if (element instanceof org.eclipse.uml2.uml.Class) {
				if (GenUtils.hasStereotype(element, "OohdmProfile::Index")) { // TODO CRIAR CONSTANTE
					if(!((org.eclipse.uml2.uml.Class) element).getName().equals(name))
					aux.add(element);
				}
			}
			if (element instanceof org.eclipse.uml2.uml.Package) {
				if (GenUtils.hasStereotype(element, "OohdmProfile::NavigationalClass")) {// TODO CRIAR CONSTANTE
					for (Element elementctx : element.getOwnedElements()) {
						if (GenUtils.hasStereotype(elementctx, "OohdmProfile::Context")) {// TODO CRIAR CONSTANTE
							aux.add(elementctx);
						}
					}
					
				}
			}
		}
		return aux;
	}


	protected String[] getArrayElementos(List<Element> elementos2) {

		List<String> fdstring = new ArrayList<String>();

		for (Element ele : elementos2) {

			if (ele instanceof org.eclipse.uml2.uml.Class) {
				if (GenUtils.hasStereotype(ele, "OohdmProfile::Index")) { // TODO CRIAR CONSTANTE
					fdstring.add("Idx "+((org.eclipse.uml2.uml.Class) ele).getName());
				}else if (GenUtils.hasStereotype(ele, "OohdmProfile::Context")) { // TODO CRIAR CONSTANTE
					fdstring.add("Ctx "+((Class) ele).getPackage().getName()+"."+((Class) ele).getName());
				}else {
					fdstring.add(((Class) ele).getName());
				}
				
			} else if (ele instanceof Property) {
				fdstring.add(((Property) ele).getName());
			} 

		}
		String[] simpleArray = new String[fdstring.size()];

		return fdstring.toArray(simpleArray);
	}

	protected List<Element> getModelContextElements(EList<Element> ownedElements) {

		List<Element> aux = new ArrayList<Element>();
		// aux.removeAll(ownedElements);
		for (Element element : ownedElements) {
			if (element.getModel().getName().equals("Context")) { //TODO CRIAR CONSTANTE
				if (element instanceof org.eclipse.uml2.uml.Model) {
					aux.add(element);
				}
			}
		}
		return aux;
	}

	protected List<Element> getModelNavigationalElements(EList<Element> ownedElements) {

		List<Element> aux = new ArrayList<Element>();
		// aux.removeAll(ownedElements);
		for (Element element : ownedElements) {
			if (element.getModel().getName().equals("Navigational")) {

				if (element instanceof org.eclipse.uml2.uml.Model) {
					aux.add(element);
				}
			}
		}
		return aux;
	}
	protected List<Element> getModelConceptualElements(EList<Element> ownedElements) {

		List<Element> aux = new ArrayList<Element>();
		// aux.removeAll(ownedElements);
		for (Element element : ownedElements) {
			if (element.getModel().getName().equals("Conceptual")) {

				if (element instanceof org.eclipse.uml2.uml.Model) {
					aux.add(element);
				}
			}
		}
		return aux;
	}
	

	protected List<Element> getClassElements(EList<Element> ownedElements) {

		List<Element> aux = new ArrayList<Element>();
		// aux.removeAll(ownedElements);
		for (Element element : ownedElements) {

			if (element instanceof org.eclipse.uml2.uml.Class) {
				aux.add(element);
			}
		}
		return aux;
	}

	protected List<Element> getPropertyElements(EList<Element> ownedElements) {

		List<Element> aux = new ArrayList<Element>();
		// aux.removeAll(ownedElements);
		for (Element element : ownedElements) {

			if (element instanceof org.eclipse.uml2.uml.Property) {
				aux.add(element);
			}
		}
		return aux;
	}

	public Class findClassInPackage(String aux, String packageName) {

		// Element root =
		// ((Class)getEObject()).getModel().getModel().getModel().getOwner();
		Element root = null;
		if (this.eo instanceof Class) {
			root = ((Class) this.eo).getModel().getModel().getModel().getOwner();
		} else if (this.eo instanceof Property) {
			root = ((Property) this.eo).getModel().getModel().getModel().getOwner();
		}

		for (Element ele : root.getOwnedElements()) {
			if (ele instanceof Model) {
				if (((Model) ele).getName().equals(packageName)) {
					for (Element ele2 : ((Model) ele).getOwnedElements()) {
						if (ele2 instanceof Class) {
							if (((Class) ele2).getName().equals(aux)) {
								return (Class) ele2;
							}
						}else if (ele2 instanceof org.eclipse.uml2.uml.Package) {
							for (Element ele3 : ((org.eclipse.uml2.uml.Package) ele2).getOwnedElements()) {
								if (ele3 instanceof Class) {
									if (((Class) ele3).getName().equals(aux)) {
										return (Class) ele3;
									}
								}
							}
						}
					}
				}

			}
			
		}
		return null;
	}
	
	public List<Element> findRelationsInPackage(String aux, String packageName) {

		List<Element> lstAux = new ArrayList<Element>();
		// Element root =
		// ((Class)getEObject()).getModel().getModel().getModel().getOwner();
		Element root = null;
		if (this.eo instanceof Class) {
			root = ((Class) this.eo).getModel().getModel().getModel().getOwner();
		} else if (this.eo instanceof Property) {
			root = ((Property) this.eo).getModel().getModel().getModel().getOwner();
		}

		for (Element ele : root.getOwnedElements()) {
			if (ele instanceof Model) {
				if (((Model) ele).getName().equals(packageName)) {
					for (Element ele2 : ((Model) ele).getOwnedElements()) {
						if (ele2 instanceof Association) {
								if (!((Association) ele2).getMemberEnds().get(0).getType().getName().equals(aux) 
										&& ((Association) ele2).getMemberEnds().get(1).getType().getName().equals(aux)) {
									lstAux.add(((Association) ele2).getMemberEnds().get(0));
									//return (Property) ele3;
								}else if (((Association) ele2).getMemberEnds().get(0).getType().getName().equals(aux) 
										&& !((Association) ele2).getMemberEnds().get(1).getType().getName().equals(aux)) {
									lstAux.add(((Association) ele2).getMemberEnds().get(1));
									//return (Property) ele3;
								}
							
						}
//							else if (ele2 instanceof org.eclipse.uml2.uml.Package) {
//							for (Element ele3 : ((org.eclipse.uml2.uml.Package) ele2).getOwnedElements()) {
//								if (ele3 instanceof Class) {
//									if (((Class) ele3).getName().equals(aux)) {
//										return (Class) ele3;
//									}
//								}
//							}
//						}
					}
				}

			}
			
		}
		return lstAux;
	}


public String findAssociationNameInPackage(String aux, String packageName, String aux2) {

	//List<Element> lstAux = new ArrayList<Element>();
	// Element root =
	// ((Class)getEObject()).getModel().getModel().getModel().getOwner();
	Element root = null;
	if (this.eo instanceof Class) {
		root = ((Class) this.eo).getModel().getModel().getModel().getOwner();
	} else if (this.eo instanceof Property) {
		root = ((Property) this.eo).getModel().getModel().getModel().getOwner();
	}

	for (Element ele : root.getOwnedElements()) {
		if (ele instanceof Model) {
			if (((Model) ele).getName().equals(packageName)) {
				for (Element ele2 : ((Model) ele).getOwnedElements()) {
					if (ele2 instanceof Association) {
							if (((Association) ele2).getMemberEnds().get(0).getType().getName().equals(StringUtils.capitalize(aux)) 
									&& ((Association) ele2).getMemberEnds().get(1).getType().getName().equals(StringUtils.capitalize(aux2))) {
								//lstAux.add(((Association) ele2).getMemberEnds().get(0));
								
								return ((Association) ele2).getName();
								//return (Property) ele3;
							}else if (((Association) ele2).getMemberEnds().get(0).getType().getName().equals(StringUtils.capitalize(aux2)) 
									&& ((Association) ele2).getMemberEnds().get(1).getType().getName().equals(StringUtils.capitalize(aux))) {
								//lstAux.add(((Association) ele2).getMemberEnds().get(1));
								return ((Association) ele2).getName();
								//return (Property) ele3;
							}
						
					}
//						else if (ele2 instanceof org.eclipse.uml2.uml.Package) {
//						for (Element ele3 : ((org.eclipse.uml2.uml.Package) ele2).getOwnedElements()) {
//							if (ele3 instanceof Class) {
//								if (((Class) ele3).getName().equals(aux)) {
//									return (Class) ele3;
//								}
//							}
//						}
//					}
				}
			}

		}
		
	}
	return null;
}

protected String getObjects(String auxClass) {
	
	Class cl = null;
	if(findClassInPackage(auxClass+"Dao", "Tests") == null) {
		//Element behaviorDaoTest = createDaoTest(findNavigationalClass(auxClass));
		//cl = (Class) ((Activity)behaviorDaoTest).getSpecification().getOwner();
	}else {
		cl = findClassInPackage(auxClass+"Dao", "Tests");
	}
	
	
	String str = comUtil.getCommentType(TypeComment.OBJECTS.getName(),cl);
	
	return str;
}

public Class findNavigationalClass(String sr) {

	String aux = sr.split(",")[0].split("\\.")[0];
	return findClassInPackage(aux, "Navigational");
}

}
