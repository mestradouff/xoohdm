package fp.dialog;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.designer.languages.common.base.GenUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.dialogs.SelectionStatusDialog;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Property;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

import fp.util.CommentUtil;
import fp.util.DiagramOOHDMUtil;
import fp.util.TypeComment;
import fp.util.UmlUtil;

import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;


public class InstanceSelectDialog extends OOHDMSelectionStatusDialog {
	Tree fTree;
	public InstanceSelectDialog(Shell parent, EObject eObject) {
		super(parent,eObject);
		// TODO Auto-generated constructor stub
		//this.eo = eObject;
		comUtil = new CommentUtil(eObject);
		this.removeLooping = false;
	}
	
	public InstanceSelectDialog(Shell parent, EObject eObject, String className) {
		super(parent,eObject);
		// TODO Auto-generated constructor stub
		//this.eo = eObject;
		this.iClass = className;
		comUtil = new CommentUtil(eObject);
		this.removeLooping = true;
	}

	
	private DiagramOOHDMUtil dmUtil = new DiagramOOHDMUtil();
	private CommentUtil comUtil;
	private boolean removeLooping;
//	private Tree fTree;
//
//	private Label fLabel;
//
//	private Combo fAlloc;
//
//	private InstanceSpecification currentIS;
//
//	private EList<InstanceSpecification> nodeOrThreadList;
//	private TreeViewer fTreeViewer;
	protected Combo c1;
	protected Combo c2;
	protected Combo c3;
	protected List<Element> elementosClasses;
	protected List<Element> elementosPropriedades;
	protected List<Element> elementosPropriedadesERelacionamentos;
	protected List<Element> elementosClassesDestino;
	private ArrayList<Text> textos = new ArrayList<Text>();
	private String iClass = "";

	@Override
	protected void computeResult() {
		// TODO Auto-generated method stub
		
		
		List<String> aux = new ArrayList<String>();
		String ret = "";
		String[] s = (String[]) ((TreeItem) fTree.getSelection()[0]).getData();
		aux.add(s[1]);
		this.setResult(aux);
	}

	@Override
	public Control createDialogArea(Composite parent) {
		Composite contents = (Composite) super.createDialogArea(parent);
		contents.setLayout(new GridLayout(2, false));
		
		fTree = new Tree(contents, SWT.H_SCROLL | SWT.BORDER);
		
		this.setTitle("Select "+iClass);
		parent.getShell().setText("Select "+iClass);
		
		fTree.setRedraw(true);
		for (TreeColumn tc : fTree.getColumns()) {
			tc.dispose();
		}
		// commentUtil = new CommentUtil(getEObject());
		String string = comUtil.getCommentType(TypeComment.ATTRIBUTS.getName());
		//if (string == null)
			//return;
		//String aux[] = string.split(",");
		
		
		
		TreeColumn instances = new TreeColumn(fTree, SWT.LEFT);
		String auxClass = iClass;
		instances.setText(auxClass);
		instances.setWidth(100);
		
		Element el = findClassInPackage(auxClass, "Navigational");
		if(el == null) {
			el = findClassInPackage(auxClass, "Conceptual");
		}
		
		List<Element> elementosPropriedades = getPropertyElements(el.getOwnedElements());
		
		List<Element> elementoPropriedadeRelacionamento = findRelationsInPackage(auxClass, "Navigational");
		if(elementoPropriedadeRelacionamento.size() == 0) {
			elementoPropriedadeRelacionamento = findRelationsInPackage(iClass, "Conceptual");
		}

		elementosPropriedades.addAll(elementoPropriedadeRelacionamento);

		for (int i = 0; i < elementosPropriedades.size(); i++) {
			TreeColumn instances1 = new TreeColumn(fTree, SWT.LEFT);

			//String aux2[] = aux[i].split("\\.");
			Property ppt = (Property) elementosPropriedades.get(i);
			instances1.setText(StringUtils.capitalize(ppt.getName()));
			instances1.setWidth(600 / elementosPropriedades.size());

		}
		TreeItem item = null;
		
		String stringObjs = getObjects(auxClass);
			

		//if (stringObjs == null)
			//return;
		
	
		
		//String aux3[] = stringObjs.split(",");

		TreeItem[] itens = fTree.getItems();
		for (TreeItem ti : itens) {
			ti.dispose();
		}
		// for (int i = 0; i < fTree.getItems().length; i++) {
		// fTree.getItems()[i].dispose();
		// }

		if(stringObjs == null) {
			MessageBox messageDialog = new MessageBox(contents.getShell(), SWT.ERROR);
			 messageDialog.setText("Erro");
			 messageDialog.setMessage("Esta classe n�o possui objetos!");
			//// messageDialog.setText(e1.getMessage());
			 messageDialog.open();
		}
		fTree.setRedraw(false);
		if(stringObjs.equals("")) {
			//return;
		}
		JsonObject objMor = new JsonObject();
		objMor = Json.parse(stringObjs).asObject();
		JsonArray items = objMor.get("objs").asArray();
		
		String aux5[] = new String[items.size() + 1];
		String aux6[] = new String[items.size() + 1];
		int i = 0; 
		
		for (JsonValue item1 : items) {
			//objs.add(item.asObject().get("obj").asString());
			
			int j = 0 ;
			int p = 0 ;
			aux5 = new String[item1.asObject().names().size() + 1];
			aux6 = new String[item1.asObject().names().size() + 1];
			
			aux5[0] = auxClass.substring(0, 1).toLowerCase() + i;
			aux6[0] = auxClass.substring(0, 1).toLowerCase() + i;
			for(String str: item1.asObject().names()) {
				if(item1.asObject().get(str).isString()) {
					if(!str.equals("id_instance")){
						aux5[p + 1] = item1.asObject().get(str).asString();
						p++;
					}
					aux6[j + 1] = "\""+str + "\":\"" + item1.asObject().get(str).asString()+"\"";
				}else if(item1.asObject().get(str).isObject()) {
					JsonObject objL = item1.asObject().get(str).asObject();
					String strAux1 = "";
					String strAux2 = "";
					int k = 0 ;
					String id_instance ="";
					String relation ="";
					//buscar objeto pelo id.
					
					strAux1 = objL.get("id_instance").asString();
//					for(String str1 : objL.names()) {
//						strAux1 += objL.get(str1).asString();
//						strAux2 += str1 + ":" + objL.get(str1).asString();
//						if(k != objL.names().size()-1) {
//							strAux1 += " - ";
//							strAux2 +=  ", ";
//							k++;
//						}
//					}
					aux5[p + 1] = strAux1;
					aux6[j + 1] = str+ ":" +item1.asObject().get(str).toString();
					p++;
				}
				
				j++;
			}
			item = new TreeItem(fTree, SWT.NONE);
			item.setText(aux5);
			aux6[0] = item1.asObject().toString();
			item.setData(aux6);
			i++;
		}
		//for (int i = 0; i < aux3.length; i++) {

			//String aux4[] = aux3[i].split("\\.");
			
			
//			for (int j = 0; j < aux4.length; j++) {
//				
//			}
			

			//aux4 = null;
			aux5 = null;
			aux6 = null;
	
		return contents;
	}

	


	@Override
	protected void okPressed() {
		
		for (int i = 0; i < textos.size(); i++) {
			if( textos.get(i).getText().equals("")) {
				MessageBox messageDialog = new MessageBox(super.getShell(), SWT.ERROR);
				messageDialog.setText("Erro");
				messageDialog.setMessage("Todos os campos s�o obrigat�rios!");
				messageDialog.open();
				return;
			}
}
		
			super.okPressed();

	}
}
