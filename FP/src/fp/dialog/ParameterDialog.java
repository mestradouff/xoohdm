package fp.dialog;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.designer.languages.common.base.GenUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.SelectionStatusDialog;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Property;

public class ParameterDialog extends OOHDMSelectionStatusDialog {

	public ParameterDialog(Shell parent, EObject eObject) {
			super(parent,eObject);
		// TODO Auto-generated constructor stub
		this.eo = eObject;
		this.parent = parent;
	}

	Shell parent;
	private EObject eo;
//	private Tree fTree;
//
//	private Label fLabel;
//
//	private Combo fAlloc;
//
//	private InstanceSpecification currentIS;
//
//	private EList<InstanceSpecification> nodeOrThreadList;
//	private TreeViewer fTreeViewer;
	protected Combo c1;
	protected Combo c2;
	protected List<Element> elementosClasses;
	protected List<Element> elementosPropriedades;
	@Override
	protected void computeResult() {
		// TODO Auto-generated method stub
		List<String> aux = new ArrayList<String>();
		String str = ((Class)elementosClasses.get(c1.getSelectionIndex())).getName();
		String str2 = ((Property)elementosPropriedades.get(c2.getSelectionIndex())).getName();
		aux.add(str+"."+str2);
		
		this.setResult(aux);
	}

	@Override
	public Control createDialogArea(Composite parent) {
		Composite contents = (Composite) super.createDialogArea(parent);
		contents.setLayout(new GridLayout(2, false));
		this.setTitle("Selecione um par�metro");
		parent.getShell().setText("Selecione um par�metro");
		Element e1 = ((Element) eo).getModel().getOwner();
		
		List<Element> elementos = null;
		//if (GenUtils.hasStereotype((Element) eo, "OohdmProfile::Index")) {
			elementos = getModelConceptualElements(e1.getOwnedElements());
		//}else{
	//		elementos = getModelNavigationalElements(e1.getOwnedElements());
		//}
		
		//List<Element> elementos = getModelNavigationalElements(e1.getOwnedElements());
		Element e2 = elementos.get(0);
		elementosClasses = getClassElements(e2.getOwnedElements());
		
		Label l = new org.eclipse.swt.widgets.Label(contents, 1);
		l.setText("Classe:");
		
		c1 = new Combo(contents, SWT.READ_ONLY);
	    c1.setBounds(50, 50, 150, 65);
	    
	    Label l2 = new org.eclipse.swt.widgets.Label(contents, 1);
		l2.setText("Par�metro:");
		
	    c2 = new Combo(contents, SWT.READ_ONLY);
	    c2.setBounds(50, 85, 150, 65);
	    c2.setEnabled(false);
	   
	    c1.setItems(getArrayElementos(elementosClasses));
	    c1.addSelectionListener(new SelectionAdapter() {
	      public void widgetSelected(SelectionEvent e) {
	    	
	    	int index = c1.getSelectionIndex();
	    	  
	    	elementosPropriedades = getPropertyElements(elementosClasses.get(index).getOwnedElements());
	    	
//	    	List<Element> elementoPropriedadeRelacionamento = findRelationsInPackage(((Class)elementosClasses.get(index)).getName(), "Navigational");
//			elementosPropriedades.addAll(elementoPropriedadeRelacionamento);
			
	          c2.setItems(getArrayElementos(elementosPropriedades));
	          c2.setEnabled(true);
	       

	      }
	    });
		
		
//		fTree = new Tree(contents, SWT.CHECK | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL |SWT.MULTI);
//		fTree.setSize(990, 999);
//		contents.setSize(999, 999);
//		contents.setLayout(new GridLayout(1, false));
//		fTreeViewer =  new TreeViewer(fTree);
//		fTreeViewer.setContentProvider(new ParamsDialogProvider());
//		fTreeViewer.setInput(elementos2);
//		fTreeViewer.setLabelProvider(new ColumnLabelProvider());
//		
//		//fTreeViewer.setContentProvider(new ParamsDialogProvider());
////		Element e2 = elementos.get(loopIndex0);
////		List<Element> elementos2 = getClassElements(e2.getOwnedElements());
//	//	fTreeViewer.setSelection(new StructuredSelection(elementos), true);
		
	
		
		
		
//		for (int loopIndex0 = 0; loopIndex0 < elementos.size(); loopIndex0++) {
//			TreeItem treeItem0 = new TreeItem(fTree, 1);
//			Element e2 = elementos.get(loopIndex0);
//			List<Element> elementos2 = getClassElements(e2.getOwnedElements());
//			treeItem0.setText(elementos.get(loopIndex0).getModel().getName().toString());
//			treeItem0.setData(elementos.get(loopIndex0).getModel());
//			for (int loopIndex1 = 0; loopIndex1 < elementos2.size(); loopIndex1++) {
//				TreeItem treeItem1 = new TreeItem(treeItem0, 0);
//				treeItem1.setText(((ClassImpl)elementos2.get(loopIndex1)).getName().toString());
//
//			}
//		}

		// (parent, "Container rules", "Avail. extensions/interceptors");

		// fTree = new Tree(contents, SWT.H_SCROLL | SWT.BORDER);
		// fTree.setHeaderVisible(true);
		// GridData data = new GridData(GridData.FILL_BOTH);
		// fTree.setLayoutData(data);
		// data.heightHint = 150;
		// // data.widthHint = 200;
		//
		// // Turn off drawing to avoid flicker
		// fTree.setRedraw(false);
		//
		// TreeColumn instances = new TreeColumn(fTree, SWT.LEFT);
		// instances.setText("Instance");
		// instances.setWidth(200);
		// TreeColumn explicitAlloc = new TreeColumn(fTree, SWT.LEFT);
		// explicitAlloc.setText("explicit allocation");
		// explicitAlloc.setWidth(150);
		// TreeColumn implicitAlloc = new TreeColumn(fTree, SWT.LEFT);
		// implicitAlloc.setText("implicit allocation");
		// implicitAlloc.setWidth(150);
		//
		//// fillTree(fTree, null, m_cdp.getMainInstance());
		//
		// // Turn drawing back on!
		// fTree.setRedraw(true);
		//
		// fTree.addSelectionListener(new SelectionAdapter() {
		//
		// @Override
		// public void widgetSelected(SelectionEvent e) {
		// if (fTree.getSelection().length > 0) {
		//// selectInstance(fTree.getSelection()[0]);
		// }
		// }
		// });
		//
		// Label label = new Label();
		// label.setText("seletected instance:");
		// fLabel = new Label();

		// createAllocInfo(contents);
		return contents;
	}

//	private String[] getArrayElementos(List<Element> elementos2) {
//
//		List<String> fdstring = new ArrayList<String>();
//		
//
//		for (Element ele : elementos2) {
//			
//		
//			if(ele instanceof Class) {
//				fdstring.add(((Class)ele).getName());
//			}else if(ele instanceof Property) {
//				fdstring.add(((Property)ele).getName());
//			}
//			
//		}
//		String[] simpleArray = new String[ fdstring.size() ];
//		
//		return fdstring.toArray( simpleArray );
//	}
//
//	private List<Element> getModelNavigationalElements(EList<Element> ownedElements) {
//
//		List<Element> aux = new ArrayList<Element>();
//		// aux.removeAll(ownedElements);
//		for (Element element : ownedElements) {
//			if (element.getModel().getName().equals("Navigational")) {//TODO CRIAR CONSTANTE
//
//				if (element instanceof org.eclipse.uml2.uml.Model) {
//					aux.add(element);
//				}
//			}
//		}
//		return aux;
//	}
//	
//	private List<Element> getClassElements(EList<Element> ownedElements) {
//
//		List<Element> aux = new ArrayList<Element>();
//		// aux.removeAll(ownedElements);
//		for (Element element : ownedElements) {
//
//				if (element instanceof org.eclipse.uml2.uml.Class) {
//					aux.add(element);
//				}
//		}
//		return aux;
//	}
//	
//	private List<Element> getPropertyElements(EList<Element> ownedElements) {
//
//		List<Element> aux = new ArrayList<Element>();
//		// aux.removeAll(ownedElements);
//		for (Element element : ownedElements) {
//
//				if (element instanceof org.eclipse.uml2.uml.Property) {
//					aux.add(element);
//				}
//		}
//		return aux;
//	}
	
	@Override
	protected void okPressed() {
		// TODO Auto-generated method stub
		System.out.println(c1.getSelectionIndex());
		if( c1.getSelectionIndex() == -1 || c2.getSelectionIndex() == -1) {
			MessageBox messageDialog = new MessageBox(super.getShell(), SWT.ERROR);
			messageDialog.setText("Erro");
			messageDialog.setMessage("Todos os campos s�o obrigat�rios!");
			messageDialog.open();
			
		}else {
			super.okPressed();
		}
		
	}

}
