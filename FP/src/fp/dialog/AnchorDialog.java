package fp.dialog;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.designer.languages.common.base.GenUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.SelectionStatusDialog;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Property;

public class AnchorDialog extends SelectionStatusDialog{


	private EObject eo;
	protected Combo c3;
	protected List<Element> elementosClassesDestino;
	
	public AnchorDialog(Shell parent, EObject eObject) {
		super(parent);
		// TODO Auto-generated constructor stub
		this.eo = eObject;
	}
	
	@Override
	protected void computeResult() {
		List<String> aux = new ArrayList<String>();
		String str3 = "";		
		Element ele = ((Class) elementosClassesDestino.get(c3.getSelectionIndex()));
		
		if (GenUtils.hasStereotype(ele, "OohdmProfile::Index")) { //TODO CRIAR CONSTANTE
			str3 = "Idx "+((Class) ele).getName();
		}else if (GenUtils.hasStereotype(ele, "OohdmProfile::Context")) {
			str3 = "Ctx "+((Class) ele).getPackage().getName()+"."+((Class) ele).getName();
		}
		
		aux.add(str3);

		this.setResult(aux);
		
	}
	
	@Override
	public Control createDialogArea(Composite parent) {
		Composite contents = (Composite) super.createDialogArea(parent);
		contents.setLayout(new GridLayout(2, false));
		this.setTitle("Selecione uma �ncora");
		parent.getShell().setText("Selecione uma �ncora");
		Element e1 = ((Element) eo).getModel().getOwner();
		List<Element> elementos = getModelNavigationalElements(e1.getOwnedElements());
		
		
		Label l3 = new org.eclipse.swt.widgets.Label(contents, 1);
		l3.setText("Destino:");

		List<Element> elementosCtx = getModelContextElements(e1.getOwnedElements());
		Element e3 = elementosCtx.get(0);
		elementosClassesDestino = getCtxIdxElements(e3.getOwnedElements());

		c3 = new Combo(contents, SWT.READ_ONLY);
		c3.setBounds(50, 50, 150, 65);
		c3.setItems(getArrayElementos(elementosClassesDestino));

		return contents;
	}

	private String[] getArrayElementos(List<Element> elementos2) {

		List<String> fdstring = new ArrayList<String>();

		for (Element ele : elementos2) {

			if (ele instanceof org.eclipse.uml2.uml.Class) {
				if (GenUtils.hasStereotype(ele, "OohdmProfile::Index")) { // TODO CRIAR CONSTANTE
					fdstring.add("Idx "+((org.eclipse.uml2.uml.Class) ele).getName());
				}else if (GenUtils.hasStereotype(ele, "OohdmProfile::Context")) { // TODO CRIAR CONSTANTE
					fdstring.add("Ctx "+((Class) ele).getPackage().getName()+"."+((Class) ele).getName());
				}else {
					fdstring.add(((Class) ele).getName());
				}
				
			} else if (ele instanceof Property) {
				fdstring.add(((Property) ele).getName());
			} 

		}
		String[] simpleArray = new String[fdstring.size()];

		return fdstring.toArray(simpleArray);
	}
	
	private List<Element> getCtxIdxElements(List<Element> classElements) {
		List<Element> aux = new ArrayList<Element>();
		// aux.removeAll(ownedElements);
		
		for (Element element : classElements) {

			if (element instanceof org.eclipse.uml2.uml.Class) {
				if (GenUtils.hasStereotype(element, "OohdmProfile::Index")) { // TODO CRIAR CONSTANTE
					aux.add(element);
				}
			}
			if (element instanceof org.eclipse.uml2.uml.Package) {
				if (GenUtils.hasStereotype(element, "OohdmProfile::NavigationalClass")) {// TODO CRIAR CONSTANTE
					for (Element elementctx : element.getOwnedElements()) {
						if (GenUtils.hasStereotype(elementctx, "OohdmProfile::Context")) {// TODO CRIAR CONSTANTE
							aux.add(elementctx);
						}
					}
					
				}
			}
		}
		return aux;
	}
	
	private List<Element> getModelNavigationalElements(EList<Element> ownedElements) {

		List<Element> aux = new ArrayList<Element>();
		// aux.removeAll(ownedElements);
		for (Element element : ownedElements) {
			if (element.getModel().getName().equals("Navigational")) {

				if (element instanceof org.eclipse.uml2.uml.Model) {
					aux.add(element);
				}
			}
		}
		return aux;
	}

	private List<Element> getModelContextElements(EList<Element> ownedElements) {

		List<Element> aux = new ArrayList<Element>();
		// aux.removeAll(ownedElements);
		for (Element element : ownedElements) {
			if (element.getModel().getName().equals("Context")) { //TODO CRIAR CONSTANTE
				if (element instanceof org.eclipse.uml2.uml.Model) {
					aux.add(element);
				}
			}
		}
		return aux;
	}
	
	@Override
	protected void okPressed() {
		
		if( c3.getSelectionIndex() == -1) {
			MessageBox messageDialog = new MessageBox(super.getShell(), SWT.ERROR);
			messageDialog.setText("Erro");
			messageDialog.setMessage("Todos os campos s�o obrigat�rios!");
			messageDialog.open();
			
		}else {
			super.okPressed();
		}
		
	}

}
