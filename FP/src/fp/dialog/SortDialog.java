package fp.dialog;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.designer.languages.common.base.GenUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.SelectionStatusDialog;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Class;

public class SortDialog extends OOHDMSelectionStatusDialog {

	public SortDialog(Shell parent, EObject eObject) {
		super(parent,eObject);
		// TODO Auto-generated constructor stub
		this.eo = eObject;
	}

	private EObject eo;

	protected Combo c1;
	protected Combo c2;
	protected Combo c3;
	protected List<Element> elementosClasses;
	protected List<Element> elementosPropriedades;
	protected String[] elementosClassesDestino;

	@Override
	protected void computeResult() {
		// TODO Auto-generated method stub
		List<String> aux = new ArrayList<String>();
		String str = ((Class) elementosClasses.get(c1.getSelectionIndex())).getName();
		String str2 = ((Property) elementosPropriedades.get(c2.getSelectionIndex())).getName();
		
		String ele = elementosClassesDestino[c3.getSelectionIndex()];
				
		aux.add(str + "." + str2 + "." + ele);

		this.setResult(aux);
	}

	@Override
	public Control createDialogArea(Composite parent) {
		Composite contents = (Composite) super.createDialogArea(parent);
		contents.setLayout(new GridLayout(2, false));
		this.setTitle("Selecione uma Ordena��o");
		parent.getShell().setText("Selecione uma Ordena��o");
		Element e1 = ((Element) eo).getModel().getOwner();
		List<Element> elementos = null;
		if (GenUtils.hasStereotype((Element) eo, "OohdmProfile::Index")) {
			elementos = getModelConceptualElements(e1.getOwnedElements());
		}else{
			elementos = getModelNavigationalElements(e1.getOwnedElements());
		}
		
		//List<Element> elementos = getModelNavigationalElements(e1.getOwnedElements());
		Element e2 = elementos.get(0);
		elementosClasses = getClassElements(e2.getOwnedElements());

		Label l = new org.eclipse.swt.widgets.Label(contents, 1);
		l.setText("Classe:");

		c1 = new Combo(contents, SWT.READ_ONLY);
		c1.setBounds(50, 50, 150, 65);

		Label l2 = new org.eclipse.swt.widgets.Label(contents, 1);
		l2.setText("Parametro:");

		c2 = new Combo(contents, SWT.READ_ONLY);
		c2.setBounds(50, 85, 150, 65);
		c2.setEnabled(false);

		c1.setItems(getArrayElementos(elementosClasses));
		c1.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {

				int index = c1.getSelectionIndex();

				elementosPropriedades = getPropertyElements(elementosClasses.get(index).getOwnedElements());
				c2.setItems(getArrayElementos(elementosPropriedades));
				c2.setEnabled(true);

			}
		});

		Label l3 = new org.eclipse.swt.widgets.Label(contents, 1);
		l3.setText("Tipo de Ordena��o:");

		String[] elementosCtx = getTipoOrdenacaoElements();
		elementosClassesDestino = elementosCtx;

		c3 = new Combo(contents, SWT.READ_ONLY);
		c3.setBounds(50, 50, 150, 65);
		c3.setItems(elementosCtx);
		
		return contents;
	}
	
	

	private String[] getTipoOrdenacaoElements() {

		String[] aux = {"Ascendente", "Descendente"};
		
		return aux;
	}

	

//	private String[] getArrayElementos(List<Element> elementos2) {
//
//		List<String> fdstring = new ArrayList<String>();
//
//		for (Element ele : elementos2) {
//
//			if (ele instanceof org.eclipse.uml2.uml.Class) {
//				if (GenUtils.hasStereotype(ele, "OohdmProfile::Index")) { // TODO CRIAR CONSTANTE
//					fdstring.add("Idx "+((Class) ele).getName());
//				}else if (GenUtils.hasStereotype(ele, "OohdmProfile::Context")) { // TODO CRIAR CONSTANTE
//					fdstring.add("Ctx "+((Class) ele).getPackage().getName()+"."+((Class) ele).getName());
//				}else {
//					fdstring.add(((Class) ele).getName());
//				}
//				
//			} else if (ele instanceof Property) {
//				fdstring.add(((Property) ele).getName());
//			} 
//
//		}
//		String[] simpleArray = new String[fdstring.size()];
//
//		return fdstring.toArray(simpleArray);
//	}

	
//
//	private List<Element> getModelNavigationalElements(EList<Element> ownedElements) {
//
//		List<Element> aux = new ArrayList<Element>();
//		// aux.removeAll(ownedElements);
//		for (Element element : ownedElements) {
//			if (element.getModel().getName().equals("Navigational")) {
//
//				if (element instanceof org.eclipse.uml2.uml.Model) {
//					aux.add(element);
//				}
//			}
//		}
//		return aux;
//	}
//
//	private List<Element> getClassElements(EList<Element> ownedElements) {
//
//		List<Element> aux = new ArrayList<Element>();
//		// aux.removeAll(ownedElements);
//		for (Element element : ownedElements) {
//
//			if (element instanceof org.eclipse.uml2.uml.Class) {
//				aux.add(element);
//			}
//		}
//		return aux;
//	}
//
//	private List<Element> getPropertyElements(EList<Element> ownedElements) {
//
//		List<Element> aux = new ArrayList<Element>();
//		// aux.removeAll(ownedElements);
//		for (Element element : ownedElements) {
//
//			if (element instanceof org.eclipse.uml2.uml.Property) {
//				aux.add(element);
//			}
//		}
//		return aux;
//	}

	@Override
	protected void okPressed() {
		// TODO Auto-generated method stub
		System.out.println(c1.getSelectionIndex());
		if( c1.getSelectionIndex() == -1 || c2.getSelectionIndex() == -1 || c3.getSelectionIndex() == -1) {
			MessageBox messageDialog = new MessageBox(super.getShell(), SWT.ERROR);
			messageDialog.setText("Erro");
			messageDialog.setMessage("Todos os campos s�o obrigat�rios!");
			messageDialog.open();
			
		}else {
			super.okPressed();
		}
		
	}
}
