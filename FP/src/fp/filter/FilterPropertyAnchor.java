package fp.filter;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.jface.viewers.IFilter;
import org.eclipse.papyrus.designer.languages.common.base.GenUtils;
import org.eclipse.papyrus.uml.alf.transaction.observation.listener.filter.FUMLScopeUtil;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Property;

public class FilterPropertyAnchor implements IFilter {

	@Override
	public boolean select(Object toTest) {

		Element element = this.resolveSemanticElement(toTest);
		boolean accepted = false;
		if(element!=null){
			accepted = this.isValidInput(element);
		}
		return accepted;
	}
	
	private boolean isValidInput(Element element){
		
		if(FUMLScopeUtil.isProperty(element)){
			if(isAnchorProperty(element)) {
				return true;
			}
			return false;
		}
		return false;
	}
	
	private boolean isAnchorProperty(Element element) {
		Property pt = (Property)element;
		if(pt.getType() == null) {
			return false;	
		}
		if(pt.getOwner().getModel().getName().equals("Navigational") && pt.getType().getName().equals("Anchor")) {
			return true;
		}
		return false;
	}

	private Element resolveSemanticElement(Object selectedElement){
		Element semanticElement = null;
		if (selectedElement instanceof IAdaptable) {
			semanticElement = (Element) ((IAdaptable) selectedElement).getAdapter(EObject.class);
		}
		else if (selectedElement instanceof GraphicalEditPart) {
			GraphicalEditPart part = (GraphicalEditPart) selectedElement;
			semanticElement = (Element)part.resolveSemanticElement();
		}
		return semanticElement;
	}
}
