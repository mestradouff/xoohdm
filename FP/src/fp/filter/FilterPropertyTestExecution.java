package fp.filter;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.jface.viewers.IFilter;
import org.eclipse.papyrus.designer.languages.common.base.GenUtils;
import org.eclipse.papyrus.infra.core.resource.NotFoundException;
import org.eclipse.papyrus.infra.gmfdiag.css.notation.CSSDiagramImpl;
import org.eclipse.papyrus.uml.alf.transaction.observation.listener.filter.FUMLScopeUtil;
import org.eclipse.papyrus.uml.tools.model.UmlUtils;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Property;

public class FilterPropertyTestExecution implements IFilter {

	@Override
	public boolean select(Object toTest) {
		
		
		Element element = this.resolveSemanticElement(toTest);
		boolean accepted = false;
		if(element!=null){
			accepted = this.isValidInput(element);
		}
		return accepted;
		
	
	}
	
	private boolean isValidInput(Element element){
		
		if(FUMLScopeUtil.isClass(element)){
			if (GenUtils.hasStereotype(element, "OohdmProfile::Context")) {
				return false;
			}
			if (GenUtils.hasStereotype(element, "OohdmProfile::Index")) {
				return true;
			}
			
		}else if(element instanceof Model){
			try {
				if(element == UmlUtils.getUmlModel().lookupRoot()){
					
				return false;
				}
			} catch (NotFoundException e) {
				return false;
			}
		}else if(FUMLScopeUtil.isPackage(element)){
			return false;
		}else if(FUMLScopeUtil.isSignal(element)){
			return false;
		}else if(FUMLScopeUtil.isEnumeration(element)){
			return false;
		}else if(FUMLScopeUtil.isDataType(element)){
			return false;
		}else if(FUMLScopeUtil.isAssociation(element)){
			return false;
		}else if(FUMLScopeUtil.isActivity(element)){
			return false;
		}else if(FUMLScopeUtil.isProperty(element)){
			if(isIndexProperty(element)) {
				return true;
			}
			return false;
		}
		return false;
	}
	
	private boolean isIndexProperty(Element element) {
		Property pt = (Property)element;
		if(pt.getType() == null) {
			return false;	
		}
		if(pt.getOwner().getModel().getName().equals("Navigational") && pt.getType().getName().equals("Index")) {
			return true;
		}
		return false;
	}

	
	private Element resolveSemanticElement(Object selectedElement){
		Element semanticElement = null;
		if (selectedElement instanceof IAdaptable) {
			if (!(selectedElement instanceof CSSDiagramImpl)) {
			semanticElement = (Element) ((IAdaptable) selectedElement).getAdapter(EObject.class);
			}else if (selectedElement instanceof GraphicalEditPart) {
				GraphicalEditPart part = (GraphicalEditPart) selectedElement;
				semanticElement = (Element)part.resolveSemanticElement();
			}
		}
		else if (selectedElement instanceof GraphicalEditPart) {
			GraphicalEditPart part = (GraphicalEditPart) selectedElement;
			semanticElement = (Element)part.resolveSemanticElement();
		}
		return semanticElement;
	}
}
