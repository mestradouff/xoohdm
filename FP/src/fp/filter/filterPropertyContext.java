package fp.filter;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.jface.viewers.IFilter;
import org.eclipse.papyrus.designer.languages.common.base.GenUtils;
import org.eclipse.papyrus.infra.gmfdiag.css.notation.CSSDiagramImpl;
import org.eclipse.papyrus.uml.alf.transaction.observation.listener.filter.FUMLScopeUtil;
import org.eclipse.uml2.uml.Element;

public class filterPropertyContext implements IFilter {

	@Override
	public boolean select(Object toTest) {
		
		
		Element element = this.resolveSemanticElement(toTest);
		boolean accepted = false;
		if(element!=null){
			accepted = this.isValidInput(element);
		}
		return accepted;
		
	
	}
	
	
	/**
	 * Check the given input and returns true if it is a valid input for the embedded ALF editor
	 * false otherwise 
	 * 
	 * @param element
	 * 		  an input element for ALF  embedded editor
	 * 
	 * @return true if element is accepted as an input false otherwise
	 */
	private boolean isValidInput(Element element){
				
		if(FUMLScopeUtil.isClass(element)){
			if (GenUtils.hasStereotype(element, "OohdmProfile::Context")) {
				return true;
			}
			return false;
		}else if(FUMLScopeUtil.isPackage(element)){
			return false;
		}else if(FUMLScopeUtil.isSignal(element)){
			return false;
		}else if(FUMLScopeUtil.isEnumeration(element)){
			return false;
		}else if(FUMLScopeUtil.isDataType(element)){
			return false;
		}else if(FUMLScopeUtil.isAssociation(element)){
			return false;
		}else if(FUMLScopeUtil.isActivity(element)){
			return false;
		}
		return false;
	}
	
	
	/**
	 * From a selection this methods tries to extract the underlying model element
	 * 
	 * @param selectedElement
	 * 		  an object selected in the view (e.g., a class in a diagram)
	 * 
	 * @return semanticElement
	 * 		   the model element that is under the graphical element (may be null)
	 */
	private Element resolveSemanticElement(Object selectedElement){
		Element semanticElement = null;
		if (selectedElement instanceof IAdaptable) {
			if (!(selectedElement instanceof CSSDiagramImpl)) {
			semanticElement = (Element) ((IAdaptable) selectedElement).getAdapter(EObject.class);
			}else if (selectedElement instanceof GraphicalEditPart) {
				GraphicalEditPart part = (GraphicalEditPart) selectedElement;
				semanticElement = (Element)part.resolveSemanticElement();
			}
		}
		else if (selectedElement instanceof GraphicalEditPart) {
			GraphicalEditPart part = (GraphicalEditPart) selectedElement;
			semanticElement = (Element)part.resolveSemanticElement();
		}
		return semanticElement;
	}

}
