package fp.filter;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.jface.viewers.IFilter;
import org.eclipse.papyrus.uml.alf.transaction.observation.listener.filter.FUMLScopeUtil;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Property;

public class FilterPropertyNM implements IFilter {

	@Override
	public boolean select(Object toTest) {

		Element element = this.resolveSemanticElement(toTest);
		boolean accepted = false;
		if(element!=null){
			accepted = this.isValidInput(element);
		}
		return accepted;
	}
	
	private boolean isValidInput(Element element){
		
		
			if(isConceptualModel(element)) {
				if(FUMLScopeUtil.isClass(element)){
					return true;
				}
				if(element instanceof Association){
					//It's a feature function, so return false now;
					return false;
				}
				return false;
			}
		return false;
	}
	
	private boolean isConceptualModel(Element element) {
		
		if(element instanceof org.eclipse.uml2.uml.Class) {
			org.eclipse.uml2.uml.Class pt = (org.eclipse.uml2.uml.Class)element;
			if(pt.getOwner().getModel().getName().equals("Conceptual")) {
				return true;
			}
		}else {
			//org.eclipse.uml2.uml.Association pt = (org.eclipse.uml2.uml.Association)element;
			//if(pt.getOwner().getModel().getName().equals("Conceptual")) {
			//	return true;
			//}
		}
		
		
		
		return false;
	}

	private Element resolveSemanticElement(Object selectedElement){
		Element semanticElement = null;
		if (selectedElement instanceof IAdaptable) {
			semanticElement = (Element) ((IAdaptable) selectedElement).getAdapter(EObject.class);
		}
		else if (selectedElement instanceof GraphicalEditPart) {
			GraphicalEditPart part = (GraphicalEditPart) selectedElement;
			semanticElement = (Element)part.resolveSemanticElement();
		}
		return semanticElement;
	}

}
