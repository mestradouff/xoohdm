package fp.propertySection;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfigurationType;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gmf.runtime.diagram.ui.properties.sections.AbstractModelerPropertySection;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.papyrus.uml.tools.utils.UMLUtil;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Slider;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleListener;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.TextConsole;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Property;
import org.json.JSONArray;
import org.json.JSONObject;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

import org.eclipse.papyrus.moka.utils.constants.MokaConstants;

import fp.dialog.InstanceDialog;
import fp.dialog.ParameterDialog;
import fp.util.CommentUtil;
import fp.util.MokaUtil;
import fp.util.Parameter;
import fp.util.TypeComment;
import fp.util.UmlUtil;
import fp.xtext.AlfCreator;

public class TestExecutionPropertySection extends AbstractModelerPropertySection {

	public TestExecutionPropertySection() {
		instance = this;
		nivel = 0;
		niveisbotao = new HashMap<Integer, Button>();
		niveisParametro = new HashMap<Integer, String>();
		// container2 = null;
		// for(Control ct :container2.getChildren()) {
		// ct.dispose();
		// }
	}

	private int nivel = 0;
	private Map<Integer, Button> niveisbotao = new HashMap<Integer, Button>();
	private Map<Integer, String> niveisParametro = new HashMap<Integer, String>();

	TestExecutionPropertySection instance;
	Tree fTree;
	Element testBehavior;
	CommentUtil commentUtil;
	private UmlUtil umlUtil = new UmlUtil();
	TabbedPropertySheetPage aTabbedPropertySheetPage;
	String ctxOrIdx = "";
	Element elementCtx;
	// StyledText textControl;

	IConsoleListener consListener = new IConsoleListener() {
		public void consolesAdded(IConsole[] consoles) {
			Display.getDefault().syncExec(new Runnable() {
				public void run() {
					console[0] = ((TextConsole) consoles[0]);
					// console[0].activate();
					// String output = console[0].getDocument().get();
					// System.out.println(output);
					// testC.setText(output);
				}
			});

		}

		public void consolesRemoved(IConsole[] consoles) {
		}
	};

	@Override
	public void refresh() {
		super.refresh();
		EList<Comment> eList = commentUtil.getComments(0);

		this.refreshDataField(eList);
		
//descomentar
		int i;
		Control[] arrCont = container5.getChildren();
		for (i = arrCont.length-1; i >= 0; i--) {
				excluirFilhosContainer(container5.getChildren()[i]);
		}
		nivel = 0;
		niveisbotao = new HashMap<Integer, Button>();
		niveisParametro = new HashMap<Integer, String>();


	}

	private void refreshDataField(EList<Comment> eList) {

		fTree.setRedraw(true);
		for (TreeColumn tc : fTree.getColumns()) {
			tc.dispose();
		}
		// commentUtil = new CommentUtil(getEObject());
		String string = commentUtil.getCommentType(TypeComment.ATTRIBUTS.getName());
		if (string == null)
			return;
		String aux[] = string.split(",");

		TreeColumn instances = new TreeColumn(fTree, SWT.LEFT);
		String auxClass = aux[0].split("\\.")[0];
		instances.setText(auxClass);
		instances.setWidth(100);
		Element el = null;
		el = findClassInPackage(auxClass, "Navigational");
		if(el == null) {
			el = findClassInPackage(auxClass, "Conceptual");
		}

		List<Element> elementosPropriedades = getPropertyElements(
				el.getOwnedElements());

		List<Element> elementoPropriedadeRelacionamento = findRelationsInPackage(auxClass, "Navigational");
		if(elementoPropriedadeRelacionamento.size() == 0) {
			elementoPropriedadeRelacionamento = findRelationsInPackage(auxClass, "Conceptual");
		}
		
		
		elementosPropriedades.addAll(elementoPropriedadeRelacionamento);

		for (int i = 0; i < elementosPropriedades.size(); i++) {
			

			// String aux2[] = aux[i].split("\\.");
			Property ppt = (Property) elementosPropriedades.get(i);
			if(ppt.getType().getName().equals("Anchor") || ppt.getType().getName().equals("Index") && ppt.getType().getName().equals("Context") ) {
				//do nothing.
			}else {
				TreeColumn instances1 = new TreeColumn(fTree, SWT.LEFT);
			instances1.setText(StringUtils.capitalize(ppt.getName()));
			instances1.setWidth(600 / elementosPropriedades.size());
			}

		}
		TreeItem item = null;

		String stringObjs = getObjects(auxClass);

		if (stringObjs == null)
			return;

		// String aux3[] = stringObjs.split(",");

		TreeItem[] itens = fTree.getItems();
		for (TreeItem ti : itens) {
			ti.dispose();
		}
		// for (int i = 0; i < fTree.getItems().length; i++) {
		// fTree.getItems()[i].dispose();
		// }

		fTree.setRedraw(false);
		if (stringObjs.equals("")) {
			return;
		}
		JsonObject objMor = new JsonObject();
		objMor = Json.parse(stringObjs).asObject();
		JsonArray items = objMor.get("objs").asArray();

		String aux5[] = new String[items.size() + 1];
		String aux6[] = new String[items.size() + 1];
		int i = 0;

		for (JsonValue item1 : items) {
			// objs.add(item.asObject().get("obj").asString());

			int j = 0;
			int p = 0;
			aux5 = new String[item1.asObject().names().size() + 1];
			aux6 = new String[item1.asObject().names().size() + 1];

			aux5[0] = auxClass.substring(0, 1).toLowerCase() + i;
			aux6[0] = auxClass.substring(0, 1).toLowerCase() + i;
			for (String str : item1.asObject().names()) {
				if (item1.asObject().get(str).isString()) {
					if (!str.equals("id_instance")) {
						aux5[p + 1] = item1.asObject().get(str).asString();
						p++;
					}
					aux6[j + 1] = str + ":" + item1.asObject().get(str).asString();
				} else if (item1.asObject().get(str).isObject()) {
					JsonObject objL = item1.asObject().get(str).asObject();
					String strAux1 = "";
					String strAux2 = "";
					String id_instance = objL.get("id_instance").asString();
					int k = 0;
					String texto = getObjectbyInstanceId(str, id_instance);

					String relation = "";
					// buscar objeto pelo id.

					strAux1 = objL.get("id_instance").asString();
					// for(String str1 : objL.names()) {
					// strAux1 += objL.get(str1).asString();
					// strAux2 += str1 + ":" + objL.get(str1).asString();
					// if(k != objL.names().size()-1) {
					// strAux1 += " - ";
					// strAux2 += ", ";
					// k++;
					// }
					// }
					aux5[p + 1] = texto;
					aux6[j + 1] = str + ":" + item1.asObject().get(str).toString();
					p++;
				}

				j++;
			}
			item = new TreeItem(fTree, SWT.NONE);
			item.setText(aux5);
			aux6[0] = item1.asObject().toString();
			item.setData(aux6);
			i++;
		}
		// for (int i = 0; i < aux3.length; i++) {

		// String aux4[] = aux3[i].split("\\.");

		// for (int j = 0; j < aux4.length; j++) {
		//
		// }

		// aux4 = null;
		aux5 = null;
		aux6 = null;
		// }
		// aux3 = null;

	}

	private String getObjectbyInstanceId(String classOfObj, String id_instance) {

		String stringObjs = getObjects(StringUtils.capitalize(classOfObj));

		JsonObject objMor = new JsonObject();
		objMor = Json.parse(stringObjs).asObject();
		JsonArray items = objMor.get("objs").asArray();
		String ret = "";
		for (JsonValue item1 : items) {
			if (item1.asObject().get("id_instance").asString().equals(id_instance)) {

				for (String str : item1.asObject().names()) {
					if (item1.asObject().get(str).isString()) {
						if (!str.equals("id_instance")) {
							ret += item1.asObject().get(str).asString() + " - ";

						}
					}
				}
				return ret;
			}

		}
		return null;
	}

	private String getJsonObjectbyInstanceId(String classOfObj, String id_instance) {

		String stringObjs = getObjects(StringUtils.capitalize(classOfObj));

		JsonObject objMor = new JsonObject();
		objMor = Json.parse(stringObjs).asObject();
		JsonArray items = objMor.get("objs").asArray();
		String ret = "";
		for (JsonValue item1 : items) {
			if (item1.asObject().get("id_instance").asString().equals(id_instance)) {

				for (String str : item1.asObject().names()) {
					if (item1.asObject().get(str).isString()) {
						if (!str.equals("id_instance")) {
							ret += item1.asObject().get(str).asString() + " - ";

						}
					}
				}
				return item1.asObject().toString();
			}

		}
		return null;
	}

	private String getObjects(String auxClass) {

		Class cl = null;
		if (findClassInPackage(auxClass + "Dao", "Tests") == null) {
			Element el = null;
			el = findNavigationalClass(auxClass);
			if(el == null) {
				el = findConceptualClass(auxClass);
			}
			
			Element behaviorDaoTest = createDaoTest((Class)el);
			cl = (Class) ((Activity) behaviorDaoTest).getSpecification().getOwner();
		} else {
			cl = findClassInPackage(auxClass + "Dao", "Tests");
		}

		String str = commentUtil.getCommentType(TypeComment.OBJECTS.getName(), cl);

		return str;
	}

	@Override
	public void setInput(IWorkbenchPart part, ISelection selection) {
		super.setInput(part, selection);

		if (selection instanceof IStructuredSelection) {
			Object input = ((IStructuredSelection) selection).getFirstElement();

			Element semanticElement = UMLUtil.resolveUMLElement(input);
			if (semanticElement != null) {
				setEObject(semanticElement);
				commentUtil = new CommentUtil(semanticElement);
				// refresh();
				// String srP = commentUtil.getCommentType(TypeComment.PARAMETERS.getName());
				if (possuiParametros()) {
					btnEx.setEnabled(false);
				} else {
					btnEx.setEnabled(true);
				}
			}
		}

	}

	private boolean possuiParametros() {
		String srP = commentUtil.getCommentType(TypeComment.PARAMETERS.getName());
		if (srP != null) {
			return true;
		} else {
			return false;
		}
	}

	private boolean possuiParametros(EObject eobject) {
		String srP = commentUtil.getCommentType(TypeComment.PARAMETERS.getName(), eobject);
		if (srP != null) {
			return true;
		} else {
			return false;
		}
	}

	private ModifyListener objectsListener = new ModifyListener() {

		@Override
		public void modifyText(ModifyEvent e) {
			TransactionalEditingDomain domain = getEditingDomain();
			domain.getCommandStack().execute(new RecordingCommand(domain) {

				@Override
				protected void doExecute() {

					commentUtil.saveCommentModel(objs.getText(), TypeComment.OBJECTS.getValue());
				}

				@Override
				protected void postExecute() {
					super.postExecute();
					this.dispose();
				}
			});

		}
	};

	private FocusListener fListener = new FocusListener() {

		@Override
		public void focusLost(FocusEvent e) {
			//
			// refresh();
			// AlfCompiler compile = new AlfCompiler();
			// try {
			// compile.parse(getTestText());
			// } catch (ParsingError e1) {
			// // TODO Auto-generated catch block
			// e1.printStackTrace();
			// MessageBox messageDialog = new MessageBox(container2.getShell(), SWT.ERROR);
			// messageDialog.setText("Erro");
			// messageDialog.setMessage(e1.getMessage());
			//// messageDialog.setText(e1.getMessage());
			// messageDialog.open();
			// }

			// Element umlElement = (Element) selected;
			// Resource resource = umlElement.eResource();
			// Resource original = (org.eclipse.emf.ecore.resource.Resource) resource;
			// if(original.getErrors().size() != 0 || original.getWarnings().size() != 0 ||
			// Diagnostician.INSTANCE.validate(resource.getContents().get(0)).getSeverity()
			// != Diagnostic.OK){
			// // input invalid
			// } else {
			// // input valid
			// }
		}

		@Override
		public void focusGained(FocusEvent e) {
			// TODO Auto-generated method stub

		}
	};

	private ModifyListener testListener = new ModifyListener() {

		@Override
		public void modifyText(ModifyEvent e) {
			TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(getEObject());
			domain.getCommandStack().execute(new RecordingCommand(domain) {

				@Override
				protected void doExecute() {

					// commentUtil.saveCommentModel(test.getText(), TypeComment.TESTS.getValue());
				}

				@Override
				protected void postExecute() {
					super.postExecute();
					this.dispose();
				}
			});

		}

	};
	private Text objs;
	// private Text test;
	// private Text testC;
	private TextConsole[] console;
	private IConsoleManager manager;
	private Composite container2;
	private Composite container5;
	private Button btnEx;
	protected Element testeModel;
	protected Element daoBehavior;
	protected Element daoClass;
	protected Element daoOperation;
	protected String itemSelectedAnterior;

	@Override
	public final void createControls(Composite parent, TabbedPropertySheetPage aTabbedPropertySheetPage) {
		super.createControls(parent, aTabbedPropertySheetPage);
		this.aTabbedPropertySheetPage = aTabbedPropertySheetPage;
		container2 = null;
		container2 = getWidgetFactory().createFlatFormComposite(parent);

		GridLayout layout = new GridLayout(3, false);

		container2.setLayout(layout);

		Label labelob = new Label(container2, SWT.NONE);
		labelob.setText("Instanciate Objects to test");

		Button button = new Button(container2, SWT.TOGGLE);
		button.setToolTipText("Adicionar objeto!");
		button.setImage(new Image(Display.getDefault(), getClass().getResourceAsStream("/icons/Plus.gif")));
		button.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				InstanceDialog pd = new InstanceDialog(container2.getShell(), getEObject());

				pd.open();

				Object[] obj = pd.getResult();
				// String[] simpleArray = new String[obj.length];
				//
				// return fdstring.toArray( simpleArray );
				Timestamp timestamp = new Timestamp(System.currentTimeMillis());
				String newinstance = "\"id_instance\":\"" + timestamp.getTime() + "\",";
				if (obj != null) {
					String string = commentUtil.getCommentType(TypeComment.ATTRIBUTS.getName());
					String aux[] = string.split(",");
					String iClass = aux[0].split("\\.")[0];
					// String stringObjs =
					// commentUtil.getCommentType(TypeComment.OBJECTS.getName());
					// newinstance += iClass;

					Element el = findClassInPackage(iClass, "Navigational");
					if(el == null) {
						el = findClassInPackage(iClass, "Conceptual");
					}
					
					List<Element> elementosPropriedades = getPropertyElements(
							el.getOwnedElements());

					List<Element> elementoPropriedadeRelacionamento = findRelationsInPackage(iClass, "Navigational");
					if(elementoPropriedadeRelacionamento.size() == 0) {
						elementoPropriedadeRelacionamento = findRelationsInPackage(iClass, "Conceptual");
					}

					elementosPropriedades.addAll(elementoPropriedadeRelacionamento);

					for (int i = 0; i < obj.length; i++) {
						// String aux2[] = aux[i].split("\\.");
						Property ppt = (Property) elementosPropriedades.get(i);
						newinstance += // ppt.getName() + ":" +
								obj[i];
						if (i != obj.length - 1) {
							newinstance += ",";
						}
					}

					adicionarInstancia("{" + newinstance + "}", iClass);
					refresh();
				}

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

			}
		});

		Button buttonExcParam = new Button(container2, SWT.TOGGLE);
		buttonExcParam.setToolTipText("Remover objeto!");
		buttonExcParam.setImage(new Image(Display.getDefault(), getClass().getResourceAsStream("/icons/remove.gif")));
		buttonExcParam.setEnabled(false);
		buttonExcParam.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				String string = commentUtil.getCommentType(TypeComment.ATTRIBUTS.getName());
				String aux[] = string.split(",");
				String iClass = aux[0].split("\\.")[0];
				removerInstancia(iClass);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

			}
		});

		Composite container3 = getWidgetFactory().createFlatFormComposite(container2);
		container3.setLayout(new GridLayout(1, false));

		// parametrosText = new Text(container2, SWT.MULTI | SWT.BORDER | SWT.WRAP |
		// SWT.V_SCROLL);
		// parametrosText.setLayoutData(parametrosData);

		fTree = new Tree(container3, SWT.H_SCROLL | SWT.BORDER);
		fTree.setHeaderVisible(true);
		fTree.setTouchEnabled(false);
		GridData data = new GridData(GridData.BEGINNING);
		data.heightHint = 110;
		fTree.setLayoutData(data);

		fTree.setRedraw(true);

		fTree.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (fTree.getSelection().length > 0) {
					buttonExcParam.setEnabled(true);
				}
			}
		});

		Composite container4 = getWidgetFactory().createFlatFormComposite(container3);
		container4.setLayout(new GridLayout(2, false));
		Button btnCommit = new Button(container4, SWT.PUSH);
		// btnEx.setText("Execute");
		btnCommit.setToolTipText("Compile Test.");
		btnCommit.setImage(new Image(Display.getDefault(), getClass().getResourceAsStream("/icons/validate.gif")));

		btnEx = new Button(container4, SWT.PUSH);
		btnEx.setVisible(false);
		// btnEx.setText("Execute");
		btnEx.setToolTipText("Execute test.");
		btnEx.setImage(new Image(Display.getDefault(), getClass().getResourceAsStream("/icons/run.gif")));

		// Label label2 = new Label(container2, SWT.NONE);
		// label2.setText("");

		container5 = getWidgetFactory().createFlatFormComposite(container4);
		container5.setLayout(new GridLayout(1, false));

//		remover daki depois
//		gerarNovoContexto("", container4);
		
		// Label label3 = new Label(container5, SWT.NONE);
		// label3.setText("Result of Test");

		// testC = new Text(container5, SWT.MULTI | SWT.BORDER | SWT.WRAP |
		// SWT.V_SCROLL);

		GridData elementosData = new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 5);
		elementosData.heightHint = 300;
		elementosData.widthHint = 450;
		elementosData.verticalSpan = 15;
		// testC.setLayoutData(elementosData);

		manager = ConsolePlugin.getDefault().getConsoleManager();
		console = new TextConsole[1];
		manager.addConsoleListener(consListener);

		// textControl = new StyledText(container2, SWT.MULTI | SWT.BORDER
		// | SWT.V_SCROLL | SWT.WRAP);
		// textControl.setVisible(false);

		btnEx.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				// new Thread(new Runnable() {
				// public void run() {
				//
				// Display.getDefault().syncExec(new Runnable() {
				// public void run() {
				// testC.setText("");
				IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				IEditorPart editor = page.getActiveEditor();
				page.saveEditor(editor, false /* confirm */);
				System.out.println("init moka");

				MokaUtil mu = new MokaUtil();
				MokaConstants.MOKA_AUTOMATIC_ANIMATION = false;
				mu.executeMoka();
				System.out.println("fim moka");

				// testC.setText(
				String resultJson = mu.updateConsole();


				Label labelResult = new Label(container5, SWT.NONE);
				labelResult.setText("Result of Test");
				
				gerarNovoIndice(resultJson, container5);
				
				

				
				// aTabbedPropertySheetPage.refresh();
				page.saveEditor(editor, false /* confirm */);
				System.out.println("passou 6, salvou");
				// }
				// });
				// }
				//
				// }).start();
				// container2.layout(true, true);
				//
				// container2.requestLayout();
				// container2.redraw();
				// refresh();

				aTabbedPropertySheetPage.resizeScrolledComposite();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

			}
		});
		btnCommit.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
				IEditorPart editor = page.getActiveEditor();
				page.saveEditor(editor, false /* confirm */);
				new Thread(new Runnable() {
					public void run() {

						Display.getDefault().syncExec(new Runnable() {
							public void run() {
								// IProject pj;
								// PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().get
								// ResourcesPlugin.getWorkspace().getRoot().
								// Element modelTest =
								// int a = 0;
								// do{
								btnEx.setVisible(true);

								IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
										.getActivePage();
								IEditorPart editor = page.getActiveEditor();
								page.saveEditor(editor, false /* confirm */);

								// List<Element> classesDao = removeModelTest();
								//
								// List<Element> elementos3 = new ArrayList<Element>();
								// for (Element element : classesDao) {
								// if (element instanceof org.eclipse.uml2.uml.Class) {
								// elementos3.add(element);
								// }
								// }
								createModelTest();

								String sr = commentUtil.getCommentType(TypeComment.ATTRIBUTS.getName());
								Element el = null;
								el = findNavigationalClass(sr);
								if(el == null) {
									el = findConceptualClass(sr);
								}
								
								Element behaviorDaoTest = createDaoTest((Class)el);

								String nameConceptualClasse = ((Class)el).getName();
								String nameClass = "GerenteIndex" + nameConceptualClasse;
								String nameOperation = "gerar";
								if (getEObject() instanceof Class) {
									nameOperation += ((Class) getEObject()).getName();
								} else if (getEObject() instanceof Property) {
									nameOperation += ((Property) getEObject()).getName();
								}

								String[] strAux = { nameClass, nameOperation };

								String stringObjs = commentUtil.getCommentType(TypeComment.OBJECTS.getName(),
										findClassInPackage(nameConceptualClasse + "Dao", "Tests"));
								if (stringObjs == null)
									return;
								List<String[]> listaAux = new ArrayList<String[]>();
								JsonObject objMor = new JsonObject();
								objMor = Json.parse(stringObjs).asObject();
								JsonArray items = objMor.get("objs").asArray();

								String aux5[][] = new String[items.size()][];
								String nameNewObj = null;
								int i = 0;
								for (JsonValue item1 : items) {

									int j = 0;
									String aux6[] = new String[item1.asObject().names().size()];
									for (String str : item1.asObject().names()) {
										if (item1.asObject().get(str).isString()) {
											// aux5[j] = item1.asObject().get(str).asString();
											if (str.equals("id_instance")) {
												// aux6[j] = item1.asObject().get(str).asString();

											} else {
												aux6[j] = str + "=\"" + item1.asObject().get(str).asString() + "\"";
											}

										} else if (item1.asObject().get(str).isObject()) {
											JsonObject objL = item1.asObject().get(str).asObject();
											// String strAux1 = "";
											String strAux2 = "";
											int k = 0;
											nameNewObj = StringUtils.capitalize(str);
											String aux7[] = new String[objL.names().size() + 1];
											// aux7[0]=nameNewObj;

											String id_instance = objL.get("id_instance").asString();
											aux6[j] = id_instance;
											String objReturned = getJsonObjectbyInstanceId(str, id_instance);

											String relation = objL.get("relation").asString();
											// buscar objeto pelo id.

											aux6[j] = strAux2 =
//													StringUtils.uncapitalize(StringUtils.remove(
//															StringUtils.remove(StringUtils.remove(relation, "A_"), "_"),
//															StringUtils.uncapitalize(nameConceptualClasse)))
													str
													+ "= a" + id_instance;
											JsonObject objMor2 = new JsonObject();
											objMor2 = Json.parse(objReturned).asObject();
											aux7 = new String[objMor2.names().size() + 1];
//											String aux62[] = new String[objMor2.names().size() - 1];
//											for (String str2 : objMor2.names()) {
//
//											}

											for (String str2 : objMor2.names()) {
												// strAux1 += objL.get(str1).asString();
												if (str2.equals("id_instance")) {
													strAux2 = objMor2.get(str2).asString();
												} else {
													if(objMor2.get(str2).isString()) {
														strAux2 = str2 + "=\"" + objMor2.get(str2).asString() + "\"";
													}else {
														
													}
													
												}

												aux7[k] = strAux2;

												if (k != objMor2.names().size() - 1) {
													// strAux1 += " - ";
													// strAux2 += ", ";
													k++;
												}
											}
											aux7[objMor2.names().size()] = relation;
											boolean contain = false;
											for (String[] auxVerify : aux5) {
												System.out.println();
												if (auxVerify != null && aux7[0].equals(auxVerify[0])) {
													contain = true;
												}
											}
											if (!contain) {
												aux5[i] = aux7;
												i++;
											}

										}

										j++;
									}
									listaAux.add(aux6);
									aux6 = new String[item1.asObject().names().size()];

								}
								int c;
								int tamArr = 0;
								for (c = 0; c < aux5.length; c++) {
									String[] te = aux5[c];
									if (te != null && !te.toString().equals("")) {
										tamArr++;
									}
								}
								String aux9[][] = new String[tamArr][];

								for (c = 0; c < tamArr; c++) {
									aux9[c] = aux5[c];
								}

								String strAuxObjs = nameConceptualClasse;
								String testeAlfObjs = AlfCreator
										.createAlfCodeObjects(strAuxObjs, listaAux, aux9, nameNewObj).toString();

								String auxrpl[] = new String[strAux.length + 1];
								auxrpl[0] = strAux[0];
								auxrpl[1] = strAux[1];
								if (possuiParametros()) {
									auxrpl[2] = "";
								} else {
									auxrpl[2] = "";
								}

								strAux = auxrpl;

								String testeAlf = AlfCreator.createAlfCodeIndexTest(strAux).toString();

								System.out.println(testeAlf);
								System.out.println(testeAlfObjs);
								TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(getEObject());
								// DESCOMENTAR

								// int a = 0;
								// do {
								// if (a != 0) {
								umlUtil.recursoAlfXtext(behaviorDaoTest);
								// }
								executeAlfCodeTest(behaviorDaoTest, testeAlfObjs);
								// if (a != 0) {
								umlUtil.executeSpecificationJob(domain, daoClass, daoOperation);
								// }

								// a++;
								// } while (a <= 1);

								// if (a != 0) {
								String params = commentUtil.getCommentType(TypeComment.PARAMETERS.getName());

								if ((params == null || params.equals(""))) {
									Element behavior = createBehaviorTest();
									umlUtil.recursoAlfXtext(behavior);
									executeAlfCodeTest(behavior, testeAlf);
									createConfiguration(getEObject(), behavior);
									System.out.println("fim createconfig");
									btnEx.setVisible(true);
									System.out.println("fim set visible");
									// IndexPropertySection.getInstance(getEObject()).specifyCard2();
								}

							}

						});

					}
				}).start();

			}


			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

			}
		});

	}

	
	private String[] getListOfObjects(String nameConceptualClasse) {
		String stringObjs = commentUtil.getCommentType(TypeComment.OBJECTS.getName(),
				findClassInPackage(nameConceptualClasse + "Dao", "Tests"));
		
		List<String[]> listaAux = new ArrayList<String[]>();
		JsonObject objMor = new JsonObject();
		objMor = Json.parse(stringObjs).asObject();
		JsonArray items = objMor.get("objs").asArray();

		String aux5[][] = new String[items.size()][];
		String nameNewObj = null;
		int i = 0;
		for (JsonValue item1 : items) {

			int j = 0;
			String aux6[] = new String[item1.asObject().names().size()];
			for (String str : item1.asObject().names()) {
				if (item1.asObject().get(str).isString()) {
					// aux5[j] = item1.asObject().get(str).asString();
					if (str.equals("id_instance")) {
						// aux6[j] = item1.asObject().get(str).asString();

					} else {
						//aux6[j] = str + "=\"" + item1.asObject().get(str).asString() + "\"";
						if(!item1.asObject().get(str).asString().equals("")) {
							aux6[j] = str;
						}else {
							j--;
						}
						
					}

				} else if (item1.asObject().get(str).isObject()) {
					JsonObject objL = item1.asObject().get(str).asObject();
					// String strAux1 = "";
					String strAux2 = "";
					int k = 0;
					nameNewObj = StringUtils.capitalize(str);
					String aux7[] = new String[objL.names().size() + 1];
					// aux7[0]=nameNewObj;

					String id_instance = objL.get("id_instance").asString();
					aux6[j] = id_instance;
					String objReturned = getJsonObjectbyInstanceId(str, id_instance);

					String relation = objL.get("relation").asString();
					// buscar objeto pelo id.

					
					JsonObject objMor2 = new JsonObject();
					objMor2 = Json.parse(objReturned).asObject();
					String aux62[] = new String[objMor2.names().size() - 1];
					aux7 = new String[objMor2.names().size() + 1];
					for (String str2 : objMor2.names()) {

					}

					for (String str2 : objMor2.names()) {
						// strAux1 += objL.get(str1).asString();
						if (str2.equals("id_instance")) {
							//strAux2 = objMor2.get(str2).asString();
						} else {
							strAux2 = str2;
							break;
						}

						aux7[k] = strAux2;

						if (k != objMor2.names().size() - 1) {
							// strAux1 += " - ";
							// strAux2 += ", ";
							k++;
						}
					}
					
					aux6[j] = strAux2 = str
//							StringUtils.uncapitalize(StringUtils.remove(
//									StringUtils.remove(StringUtils.remove(relation, "A_"), "_"),
//									StringUtils.uncapitalize(nameConceptualClasse)))
							+"."+strAux2
							;
					
					aux7[objMor2.names().size()] = relation;
					boolean contain = false;
					for (String[] auxVerify : aux5) {
						System.out.println();
						if (auxVerify != null && aux7[0].equals(auxVerify[0])) {
							contain = true;
						}
					}
					if (!contain) {
						aux5[i] = aux7;
						i++;
					}

				}

				j++;
			}
			listaAux.add(aux6);
			aux6 = new String[item1.asObject().names().size()];

		}
		int c;
		int tamArr = 0;
		for (c = 0; c < listaAux.get(0).length; c++) {
			String te = listaAux.get(0)[c];
			if (te != null && !te.toString().equals("")) {
				tamArr++;
			}
		}
		String aux9[] = new String[tamArr];

		for (c = 0; c < tamArr; c++) {
			
			aux9[c] =listaAux.get(0)[c+1];
			
		}
		
		return aux9;

	}
	protected List<Element> getPropertyElements(EList<Element> ownedElements) {
		List<Element> aux = new ArrayList<Element>();
		// aux.removeAll(ownedElements);
		for (Element element : ownedElements) {

			if (element instanceof org.eclipse.uml2.uml.Property) {
				aux.add(element);
			}
		}
		return aux;
	}

	public void gerarNovoContexto(String resultJson, Composite container4) {

		// container4.layout(true, true);
		// container4.getParent().layout();
		// container4.requestLayout();
		// container2.layout(true, true);
		// container2.getParent().layout();

		
		
//		 resultJson = "{\r\n" +
//		 "\"name\":\r\n" +
//		 "\"porTitulacao\",\r\n" +
//		 "\"objs\":[\r\n" +
//		 "{\"obj\":\r\n" +
//		 "\"Jo�o Paulo\"}\r\n" +
//		 ",\r\n" +
//		 "{\"obj\":\r\n" +
//		 "\"Patrick\"}\r\n" +
//		 ",\r\n" +
//		 "{\"obj\":\r\n" +
//		 "\"Pedro\"}\r\n" +
//		 ",\r\n" +
//		 "{\"obj\":\r\n" +
//		 "\"Jose\"}\r\n" +
//		 "],\r\n" +
//		 "\"start\":\r\n" +
//		 "\"Jo�o Paulo\",\r\n" +
//		 "\"internalNavigation\":\r\n" +
//		 "\"circular\"\r\n" +
//		 "}\r\n" +
//		 "";

//		 
//		 resultJson = "{\r\n" + 
//		 		"\"name\":\"porTitulacao\",\r\n" + 
//		 		"\"objs\":[\r\n" + 
//		 		"{\"nome\":\"Jo�o Paulo\",\"titulacao\":\"Mestre\",\"area.nome\":\"Soft\"}\r\n" + 
//		 		",\r\n" + 
//		 		"{\"nome\":\"Patrick\",\"titulacao\":\"Mestre\",\"area.nome\":\"Soft\"}\r\n" + 
//		 		",\r\n" + 
//		 		"{\"nome\":\"Ricardo\",\"titulacao\":\"Dsc\",\"area.nome\":\"Dir\"}\r\n" + 
//		 		",\r\n" + 
//		 		"{\"nome\":\"joao\",\"titulacao\":\"Grad\",\"area.nome\":\"Outra\"}\r\n" + 
//		 		"],\r\n" + 
//		 		"\"start\":\r\n" + 
//		 		"\"Jo�o Paulo\",\r\n" + 
//		 		"\"internalNavigation\":\r\n" + 
//		 		"\"circular\"\r\n" + 
//		 		"}";
//		while (resultJson.startsWith("{")) {
//			resultJson = resultJson.substring(1, resultJson.length());
//		}

		JsonObject value = Json.parse(resultJson).asObject();
		System.out.println(value.names().toString());
		String n = value.get("name").asString();
		String selected = value.get("start").asString();
		String internalNavigation = value.get("internalNavigation").asString();

		Label lb = new Label(container4, SWT.NONE);
		lb.setText("Ctx: "+n);

		// Tree idxTree = new Tree(container4, SWT.H_SCROLL | SWT.BORDER);
		// idxTree.setHeaderVisible(true);
		// GridData data = new GridData(GridData.BEGINNING);
		// data.heightHint = 110;
		// idxTree.setLayoutData(data);

		int qtdObjs = value.get("objs").asArray().size();
		// List<String> objs =new ArrayList<String>();
		HashMap<Integer, String> objs = new HashMap<Integer, String>();
		int valRef = 10;
		int i = valRef;
		JsonArray items = value.get("objs").asArray();
		for (JsonValue item : items) {
			// objs.add(item.asObject().get("obj").asString());
			objs.put(i, item.asObject().toString());
			i += valRef;
		}

		// int indexSelected = objs. indexOf(selected);
		
		Integer indexSelected = getKeyContainValue(objs, selected.replaceAll("\'", "\""));

		if (indexSelected == null) {
			indexSelected = valRef;
		}
//		if(objs.get(10)) {
//			
//		}
		//indexSelected = (qtdObjs+1)*valRef-indexSelected;

		System.out.println(qtdObjs);
		System.out.println(indexSelected);

		final Slider slider = new Slider(container4, SWT.HORIZONTAL);
		slider.setMaximum((qtdObjs + 1) * (valRef));// tamanho da lista
		slider.setMinimum(valRef);
		slider.setSelection(indexSelected);// objeto que foi selecionado
		slider.setIncrement(valRef);

		final Label text = new Label(container4, SWT.NONE);

		text.setText(montarObjetosParaExibicao(objs.get(slider.getSelection())));
		//text.setBounds(20, 100, 286, 15);

		int direction = 0;
		slider.addListener(SWT.Selection, new Listener() {

			public void handleEvent(Event event) {

				int direction = 0;
				System.out.println("entrou lister");
				String outString = text.getText();
				switch (event.detail) {
				case SWT.ARROW_DOWN:
					direction = 1;
					break;
				case SWT.ARROW_UP:
					direction = -1;
					break;
				// case SWT.DRAG:
				// outString = "Event: SWT.DRAG";
				// break;
				// case SWT.END:
				// outString = "Event: SWT.END";
				// break;
				// case SWT.HOME:
				// outString = "Event: SWT.HOME";
				// break;
				// case SWT.PAGE_DOWN:
				// outString = "Event: SWT.PAGE_DOWN";
				// break;
				// case SWT.PAGE_UP:
				// outString = "Event: SWT.PAGE_UP";
				// break;
				}

				switch (internalNavigation) {
				case "circular":
					slider.setMaximum((qtdObjs +2) * (valRef));// tamanho da lista
					slider.setMinimum(0);
					int sel2 = slider.getSelection() ;
					
					if (direction == 1 && (qtdObjs * valRef) < (sel2)) {
						slider.setSelection(valRef);
						outString = montarObjetosParaExibicao(objs.get(valRef));
					} else if (direction == -1 &&  valRef > sel2) {
						slider.setSelection((qtdObjs ) * (valRef));
						outString = montarObjetosParaExibicao(objs.get((qtdObjs ) * (valRef)));
					}else if (direction == 1 && (qtdObjs) * valRef >= (sel2)) {
						outString = montarObjetosParaExibicao(objs.get(sel2));
						// slider.setSelection(sel+1);
					} else if (direction == -1 && valRef <= sel2) {
						outString = montarObjetosParaExibicao(objs.get(sel2));
						// slider.setSelection(sel-1);
					}

					
					break;
				case "sequential":
					int sel = slider.getSelection();

					if (direction == 1 && (qtdObjs) * valRef >= (sel)) {
						outString = montarObjetosParaExibicao(objs.get(sel));
						// slider.setSelection(sel+1);
					} else if (direction == -1 && valRef <= sel) {
						outString = montarObjetosParaExibicao(objs.get(sel));
						// slider.setSelection(sel-1);
					}
					// if(qtdObjs == (sel)) {
					// //n�o faz nada
					// }else if(0 == sel) {
					// //n�o faz nada
					// }else {
					// outString = objs.get(sel);
					// }
					break;
				}

				String outString2 = " Position: " + slider.getSelection();
				text.setText(outString);
				System.out.println(outString);
				text.getParent().layout(true, true);
				System.out.println(outString2);
			}
		});

		container4.layout(true, true);
		// container4.getParent().layout();
		// container4.getParent().layout(true, true);
		// container4.requestLayout();
		// container2.layout(true, true);
		// container2.getParent().layout();
		// container2.getParent().layout(true, true);
		//
		// container2.getDisplay().update();
		//
		// Display.getDefault().update();

		aTabbedPropertySheetPage.resizeScrolledComposite();
	}

	private String montarObjetosParaExibicao(String string) {
		
		string = string.replaceAll("\":\"", ": ");

		string = string.replaceAll("\"", "");
		string = string.replaceAll("\\{", "");
		string = string.replaceAll("\\}", "");
		String[] n = string.split(",");
		String ret = "";
		for(String it : n) {
			ret += it;
			ret += "\n";
		}
		return ret;
	}

	public static <T, E> T getKeyContainValue(Map<T, E> map, E value) {
		for (Entry<T, E> entry : map.entrySet()) {
			if (entry.getValue().toString().contains((CharSequence) value)) {
				return entry.getKey();
			}
		}
		return null;
	}
	
	public static <T, E> T getKeyByValue(Map<T, E> map, E value) {
		for (Entry<T, E> entry : map.entrySet()) {
			if (Objects.equals(value, entry.getValue())) {
				return entry.getKey();
			}
		}
		return null;
	}

	public void gerarNovoIndice(String resultJson, Composite container4) {

		// container4.layout(true, true);
		// container4.getParent().layout();
		// container4.requestLayout();
		// container2.layout(true, true);
		// container2.getParent().layout();
		//
		// CommentUtil cu = new CommentUtil(getEObject());
		JsonObject value = Json.parse(resultJson).asObject();
		System.out.println(value.names().toString());
		String n = value.get("name").asString();
		// System.out.println(n);
		Label lb = new Label(container4, SWT.NONE);
		lb.setText("Idx: "+n);

		Tree idxTree = new Tree(container4, SWT.H_SCROLL | SWT.BORDER);
		idxTree.setHeaderVisible(true);
		GridData data = new GridData(GridData.BEGINNING);
		data.heightHint = 110;
		idxTree.setLayoutData(data);

		int qtdcol = value.get("idx").asArray().get(0).asObject().get("anchors").asArray().size();

		for (int i = 0; i < qtdcol; i++) {
			TreeColumn instances1 = new TreeColumn(idxTree, SWT.LEFT);
			instances1.setText("Anchor " + (i + 1));
			instances1.setWidth(600 / qtdcol);

		}

		JsonArray items = value.get("idx").asArray();
		for (JsonValue item : items) {
			// JsonObject value2 = Json.parse(item.asString()).asObject();
			JsonArray items2 = item.asObject().get("anchors").asArray();
			TreeItem itemIdx = new TreeItem(idxTree, SWT.NONE);

			String[] auxText = new String[items2.size()];
			String[] auxData = new String[items2.size()];
			int index = 0;
			for (JsonValue item2 : items2) {
				auxText[index] = item2.asObject().get("anchorName").asString();
				auxData[index] = item2.asObject().get("target").asString();
				// System.out.println(item2.asObject().get("anchorName").asString());
				// System.out.println(item2.asObject().get("target").asString());
				index++;
			}
			itemIdx.setText(auxText);
			itemIdx.setData(auxData);
			// System.out.println("--------------");
		}

		//refresh();
		Composite containertmp = getWidgetFactory().createFlatFormComposite(container4);
		containertmp.setLayout(new GridLayout(2, false));
		Button btn = new Button(containertmp, SWT.NONE);
		btn.setEnabled(false);
		niveisbotao.put(nivel + 1, btn);

		nivel++;
		btn.setImage(new Image(Display.getDefault(), getClass().getResourceAsStream("/icons/validate.gif")));
		// String ctxOrIdx = "";
		// Element elementCtx;
		idxTree.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (idxTree.getSelection().length > 0) {
					btn.setEnabled(true);
				}
			}
		});

		Button btn2 = new Button(containertmp, SWT.NONE);
		btn2.setImage(new Image(Display.getDefault(), getClass().getResourceAsStream("/icons/run.gif")));
		btn2.setEnabled(false);

		btn.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				btn2.setEnabled(true);
				String[] s = (String[]) ((TreeItem) idxTree.getSelection()[0]).getData();
				System.out.println(s);
				ctxOrIdx = s[0].substring(0, 3);
				String itemSelected = s[0].substring(s[0].indexOf("(") + 1, s[0].indexOf(")"));
				niveisParametro.put(nivel, itemSelected);
				String nameOfIdxOrCtx = s[0].substring(4, s[0].indexOf("(")).trim();
				String sr = "";
				if (ctxOrIdx.equals("Ctx")) {
					sr = nameOfIdxOrCtx.split("\\.")[0];
					nameOfIdxOrCtx = nameOfIdxOrCtx.split("\\.")[1];
				}
				elementCtx = findContextClass(nameOfIdxOrCtx);
				// Element behavior = createBehaviorTest();
				if (ctxOrIdx.equals("Idx")) {
					sr = commentUtil.getCommentType(TypeComment.ATTRIBUTS.getName(), (EObject) elementCtx);
				}
				Element el = null;
				el = findNavigationalClass(sr);
				if(el == null) {
					el = findConceptualClass(sr);
				}
				
				Element behaviorDaoTest = createDaoTest((Class)el);
				Element behavior = createBehaviorTest((EObject) elementCtx);
				String nameConceptualClasse = ((Class)el).getName();
				String nameClass = "";
				if (ctxOrIdx.equals("Idx")) {
					nameClass = "GerenteIndex" + nameConceptualClasse;
				} else if (ctxOrIdx.equals("Ctx")) {
					nameClass = "GerenteContext" + nameConceptualClasse;
				}
				String nameOperation = "gerar";
				if (elementCtx instanceof Class) {
					nameOperation += ((Class) elementCtx).getName();
				} else if (elementCtx instanceof Property) {
					nameOperation += ((Property) elementCtx).getName();
				}

				String[] strAux = { nameClass, nameOperation };

				String auxrpl[] = new String[strAux.length + 1];
				auxrpl[0] = strAux[0];
				auxrpl[1] = strAux[1];

				int temp = getKeyByValue(niveisbotao, btn);
				itemSelectedAnterior = niveisParametro.get(temp - 1);

				if (ctxOrIdx.equals("Idx")) {
					if (possuiParametros(elementCtx)) {
						auxrpl[2] = "\"" + itemSelected + "\"";
					} else {
						auxrpl[2] = "";
					}
				} else if (ctxOrIdx.equals("Ctx")) {
					if (possuiParametros(elementCtx)) {
						auxrpl[2] = "\"" + (itemSelectedAnterior == null ? itemSelected : itemSelectedAnterior) + "\"";
					} else {
						auxrpl[2] = "";
					}
				}

				strAux = auxrpl;
				String testeAlf = "";
				if (ctxOrIdx.equals("Idx")) {
					testeAlf = AlfCreator.createAlfCodeIndexTest(strAux).toString();
				} else if (ctxOrIdx.equals("Ctx")) {
					
					sr = commentUtil.getCommentType(TypeComment.ATTRIBUTS.getName(), getEObject());
					String aux4[] = new String[6];
					aux4[0] = strAux[0];
					aux4[1] = strAux[1];
					aux4[2] = strAux[2];
					aux4[3] = sr.split(",")[0].split("\\.")[1];
					aux4[4] = itemSelected;
					aux4[5] = nameConceptualClasse;
					strAux = aux4;
					String aux10[] =getListOfObjects(nameConceptualClasse);
					testeAlf = AlfCreator.createAlfCodeContextTest(strAux,aux10).toString();
				}
				System.out.println(testeAlf);
				//
				umlUtil.recursoAlfXtext(behavior);
				executeAlfCodeTest(behavior, testeAlf);
				createConfiguration((EObject) elementCtx, behavior);

				if (ctxOrIdx.equals("Idx")) {
					// IndexPropertySection.getInstance((EObject) elementCtx).specifyCard2();
				} else if (ctxOrIdx.equals("Ctx")) {
					// ContextPropertySection.getInstance((EObject) elementCtx).specifyCard2();
				}

				// MokaConstants.MOKA_AUTOMATIC_ANIMATION = false;
				// mu.executeMoka();
				//
				// String resultJson = mu.updateConsole();
				// gerarNovoIndice(resultJson, container4);
				// se o q eu to clicando � igual

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				container4.dispose();
			}
		});

		btn2.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				int temp = getKeyByValue(niveisbotao, btn);
				int i;
				Control[] arrCont = container4.getChildren();
				for (i = arrCont.length-1; i >= 0; i--) {
					if (temp * 3 <= i) {
						excluirFilhosContainer(container4.getChildren()[i]);
					}
				}

				MokaUtil mu = new MokaUtil();
				MokaConstants.MOKA_AUTOMATIC_ANIMATION = false;
				mu.executeMoka();
				String resultJson = mu.updateConsole(true);

				if (ctxOrIdx.equals("Idx")) {
					gerarNovoIndice(resultJson, container4);
				} else if (ctxOrIdx.equals("Ctx")) {
					gerarNovoContexto(resultJson, container4);
				}

	

				aTabbedPropertySheetPage.resizeScrolledComposite();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
			}
		});

		// TransactionalEditingDomain domain =
		// TransactionUtil.getEditingDomain((EObject) elementCtx);

//		container2.getDisplay().update();
//		container4.layout(true, true);
//		container4.getParent().layout();
//		container4.getParent().layout(true, true);
//		container4.requestLayout();
//		container2.layout(true, true);
//		container2.getParent().layout();
//		container2.getParent().layout(true, true);
//		container2.requestLayout();
//		container4.getShell().layout(true, true);

	}

	protected void excluirFilhosContainer(Control control) {
		if (control instanceof Composite) {
			Control[] arrCont2 = ((Composite) control).getChildren();
			int j;
			for (j = arrCont2.length-1; j >= 0; j--) {
				if (arrCont2[j] instanceof Composite) {
					excluirFilhosContainer(arrCont2[j]);
					//arrCont2[j].dispose();
//					((Composite) arrCont2[j]).getChildren()[j].dispose();
				} else {
					arrCont2[j].dispose();
				}
			}
			control.dispose();
		} else {
			control.dispose();
		}
	}

	protected void removerInstancia(String classOfObj) {

		String params = commentUtil.getCommentType("objects", findClassInPackage(classOfObj + "Dao", "Tests"));// Parameters
		String[] s = (String[]) ((TreeItem) fTree.getSelection()[0]).getData();
		String toRemove = "";
		for (int i = 1; i < s.length; i++) {
			toRemove += s[i];
			if (i != s.length - 1) {
				toRemove += ".";
			}
		}

		toRemove = s[0];
		String str = StringUtils.remove(params, toRemove);

		if (str.startsWith(",")) {
			str = str.substring(1);
		}
		if (str.endsWith(",")) {
			str = str.substring(0, str.length() - 1);
		}

		if (str.contains(",,")) {
			str = str.replace(",,", ",");
		}
		//
		//
		//
		final String strParam = str;
		TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(getEObject());
		domain.getCommandStack().execute(new RecordingCommand(domain) {

			@Override
			protected void doExecute() {

				// saveCommentParametrosModel(strParam);
				commentUtil.saveCommentModel(strParam, TypeComment.OBJECTS.getValue(),
						findClassInPackage(classOfObj + "Dao", "Tests"));// Parametros
			}

			@Override
			protected void postExecute() {
				super.postExecute();
				this.dispose();
			}
		});
		((TreeItem) fTree.getSelection()[0]).dispose();

	}

	protected void adicionarInstancia(String string, String classOfObj) {

		String str = commentUtil.getCommentType(TypeComment.OBJECTS.getName(),
				findClassInPackage(classOfObj + "Dao", "Tests"));

		// String params = commentUtil.getCommentType("objects");//
		String params = str;//
		String aux = "";

		JsonObject objMor = new JsonObject();

		if (params == null || params.equals("")) {

			JsonArray jsonArray = new JsonArray();
			jsonArray.add(Json.parse(string).asObject());

			objMor.add("objs", jsonArray);
			// aux = string;
		} else {

			objMor = Json.parse(params).asObject();
			objMor.get("objs").asArray().add(Json.parse(string).asObject());
			// aux += params + "," + string;
		}

		final String strParam = objMor.toString();
		TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(getEObject());
		domain.getCommandStack().execute(new RecordingCommand(domain) {

			@Override
			protected void doExecute() {

				commentUtil.saveCommentModel(strParam, TypeComment.OBJECTS.getValue(),
						findClassInPackage(classOfObj + "Dao", "Tests"));// Parametros
			}

			@Override
			protected void postExecute() {
				super.postExecute();
				this.dispose();
			}
		});
	}

	public List<Element> removeModelTest() {
		List<Element> elementos3 = new ArrayList<Element>();
		TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(getEObject());
		domain.getCommandStack().execute(new RecordingCommand(domain) {

			@Override
			protected void doExecute() {

				elementos3.addAll(umlUtil.removeModelElement("Tests", getEObject()));// TODO criar contante

			}

			@Override
			protected void postExecute() {
				super.postExecute();
				this.dispose();
				System.out.println("passou 5");
			}
		});

		return elementos3;
	}

	public Class findConceptualClass(String sr) {

		String aux = sr.split(",")[0].split("\\.")[0];

		Element root = ((Class) getEObject()).getModel().getModel().getModel().getOwner();
		for (Element ele : root.getOwnedElements()) {
			if (ele instanceof Model) {
				if (((Model) ele).getName().equals("Conceptual")) {
					for (Element ele2 : ((Model) ele).getOwnedElements()) {
						if (ele2 instanceof Class) {
							if (((Class) ele2).getName().equals(aux)) {
								return (Class) ele2;
							}
						}
					}
				}

			}
		}
		return null;
	}

	// public Class findNavigationalClass(String sr) {
	//
	// String aux = sr.split(",")[0].split("\\.")[0];
	//
	// // Element root =
	// // ((Class)getEObject()).getModel().getModel().getModel().getOwner();
	// Element root = null;
	// if (getEObject() instanceof Class) {
	// root = ((Class) getEObject()).getModel().getModel().getModel().getOwner();
	// } else if (getEObject() instanceof Property) {
	// root = ((Property) getEObject()).getModel().getModel().getModel().getOwner();
	// }
	//
	// for (Element ele : root.getOwnedElements()) {
	// if (ele instanceof Model) {
	// if (((Model) ele).getName().equals("Navigational")) {
	// for (Element ele2 : ((Model) ele).getOwnedElements()) {
	// if (ele2 instanceof Class) {
	// if (((Class) ele2).getName().equals(aux)) {
	// return (Class) ele2;
	// }
	// }
	// }
	// }
	//
	// }
	// }
	// return null;
	// }

	public Class findNavigationalClass(String sr) {

		String aux = sr.split(",")[0].split("\\.")[0];
		return findClassInPackage(aux, "Navigational");
	}

	
	public Class findContextClass(String sr) {
		return findClassInPackage(sr, "Context");
	}

	public Class findClassInPackage(String aux, String packageName) {

		// Element root =
		// ((Class)getEObject()).getModel().getModel().getModel().getOwner();
		Element root = null;
		if (getEObject() instanceof Class) {
			root = ((Class) getEObject()).getModel().getModel().getModel().getOwner();
		} else if (getEObject() instanceof Property) {
			root = ((Property) getEObject()).getModel().getModel().getModel().getOwner();
		}

		for (Element ele : root.getOwnedElements()) {
			if (ele instanceof Model) {
				if (((Model) ele).getName().equals(packageName)) {
					for (Element ele2 : ((Model) ele).getOwnedElements()) {
						if (ele2 instanceof Class) {
							if (((Class) ele2).getName().equals(aux)) {
								return (Class) ele2;
							}
						} else if (ele2 instanceof org.eclipse.uml2.uml.Package) {
							for (Element ele3 : ((org.eclipse.uml2.uml.Package) ele2).getOwnedElements()) {
								if (ele3 instanceof Class) {
									if (((Class) ele3).getName().equals(aux)) {
										return (Class) ele3;
									}
								}
							}
						}
					}
				}

			}

		}
		return null;
	}

	public List<Element> findRelationsInPackage(String aux, String packageName) {

		List<Element> lstAux = new ArrayList<Element>();
		// Element root =
		// ((Class)getEObject()).getModel().getModel().getModel().getOwner();
		Element root = null;
		if (getEObject() instanceof Class) {
			root = ((Class) getEObject()).getModel().getModel().getModel().getOwner();
		} else if (getEObject() instanceof Property) {
			root = ((Property) getEObject()).getModel().getModel().getModel().getOwner();
		}

		for (Element ele : root.getOwnedElements()) {
			if (ele instanceof Model) {
				if (((Model) ele).getName().equals(packageName)) {
					for (Element ele2 : ((Model) ele).getOwnedElements()) {
						if (ele2 instanceof Association) {
							if (!((Association) ele2).getMemberEnds().get(0).getType().getName().equals(aux)
									&& ((Association) ele2).getMemberEnds().get(1).getType().getName().equals(aux)) {
								lstAux.add(((Association) ele2).getMemberEnds().get(0));
								// return (Property) ele3;
							} else if (((Association) ele2).getMemberEnds().get(0).getType().getName().equals(aux)
									&& !((Association) ele2).getMemberEnds().get(1).getType().getName().equals(aux)) {
								lstAux.add(((Association) ele2).getMemberEnds().get(1));
								// return (Property) ele3;
							}

						}
						// else if (ele2 instanceof org.eclipse.uml2.uml.Package) {
						// for (Element ele3 : ((org.eclipse.uml2.uml.Package) ele2).getOwnedElements())
						// {
						// if (ele3 instanceof Class) {
						// if (((Class) ele3).getName().equals(aux)) {
						// return (Class) ele3;
						// }
						// }
						// }
						// }
					}
				}

			}

		}
		return lstAux;
	}

	public void executeAlfCodeTest(Element behavior, String testeAlf) {
		umlUtil.executeAlfCode(behavior, testeAlf);
		System.out.println("passou 2");

		System.out.println("passou 3");
	}

	public void createConfiguration(EObject selected, Element behavior) {

		Element umlElement = (Element) selected;
		Resource resource = umlElement.eResource();
		// String id = resource.getURIFragment(umlElement);
		String[] segments = resource.getURI().segments();

		String concat = "";
		for (int i = 1; i < segments.length; i++) {
			concat += segments[i];
			if (i != segments.length - 1) {
				concat += "/";
			}
		}

		ILaunchManager manager = DebugPlugin.getDefault().getLaunchManager();

		ILaunchConfigurationType launchType = manager
				.getLaunchConfigurationType("org.eclipse.papyrus.moka.launchConfiguration");// TODO criar contante

		// ILaunchConfiguration[] lcs;
		// try {
		// lcs = manager.getLaunchConfigurations(launchType);
		// for (ILaunchConfiguration iLaunchConfiguration : lcs) {
		// if (iLaunchConfiguration.getName().equals("xOOHDMTest")) {// TODO criar
		// constante
		// ILaunchConfigurationWorkingCopy t = iLaunchConfiguration.getWorkingCopy();
		// t.delete();
		//
		// }
		// }
		// } catch (CoreException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		try {

			Resource resourceBh = behavior.eResource();
			String id = resourceBh.getURIFragment(behavior);

			ILaunchConfigurationWorkingCopy workingCopy = launchType.newInstance(null, "xOOHDMTest");// TODO criar

			workingCopy.setAttribute("URI_ATTRIBUTE", "platform:/resource/" + concat);// projeto/modelo.uml//TODO criar

			workingCopy.setAttribute("FRAGMENT_ATTRIBUTE", id);
			workingCopy.setAttribute("ARGS_ATTRIBUTE", "org.eclipse.papyrus.moka.fuml");// TODO criar contante
			workingCopy.doSave();

		} catch (CoreException e) {

		}
	}

	protected Element createModelTest(List<Element> elements) {
		TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(getEObject());
		domain.getCommandStack().execute(new RecordingCommand(domain) {

			@Override
			protected void doExecute() {

				testeModel = umlUtil.createOrSelectModelElement("Tests", getEObject(), elements);// TODO criar contante

			}

			@Override
			protected void postExecute() {
				super.postExecute();
				this.dispose();
				System.out.println("passou");
			}
		});
		return testeModel;

	}

	protected Element createModelTest() {
		TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(getEObject());
		domain.getCommandStack().execute(new RecordingCommand(domain) {

			@Override
			protected void doExecute() {

				testeModel = umlUtil.createOrSelectModelElement("Tests", getEObject(), null);// TODO criar contante

			}

			@Override
			protected void postExecute() {
				super.postExecute();
				this.dispose();
				System.out.println("passou");
			}
		});
		return testeModel;

	}

	protected Element createDaoTest(Class clasRetorno) {
		TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(getEObject());
		domain.getCommandStack().execute(new RecordingCommand(domain) {

			@Override
			protected void doExecute() {

				if (testeModel == null) {
					createModelTest(null);
				}

				// Class c = UMLFactory.eINSTANCE.createClass();
				// c.setName("Professor");

				// Element daoClass = umlUtil.generateClass(testeModel, "Dao");// TODO criar
				// contante
				daoClass = umlUtil.generateClass(testeModel, clasRetorno.getName() + "Dao");
				// Element daoClassProperty =
				umlUtil.generateAttributs(umlUtil.generateClass(testeModel, clasRetorno.getName() + "Dao"), clasRetorno,
						"lista", getEObject(), true);// TODO criar contante

				daoOperation = umlUtil.generateOperation(daoClass, clasRetorno, "", getEObject(), "getObjects", true);// TODO
																														// criar
																														// contante
				daoBehavior = umlUtil.generateBehaviorOperation(
						umlUtil.generateClass(testeModel, clasRetorno.getName() + "Dao"), daoOperation, clasRetorno,
						null, "getObjectsImpl", getEObject(), true);// TODO criar contante

			}

			@Override
			protected void postExecute() {
				super.postExecute();
				this.dispose();
				System.out.println("passou 2");
			}
		});
		return daoBehavior;

	}

	protected Element createBehaviorTest() {

		return createBehaviorTest(getEObject());

	}

	protected Element createBehaviorTest(EObject eobject) {
		TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(eobject);
		domain.getCommandStack().execute(new RecordingCommand(domain) {

			@Override
			protected void doExecute() {

				testBehavior = umlUtil.generateBehaviorModel(testeModel, null, null, "ActivityOOHDMTest", eobject,
						false);// TODO criar
			}

			@Override
			protected void postExecute() {
				super.postExecute();
				this.dispose();
				System.out.println("passou");
			}
		});
		return testBehavior;

	}

	protected String getTestText() {

		return commentUtil.getCommentType(TypeComment.TESTS.getName());
	}

}
