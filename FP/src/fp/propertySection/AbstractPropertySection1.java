package fp.propertySection;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.papyrus.uml.alf.text.generation.DefaultEditStringRetrievalStrategy;
import org.eclipse.papyrus.uml.alf.transaction.commit.ScenarioFactory;
import org.eclipse.papyrus.uml.tools.utils.UMLUtil;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.views.properties.tabbed.AbstractPropertySection;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.NamedElement;

import fp.util.CommentUtil;

public class AbstractPropertySection1 extends AbstractPropertySectionOOHDM {

	public AbstractPropertySection1() {
		// TODO Auto-generated constructor stub
	}

	Button commitButton;
	private DefaultEditStringRetrievalStrategy alfSerialization;
	
	@Override
	public void setInput(IWorkbenchPart part, ISelection selection) {
		// TODO Auto-generated method stub
		super.setInput(part, selection);
		localSelection = selection;
		if (selection instanceof IStructuredSelection) {
			Object input = ((IStructuredSelection) selection).getFirstElement();

			Element semanticElement = UMLUtil.resolveUMLElement(input);
			if (semanticElement != null) {
				setEObject(semanticElement);
				commentUtil = new CommentUtil(semanticElement);
			}
		}
	//testeExecuteForum(((Element) getEObject()).getModel().getOwner());
	}

	
	@Override
	public final void createControls(Composite parent, TabbedPropertySheetPage aTabbedPropertySheetPage) {
		super.createControls(parent, aTabbedPropertySheetPage);
		Composite container = getWidgetFactory().createFlatFormComposite(parent);
		container.setLayout(new GridLayout(3, false));
		setContainer(container);
		
		this.commitButton = new Button(parent, SWT.PUSH);
		this.commitButton.setText("Save");
		this.commitButton.setToolTipText("Save the specification card in your model");
		this.commitButton
				.setImage(new Image(Display.getDefault(), getClass().getResourceAsStream("/icons/validate.gif")));
		// this.commitButton.addSelectionListener(new
		// CommitButtonSelectionListener(this));
		this.commitButton.addDisposeListener(new DisposeListener() {

			@Override
			public void widgetDisposed(DisposeEvent e) {
			}
		});

		this.commitButton.addSelectionListener(new SelectionListener() {

			

			@Override
			public void widgetSelected(SelectionEvent e) {
				try {

					alfSerialization = new DefaultEditStringRetrievalStrategy();
					String serialization = "/*Error: serialization could not be computed*/";
//					if (getEObject() != null) {
//						serialization = alfSerialization.getEditString((Element) getEObject());
//					}
					
					serialization = "namespace Generated::GerenteIndexProfessor;\r\n\t\tprivate import Alf::Library::CollectionClasses::List;\r\n\t\tprivate import Alf::Library::PrimitiveBehaviors::IntegerFunctions::ToString;\r\n\t\tprivate import Alf::Library::BasicInputOutput::WriteLine;\r\n\t\tprivate import oohdmModel::Index;\t\t\r\n\t\tprivate import oohdmModel::SimpleIndex;\r\n\t\tprivate import oohdmModel::IndexEntry;\r\n\t\tprivate import oohdmModel::Anchor;\r\n\t\tprivate import Tests::Dao;\r\n\t\tprivate import Conceptual::Professor;\r\n\t\t\r\n\t\tactivity gerarProfessoresImpl () : SimpleIndex {\r\n\t\t//WriteLine(\"inicio gerar Professores\");\r\n\t\tsi = new SimpleIndex();\r\n\t\tsi.setName(\"Professores\");\r\n\t\t//WriteLine(si.getName());\r\n\t\t//WriteLine(\"passou 1\");\r\n\t\tdao = new Dao();\r\n\t\t//WriteLine(\"passou 1\");\r\n\t\tlista = dao.getObjects();\r\n\t\t//WriteLine(\"passou 1\");\r\n\t\tobjs = lista->select p (p instanceof Professor);// elementos + parametros\r\n\t\t//WriteLine(\"passou 1\");\r\n\t\t//WriteLine(ToString(objs->size()));\r\n\t\ti = 1;\r\n\t\twhile(i <= objs->size()){\r\n\t\t\tie = new IndexEntry();\r\n\t\t\tProfessor e0  = objs->at(i);\r\n\t\t\ta0 = new Anchor();\r\n\t\t\t//WriteLine(e0.nome);\r\n\t\t\ta0.setName(e0.nome);\r\n\t\t\ta0.setUrl(\"Ctx Professor.ProfessorAlfabetico\");\r\n\t\t\tie.addAnchor(a0);\r\n\t\t\tProfessor e1  = objs->at(i);\r\n\t\t\ta1 = new Anchor();\r\n\t\t\t//WriteLine(e1.titulacao);\r\n\t\t\ta1.setName(e1.titulacao);\r\n\t\t\ta1.setUrl(\"\");\r\n\t\t\tie.addAnchor(a1);\r\n\t\t\tsi.addIndexEntry(ie);\r\n\t\t\ti++;\r\n\t\t\t//WriteLine(\"Gerou indice \");\r\n\t\t}\r\n\t\t//WriteLine(\"fim gerar Professores\");\r\n\t\treturn si;\r\n\t\t}";
					NamedElement semanticObject = ((NamedElement) getEObject());
					if (semanticObject != null && serialization != null) {
						ScenarioFactory.getInstance().createCommitScenario().execute(semanticObject, serialization);
					}

				}catch (Exception ex) {
					ex.printStackTrace();
				}
				
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		
	}


	@Override
	protected void specifyCard() {
		// TODO Auto-generated method stub
		
	}


	@Override
	protected void specifyCard2() {
		// TODO Auto-generated method stub
		
	}
}
