package fp.propertySection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.papyrus.uml.extensionpoints.library.IRegisteredLibrary;
import org.eclipse.papyrus.uml.extensionpoints.library.RegisteredLibrary;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.UMLFactory;

import fp.adapter.GerenteContextTransition;
import fp.dialog.ParameterDialog;
import fp.dialog.SortDialog;
import fp.util.TypeComment;
import fp.xtext.GerenteCreator;

public class ContextPropertySection extends AbstractPropertySectionOOHDM {

	public ContextPropertySection() {
	}

	private static ContextPropertySection cps;
	
	public static ContextPropertySection getInstance(EObject eo) {
		if(cps == null) {
			cps = new ContextPropertySection();
			cps.setEObject(eo);
			return cps;
		}
		cps.setEObject(eo);
		return cps;
	}

	Combo c2 = null;
	String[] arrayNavigation = { "Circular", "Sequential" };

	@Override
	public void refresh() {
		super.refresh();
		this.refreshDataField(commentUtil.getComments(0));
	}

	private void limparFormulario() {
		TreeItem[] itens = fTree.getItems();
		for (TreeItem ti : itens) {
			ti.dispose();
		}
		elementosText.setText("");
		TreeItem[] itens2 = fTree2.getItems();
		for (TreeItem ti : itens2) {
			ti.dispose();
		}

		c2.setItems(arrayNavigation);
		
	}
	private void refreshDataField(EList<Comment> eList) {
		elementosText.removeModifyListener(elementsListener);
		
		//if(eList.size() == 0) {
			limparFormulario();
		//}
		for (Comment co : eList) {
			String aux[] = co.getBody().split("://");
			if (aux[0].equals("parameters") && aux.length == 2) {// TODO criar constante
				refreshParametros(aux[1]);
				// parametrosText.setText(aux[1]);
			} else if (aux[0].equals("elements")) {// TODO criar constante
				elementosText.setText(aux[1]);
			} else if (aux[0].equals("sort") && aux.length == 2) {// TODO criar constante
				refreshSort(aux[1]);
			} else if (aux[0].equals("navigation") && aux.length == 2) {// TODO criar constante
				refreshNavigation(aux[1]);
			}
		}
		elementosText.addModifyListener(elementsListener);
	}

	private void refreshNavigation(String string) {

		c2.select(getIndexOfItemInArray(arrayNavigation, string));
	}

	private int getIndexOfItemInArray(String[] stringArray, String name) {
		if (stringArray != null && stringArray.length > 0) {
			ArrayList<String> list = new ArrayList<String>(Arrays.asList(stringArray));
			int index = list.indexOf(name);
			list.clear();
			return index;
		}
		return -1;
	}

	private void refreshSort(String string) {
		TreeItem[] itens = fTree2.getItems();
		for (TreeItem ti : itens) {
			ti.dispose();
		}
		String aux[] = string.split(",");
		for (int i = 0; i < aux.length; i++) {
			TreeItem item = new TreeItem(fTree2, SWT.NONE);
			String aux2[] = aux[i].split("\\.");
			if (aux2.length == 3) {
				String aux3[] = new String[2];
				aux3[0] = aux2[0] + "." + aux2[1];
				aux3[1] = aux2[2];
				item.setText(aux3);
				item.setData(aux3);
			}

		}
	}

	@Override
	public final void createControls(Composite parent, TabbedPropertySheetPage aTabbedPropertySheetPage) {
		super.createControls(parent, aTabbedPropertySheetPage);

		Composite container = getWidgetFactory().createFlatFormComposite(parent);
		container.setLayout(new GridLayout(3, false));
		setContainer(container);

		GridData labelParametros = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		labelParametros.heightHint = 50;
		labelParametros.widthHint = 50;
		labelParametros.verticalSpan = 7;

		GridData parametrosData = new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 5);
		parametrosData.heightHint = 50;
		parametrosData.widthHint = 350;
		parametrosData.verticalSpan = 5;

		getWidgetFactory().createCLabel(container, "Par�metros:");

		// Image image = new Image(display, "yourFile.gif");
		Button button = new Button(container, SWT.TOGGLE);
		button.setToolTipText("Choose the parameters!");
		button.setImage(new Image(Display.getDefault(), getClass().getResourceAsStream("/icons/Plus.gif")));
		button.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				ParameterDialog pd = new ParameterDialog(container.getShell(), getEObject());
				pd.open();
				Object[] obj = pd.getResult();

				String teste = obj[0].toString();
				if (commentUtil.getCommentType("parameters") != null
						&& commentUtil.getCommentType("parameters").contains(teste)) {
					// Message
					MessageBox messageDialog = new MessageBox(container.getShell(), SWT.ERROR);
					messageDialog.setText("Erro");
					messageDialog.setMessage("Par�metro j� inserido!!!");
					messageDialog.open();
				} else if (teste != null) {
					adicionarParametro(teste);
					// refressar somente os paramentros TODO
					refresh();
					// System.out.println(teste);
				}

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

			}
		});

		buttonExcParam = new Button(container, SWT.TOGGLE);
		buttonExcParam.setToolTipText("Remove the parameter!");
		buttonExcParam.setImage(new Image(Display.getDefault(), getClass().getResourceAsStream("/icons/remove.gif")));
		buttonExcParam.setEnabled(false);
		buttonExcParam.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				removerParametro();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

			}
		});

		Composite container2 = getWidgetFactory().createFlatFormComposite(container);
		container2.setLayout(new GridLayout(1, false));

		fTree = new Tree(container2, SWT.H_SCROLL | SWT.BORDER);
		fTree.setHeaderVisible(true);
		GridData data = new GridData(GridData.BEGINNING);
		fTree.setLayoutData(data);
		data.heightHint = 50;
		fTree.setRedraw(false);

		TreeColumn instances = new TreeColumn(fTree, SWT.LEFT);
		instances.setText("Classe");
		instances.setWidth(300);
		TreeColumn explicitAlloc = new TreeColumn(fTree, SWT.LEFT);
		explicitAlloc.setText("Par�metro");
		explicitAlloc.setWidth(300);

		fTree.setRedraw(true);

		fTree.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (fTree.getSelection().length > 0) {
					buttonExcParam.setEnabled(true);
				}
			}
		});

		GridData labelElementos = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		labelElementos.heightHint = 80;
		labelElementos.verticalSpan = 5;
		labelElementos.widthHint = 200;

		GridData elementosData = new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 5);
		elementosData.heightHint = 80;
		elementosData.widthHint = 350;
		elementosData.verticalSpan = 5;

		getWidgetFactory().createCLabel(container2, "Elementos:");
		elementosText = new Text(container2, SWT.MULTI | SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
		elementosText.setLayoutData(elementosData);
		elementosText.addModifyListener(elementsListener);
		
		Composite container3 = getWidgetFactory().createFlatFormComposite(container2);
		container3.setLayout(new GridLayout(3, false));

		GridData labelAtributos = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		labelAtributos.heightHint = 50;
		labelAtributos.widthHint = 50;
		labelAtributos.verticalSpan = 7;

		GridData atributosData = new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 5);
		atributosData.heightHint = 50;
		atributosData.widthHint = 350;
		atributosData.verticalSpan = 5;

		getWidgetFactory().createCLabel(container3, "Ordena��o:");

		// Image image = new Image(display, "yourFile.gif");
		Button button2 = new Button(container3, SWT.TOGGLE);
		button2.setToolTipText("Choose the sort!");
		button2.setImage(new Image(Display.getDefault(), getClass().getResourceAsStream("/icons/Plus.gif")));
		button2.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				SortDialog pd = new SortDialog(container3.getShell(), getEObject());
				pd.open();
				Object[] obj = pd.getResult();

				String teste = obj != null ? obj[0].toString() : null;
				if (getCommentSort() != null && getCommentSort().contains(teste)) {
					// Message
					MessageBox messageDialog = new MessageBox(container3.getShell(), SWT.ERROR);
					messageDialog.setText("Erro");
					messageDialog.setMessage("Ordena��o j� inserida!!!");
					messageDialog.open();
				} else if (teste != null) {
					addSort(teste);
					refresh();
				}

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

			}
		});

		buttonExcAttr = new Button(container3, SWT.TOGGLE);
		buttonExcAttr.setToolTipText("Remove the Sort!");
		buttonExcAttr.setImage(new Image(Display.getDefault(), getClass().getResourceAsStream("/icons/remove.gif")));
		buttonExcAttr.setEnabled(false);
		buttonExcAttr.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				removeSort();

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

			}
		});

		// parametrosText = getWidgetFactory().createText(container, "");
		// parametrosText.setLayoutData(parametrosData);

		Composite container4 = getWidgetFactory().createFlatFormComposite(container3);
		container4.setLayout(new GridLayout(1, false));

		// parametrosText = new Text(container2, SWT.MULTI | SWT.BORDER | SWT.WRAP |
		// SWT.V_SCROLL);
		// parametrosText.setLayoutData(parametrosData);

		fTree2 = new Tree(container4, SWT.H_SCROLL | SWT.BORDER);
		fTree2.setHeaderVisible(true);
		GridData data2 = new GridData(GridData.BEGINNING);
		fTree2.setLayoutData(data2);
		data2.heightHint = 50;
		// data.widthHint = 200;

		// Turn off drawing to avoid flicker
		fTree2.setRedraw(false);

		TreeColumn instances2 = new TreeColumn(fTree2, SWT.LEFT);
		instances2.setText("Atributo");
		instances2.setWidth(300);
		TreeColumn explicitAlloc2 = new TreeColumn(fTree2, SWT.LEFT);
		explicitAlloc2.setText("Tipo de ordena��o");
		explicitAlloc2.setWidth(300);

		// inserirLinhasParametros(fTree);

		// fillTree(fTree, null, m_cdp.getMainInstance());

		// Turn drawing back on!
		fTree2.setRedraw(true);

		fTree2.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (fTree2.getSelection().length > 0) {
					buttonExcAttr.setEnabled(true);
				}
			}
		});

		Composite container5 = getWidgetFactory().createFlatFormComposite(container4);
		container5.setLayout(new GridLayout(2, false));

		getWidgetFactory().createCLabel(container5, "Navega��o Interna:");

		c2 = new Combo(container5, SWT.READ_ONLY);
		c2.setBounds(50, 85, 150, 65);

		c2.setItems(arrayNavigation);

		Composite container6 = getWidgetFactory().createFlatFormComposite(container5);
		container6.setLayout(new GridLayout(2, false));

		getWidgetFactory().createCLabel(container6, "Salvar:");
		this.createPushButton(container6);
		
//		textControl = new StyledText(container6, SWT.MULTI | SWT.BORDER
//				| SWT.V_SCROLL | SWT.WRAP);
//		textControl.setVisible(false);

	}


	protected void specifyCard2() {

	}

	protected void specifyCard() {

		TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(getEObject());
		domain.getCommandStack().execute(new RecordingCommand(domain) {

			@Override
			protected void doExecute() {

				try {
					fazerValidacao();

					saveCommentNavigationModel();

					importPackageOohdmIfNotApply();
					importPackageAlfIfNotApply();
					
					for (IRegisteredLibrary library : RegisteredLibrary.getRegisteredLibraries()) {
						if (library.getName() != null) {
							if (library.getName().equals("FoundationalModelLibrary")) {
								importPackageFoundationalModelLibraryIfNotApply(library.getUri());
							} else if (library.getName().equals("CollectionClassesImpl")) {
								importPackageCollectionClassesImplIfNotApply(library.getUri());
							} else if (library.getName().equals("PrimitiveTypes")) {
								importPackagePrimitiveTypesIfNotApply(library.getUri());
							} else if (library.getName().equals("EcorePrimitiveTypes")) {
								importPackageEcorePrimitiveTypesIfNotApply(library.getUri());
							} else if (library.getName().equals("AssertionLibrary")) {
								importPackageAssertionLibraryIfNotApply(library.getUri());
							}
						}
					}
					Element modelGenerate = getModelGenerated();
					
//					createModelTest();
//					//Element behaviorDaoTest = 
//							createDaoTest( findNavigationalClass(getGerenteAtual()));

					classGerente = generateGerenteClass(modelGenerate, getGerenteAtual());
					//String name = ((Class) getEObject()).getName();
					operationGerente = generateGerenteOperation(classGerente, getEObject());

					behaviorOperation = generateBehaviorOperation(classGerente, operationGerente, getClassContext());// alf

					
					String alfCode = buildAlfCode(operationGerente); // TODO verificar se a valida��o do alf ser� aqui.
					System.out.println(alfCode);
					umlUtil.recursoAlfXtext(behaviorOperation);
					umlUtil.executeAlfCode(behaviorOperation, alfCode);

					umlUtil.executeSpecificationJob(domain, classGerente, operationGerente);
					//
					// Element behaviorOperationWithSpecification = setSpecification(classGerente,
					// operationGerente);

					//
				} catch (Exception e) {
					MessageBox messageDialog = new MessageBox(getContainer().getShell(), SWT.ERROR);
					messageDialog.setText("Erro");
					messageDialog.setMessage("Erro ao compilar c�digo alf!");
					messageDialog.open();
					commitButton.setEnabled(true);
					e.printStackTrace();
				}

			}

			@Override
			protected void postExecute() {
				super.postExecute();
			}

		});

		// domain.getCommandStack().execute(new RecordingCommand(domain) {
		//
		//
		// @Override
		// protected void doExecute() {
		// try {
		//
		// Element behaviorOperationWithSpecification = setSpecification(classGerente,
		// operationGerente);
		//
		//
		// } catch (Exception e) {
		// MessageBox messageDialog = new MessageBox(getContainer().getShell(),
		// SWT.ERROR);
		// messageDialog.setText("Erro");
		// messageDialog.setMessage("Erro ao vincular m�todo!");
		// messageDialog.open();
		// commitButton.setEnabled(true);
		// e.printStackTrace();
		// }
		// }
		//
		// @Override
		// protected void postExecute() {
		// super.postExecute();
		// }
		//
		// });
		//
	}



	protected void saveCommentNavigationModel() {
		List<Comment> commentList = new ArrayList<Comment>();

		Comment comentElementos2 = UMLFactory.eINSTANCE.createComment();
		commentUtil.removeComment(TypeComment.NAVIGATION.getValue());

		comentElementos2.setBody(
				commentUtil.compileComment(arrayNavigation[c2.getSelectionIndex()], TypeComment.NAVIGATION.getValue()));
		commentList.add(comentElementos2);
		((Element) getEObject()).getOwnedComments().add(comentElementos2);
	}

	protected Element generateGerenteOperation(Element cGerente, EObject eObjectSelected) {

		return umlUtil.generateOperation(cGerente, getClassContext(),commentUtil.getCommentType("parameters"), eObjectSelected,"",false);
	}

	private Class getClassContext() {

		Resource modelResource = umlUtil.getResourceOohdmModel(getEObject());
		Model m = ((Model) modelResource.getContents().get(0));
		for (Element ele : m.getOwnedElements()) {
			if (ele instanceof Class) {
				if (((Class) ele).getName().equals("Context")) { // TODO CRIAR CONSTANTE
					return (Class) ele;
				}
			}
		}
		return null;
	}

	protected void fazerValidacao() {
		// TODO Auto-generated method stub
		// tem que ter pelo menos o campo elemento
		// e um atributo com destino.
	}

	protected Element generateGerenteClass(Element mGenerate, String gerenteAtual) {
		String name = "GerenteContext" + gerenteAtual;
		return umlUtil.generateClass(mGenerate, name);

	}

	protected String getGerenteAtual() {

		String aux = getCommentSort();

		if (aux.split("\\.").length > 1) {
			return aux.split("\\.")[0];
		}

		return null;
	}

	protected String buildAlfCode(Element operationGerente) {
		// TODO Tem que colocar os outros (ordena��o e navega��o interna)
		String in = ((Class) eObject).getName();
		String namespace = ((Class) ((Operation) operationGerente).getOwner()).getModel().getName() + "::"
				+ ((Class) ((Operation) operationGerente).getOwner()).getName();
		String behaviorName = ((Operation) operationGerente).getName() + "Impl";

		GerenteContextTransition git = new GerenteContextTransition(namespace, behaviorName, in,
				commentUtil.getCommentType("elements"), getCommentSort(), commentUtil.getCommentType("parameters"),
				getCommentNavigation());

		String alfCode = GerenteCreator.createGerenteContext(git).toString();

		return alfCode;
	}

	


	protected String getCommentNavigation() {

		for (Comment co : commentUtil.getComments(TypeComment.NAVIGATION.getValue())) {

			String aux[] = co.getBody().split("://");
			if (aux[0].equals("navigation")) {// TODO criar constante
				if (aux.length == 2)
					return aux[1];
			}

		}
		return null;

	}

}
