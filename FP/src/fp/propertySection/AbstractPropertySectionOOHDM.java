package fp.propertySection;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gmf.runtime.diagram.ui.properties.sections.AbstractModelerPropertySection;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.papyrus.infra.core.resource.ModelSet;
import org.eclipse.papyrus.infra.core.services.ServiceException;
import org.eclipse.papyrus.infra.gmfdiag.common.model.NotationUtils;
import org.eclipse.papyrus.infra.services.openelement.service.OpenElementService;
import org.eclipse.papyrus.infra.ui.editor.IMultiDiagramEditor;
import org.eclipse.papyrus.infra.ui.util.EditorUtils;
import org.eclipse.papyrus.uml.alf.transaction.job.AlfJobObserver;
import org.eclipse.papyrus.uml.tools.utils.UMLUtil;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.UMLFactory;

import fp.util.CommentUtil;
import fp.util.DiagramOOHDMUtil;
import fp.util.TypeComment;
import fp.util.UmlUtil;
import fp.xtext.GerenteCreator;

public abstract class AbstractPropertySectionOOHDM extends AbstractModelerPropertySection {

	// private StyledText textControl;
	//
	// private UndoRedoStack<ExtendedModifyEvent> undoRedoStack;

	protected boolean isUndo;

	protected boolean isRedo;

//	StyledText textControl;
	/** the id of the model explorer */
	public static final String viewId = "org.eclipse.papyrus.views.modelexplorer.modelexplorer";

	public AbstractPropertySectionOOHDM() {
	}

	Element classGerente;
	Element operationGerente;
	Element behaviorOperation;
	DiagramOOHDMUtil dmUtil = new DiagramOOHDMUtil();

	// private FormToolkit toolkit;
	//
	// private Form form;
	Button commitButton;
	Text elementosText;
	Text nameText;
	Text parametrosText;
	Button buttonExcParam;
	Button buttonExcAttr;
	
	Button buttonExcSort;
	// TextArea parametrosTextArea;
	// private EObjectTreeElement data = new EObjectTreeElementImpl();
	// private ISelection selection;
	//
	// private ModelExplorerView modelExplorerView;
	// private ModelExplorerPageBookView view;
	// private CommonViewer commonViewer;
	//
	// private EModelElement diagramElement;
	Element testeModel;
	Element daoBehavior;
	Element testBehavior;
	Tree fTree;
	Tree fTree2;
	Tree fTree2Sort;
	
	ISelection localSelection;
	UmlUtil umlUtil = new UmlUtil();
	private Composite container;
	 CommentUtil commentUtil;
	protected ModifyListener elementsListener = new ModifyListener() {
		 @Override
		 public void modifyText(ModifyEvent e) {
			 TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(getEObject());
				domain.getCommandStack().execute(new RecordingCommand(domain) {

					@Override
					protected void doExecute() {

						commentUtil.saveCommentModel(elementosText.getText(), TypeComment.ELEMENTS.getValue());//Elementos
					}

					@Override
					protected void postExecute() {
						super.postExecute();
						this.dispose();
					}
				});
			 
			 
		 }
	};
	// private ModifyListener nameListener = new ModifyListener() {
	// @Override
	// public void modifyText(ModifyEvent e) {

	// IViewPart navigator =
	// PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().findView("org.eclipse.papyrus.views.modelexplorer.modelexplorer");//find
	// the navigator
	// view = (ModelExplorerPageBookView)navigator;
	// IPage currentPage = view.getCurrentPage();
	// ModelExplorerPage page = (ModelExplorerPage)currentPage;
	// @SuppressWarnings("restriction")
	// IViewPart viewer = page.getViewer();
	// modelExplorerView = (ModelExplorerView)viewer;
	// commonViewer = modelExplorerView.getCommonViewer();
	// ClassImpl ci = (ClassImpl) UMLUtil.resolveUMLElement(getEObject());

	// ci.setName(nameText.getText());
	// EObject eo = ci;
	// (ClassImpl) UMLUtil.resolveUMLElement(getEObject());
	// eo.
	// modelExplorerView.selectReveal(selection);
	// reveal(selection, commonViewer);
	// commonViewer.refresh(eo);

	// data.setName(nameText.getText());
	// System.out.println(nameText.getText());
	// IViewReference[] re =
	// PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getViewReferences();
	// IViewPart navigator =
	// PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().findView("org.eclipse.papyrus.views.modelexplorer.modelexplorer");//find
	// the navigator
	// //((EModelElement)navigator).get getCommonViewer().refresh(data);
	// ((org.eclipse.ui.navigator.CommonNavigator)navigator).getCommonViewer().refresh(data);
	// System.out.println("modificou: "+ e.toString());
	// System.out.println(navigator.toString());

	// ButtonElementProperties properties = (ButtonElementProperties)
	// buttonElement.getAdapter(IPropertySource.class);
	// properties.setPropertyValue(ButtonElementProperties.PROPERTY_TEXT,nameText.getText());

	// }
	// };

	
	@Override
	public void setInput(IWorkbenchPart part, ISelection selection) {
		super.setInput(part, selection);
		localSelection = selection;
		if (selection instanceof IStructuredSelection) {
			Object input = ((IStructuredSelection) selection).getFirstElement();

			Element semanticElement = UMLUtil.resolveUMLElement(input);
			if (semanticElement != null) {
				setEObject(semanticElement);
				commentUtil = new CommentUtil(semanticElement);
			}
		}
		
		retiraEspacos();
		System.out.println("passou input");
	}
	private void retiraEspacos() {
		
		
		TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(getEObject());
		domain.getCommandStack().execute(new RecordingCommand(domain) {

			@Override
			protected void doExecute() {
				Element ele = (Element) getEObject();
				
				if(ele instanceof Class) {
					if(((Class)ele).getName().contains(" ")) {
						((Class) ele).setName(((Class)ele).getName().replaceAll("\\s+",""));
					}
				}
				
				
			}

			@Override
			protected void postExecute() {
				super.postExecute();
				this.dispose();
			}
		});
		
	}

	@Override
	public void refresh() {
		super.refresh();
	}



	// private void refreshAttributs(String string) {
	// TreeItem[] itens = fTree2.getItems();
	// for (TreeItem ti : itens) {
	// ti.dispose();
	// }
	// String aux[] = string.split(",");
	// for (int i = 0; i < aux.length; i++) {
	// TreeItem item = new TreeItem(fTree2, SWT.NONE);
	// String aux2[] = aux[i].split("\\.");
	// if (aux2.length == 3) {
	// String aux3[] = new String[2];
	// aux3[0] = aux2[0] + "." + aux2[1];
	// aux3[1] = aux2[2];
	// item.setText(aux3);
	// item.setData(aux3);
	// } else if (aux2.length == 4) {
	// String aux3[] = new String[2];
	// aux3[0] = aux2[0] + "." + aux2[1];
	// aux3[1] = aux2[2] + "." + aux2[3];
	// item.setText(aux3);
	// item.setData(aux3);
	// }
	//
	// }
	// }

	protected void refreshParametros(String string) {
		TreeItem[] itens = fTree.getItems();
		for (TreeItem ti : itens) {
			ti.dispose();
		}
		String aux[] = string.split(",");
		for (int i = 0; i < aux.length; i++) {
			TreeItem item = new TreeItem(fTree, SWT.NONE);
			String aux2[] = aux[i].split("\\.");
			item.setText(aux2);
			item.setData(aux2);
		}
	}

	

	// @Override
	// public final void createControls(Composite parent, TabbedPropertySheetPage
	// aTabbedPropertySheetPage) {
	// super.createControls(parent, aTabbedPropertySheetPage);
	//
	// Composite container = getWidgetFactory().createFlatFormComposite(parent);
	// container.setLayout(new GridLayout(3, false));
	// this.container = container;
	// // toolkit = new FormToolkit(parent.getDisplay());
	//
	// // GridData labelName = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
	// // labelName.verticalSpan = 1;
	// // labelName.heightHint = 10;
	// // labelName.widthHint = 200;
	// //
	// // GridData nameData = new GridData(SWT.BEGINNING, SWT.FILL, true, true, 1,
	// 1);
	// // nameData.heightHint = 10;
	// // nameData.widthHint = 300;
	// // nameData.verticalSpan = 1;
	// //
	// //
	// // getWidgetFactory().createCLabel(container, "Name:");
	// // nameText = getWidgetFactory().createText(container, "", SWT.READ_ONLY);
	// // nameText.setLayoutData(nameData);
	// // nameText.addModifyListener(nameListener);
	// // Section section1 = toolkit.createSection(parent, Section.TITLE_BAR);
	// // section1.setText("Image General IInformations");
	//
	// // Composite composite1 = toolkit.createComposite(section1);
	// GridData labelParametros = new GridData(SWT.FILL, SWT.FILL, false, false, 1,
	// 1);
	// labelParametros.heightHint = 50;
	// labelParametros.widthHint = 50;
	// labelParametros.verticalSpan = 7;
	//
	// GridData parametrosData = new GridData(SWT.BEGINNING, SWT.BEGINNING, true,
	// false, 1, 5);
	// parametrosData.heightHint = 50;
	// parametrosData.widthHint = 350;
	// parametrosData.verticalSpan = 5;
	//
	// getWidgetFactory().createCLabel(container, "Parametros:");
	//
	// // Image image = new Image(display, "yourFile.gif");
	// Button button = new Button(container, SWT.TOGGLE);
	// button.setToolTipText("Choose the parameters!");
	// button.setImage(new Image(Display.getDefault(),
	// getClass().getResourceAsStream("/icons/Plus.gif")));
	// button.addSelectionListener(new SelectionListener() {
	//
	// @Override
	// public void widgetSelected(SelectionEvent e) {
	// ParameterDialog pd = new ParameterDialog(container.getShell(), getEObject());
	// pd.open();
	// Object[] obj = pd.getResult();
	// // String[] simpleArray = new String[obj.length];
	// //
	// // return fdstring.toArray( simpleArray );
	//
	// String teste = obj[0].toString();
	// if (getCommentParameters() != null && getCommentParameters().contains(teste))
	// {
	// // Message
	// MessageBox messageDialog = new MessageBox(container.getShell(), SWT.ERROR);
	// messageDialog.setText("Erro");
	// messageDialog.setMessage("Parametro j� inserido!!!");
	// int returnCode = messageDialog.open();
	// } else if (teste != null) {
	// adicionarParametro(teste);
	// // refressar somente os paramentros TODO
	// refresh();
	// // System.out.println(teste);
	// }
	//
	// }
	//
	// @Override
	// public void widgetDefaultSelected(SelectionEvent e) {
	//
	// }
	// });
	//
	// buttonExcParam = new Button(container, SWT.TOGGLE);
	// buttonExcParam.setToolTipText("Remove the parameter!");
	// buttonExcParam.setImage(new Image(Display.getDefault(),
	// getClass().getResourceAsStream("/icons/remove.gif")));
	// buttonExcParam.setEnabled(false);
	// buttonExcParam.addSelectionListener(new SelectionListener() {
	//
	// @Override
	// public void widgetSelected(SelectionEvent e) {
	//
	// removerParametro();
	// }
	//
	// @Override
	// public void widgetDefaultSelected(SelectionEvent e) {
	//
	// }
	// });
	//
	// // parametrosText = getWidgetFactory().createText(container, "");
	// // parametrosText.setLayoutData(parametrosData);
	//
	// Composite container2 = getWidgetFactory().createFlatFormComposite(container);
	// container2.setLayout(new GridLayout(1, false));
	//
	// // parametrosText = new Text(container2, SWT.MULTI | SWT.BORDER | SWT.WRAP |
	// // SWT.V_SCROLL);
	// // parametrosText.setLayoutData(parametrosData);
	//
	// fTree = new Tree(container2, SWT.H_SCROLL | SWT.BORDER);
	// fTree.setHeaderVisible(true);
	// GridData data = new GridData(GridData.BEGINNING);
	// fTree.setLayoutData(data);
	// data.heightHint = 50;
	// // data.widthHint = 200;
	//
	// // Turn off drawing to avoid flicker
	// fTree.setRedraw(false);
	//
	// TreeColumn instances = new TreeColumn(fTree, SWT.LEFT);
	// instances.setText("Classe");
	// instances.setWidth(300);
	// TreeColumn explicitAlloc = new TreeColumn(fTree, SWT.LEFT);
	// explicitAlloc.setText("Parametro");
	// explicitAlloc.setWidth(300);
	//
	// // inserirLinhasParametros(fTree);
	//
	// // fillTree(fTree, null, m_cdp.getMainInstance());
	//
	// // Turn drawing back on!
	// fTree.setRedraw(true);
	//
	// fTree.addSelectionListener(new SelectionAdapter() {
	//
	// @Override
	// public void widgetSelected(SelectionEvent e) {
	// if (fTree.getSelection().length > 0) {
	// buttonExcParam.setEnabled(true);
	// }
	// }
	// });
	//
	// // --------------------------------------------------
	// // getWidgetFactory().createCLabel(container, "");
	// // getWidgetFactory().createCLabel(container, "");
	// // getWidgetFactory().createCLabel(container, "onde est�");
	//
	// GridData labelElementos = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
	// labelElementos.heightHint = 80;
	// labelElementos.verticalSpan = 5;
	// labelElementos.widthHint = 200;
	//
	// GridData elementosData = new GridData(SWT.BEGINNING, SWT.BEGINNING, true,
	// false, 1, 5);
	// elementosData.heightHint = 80;
	// elementosData.widthHint = 350;
	// elementosData.verticalSpan = 5;
	//
	// getWidgetFactory().createCLabel(container2, "Elementos:");
	// elementosText = new Text(container2, SWT.MULTI | SWT.BORDER | SWT.WRAP |
	// SWT.V_SCROLL);
	// elementosText.setLayoutData(elementosData);
	//
	// Composite container3 =
	// getWidgetFactory().createFlatFormComposite(container2);
	// container3.setLayout(new GridLayout(3, false));
	//
	// GridData labelAtributos = new GridData(SWT.FILL, SWT.FILL, false, false, 1,
	// 1);
	// labelAtributos.heightHint = 50;
	// labelAtributos.widthHint = 50;
	// labelAtributos.verticalSpan = 7;
	//
	// GridData atributosData = new GridData(SWT.BEGINNING, SWT.BEGINNING, true,
	// false, 1, 5);
	// atributosData.heightHint = 50;
	// atributosData.widthHint = 350;
	// atributosData.verticalSpan = 5;
	//
	// getWidgetFactory().createCLabel(container3, "Atributos:");
	//
	// // Image image = new Image(display, "yourFile.gif");
	// Button button2 = new Button(container3, SWT.TOGGLE);
	// button2.setToolTipText("Choose the attributs!");
	// button2.setImage(new Image(Display.getDefault(),
	// getClass().getResourceAsStream("/icons/Plus.gif")));
	// button2.addSelectionListener(new SelectionListener() {
	//
	// @Override
	// public void widgetSelected(SelectionEvent e) {
	// AttributDialog pd = new AttributDialog(container3.getShell(), getEObject());
	// pd.open();
	// Object[] obj = pd.getResult();
	// // String[] simpleArray = new String[obj.length];
	// //
	// // return fdstring.toArray( simpleArray );
	//
	// String teste = obj != null ? obj[0].toString() : null;
	// if (getCommentAttributs() != null && getCommentAttributs().contains(teste)) {
	// // Message
	// MessageBox messageDialog = new MessageBox(container3.getShell(), SWT.ERROR);
	// messageDialog.setText("Erro");
	// messageDialog.setMessage("Atributo j� inserido!!!");
	// int returnCode = messageDialog.open();
	// } else if (teste != null) {
	// adicionarAtributo(teste);
	// // refressar somente os atributos TODO
	// refresh();
	// // System.out.println(teste);
	// }
	//
	// }
	//
	// @Override
	// public void widgetDefaultSelected(SelectionEvent e) {
	//
	// }
	// });
	//
	// buttonExcAttr = new Button(container3, SWT.TOGGLE);
	// buttonExcAttr.setToolTipText("Remove the attributs!");
	// buttonExcAttr.setImage(new Image(Display.getDefault(),
	// getClass().getResourceAsStream("/icons/remove.gif")));
	// buttonExcAttr.setEnabled(false);
	// buttonExcAttr.addSelectionListener(new SelectionListener() {
	//
	// @Override
	// public void widgetSelected(SelectionEvent e) {
	//
	// removerAtributo();
	//
	// }
	//
	// @Override
	// public void widgetDefaultSelected(SelectionEvent e) {
	//
	// }
	// });
	//
	// // parametrosText = getWidgetFactory().createText(container, "");
	// // parametrosText.setLayoutData(parametrosData);
	//
	// Composite container4 =
	// getWidgetFactory().createFlatFormComposite(container3);
	// container4.setLayout(new GridLayout(1, false));
	//
	// // parametrosText = new Text(container2, SWT.MULTI | SWT.BORDER | SWT.WRAP |
	// // SWT.V_SCROLL);
	// // parametrosText.setLayoutData(parametrosData);
	//
	// fTree2 = new Tree(container4, SWT.H_SCROLL | SWT.BORDER);
	// fTree2.setHeaderVisible(true);
	// GridData data2 = new GridData(GridData.BEGINNING);
	// fTree2.setLayoutData(data2);
	// data2.heightHint = 50;
	// // data.widthHint = 200;
	//
	// // Turn off drawing to avoid flicker
	// fTree2.setRedraw(false);
	//
	// TreeColumn instances2 = new TreeColumn(fTree2, SWT.LEFT);
	// instances2.setText("Atributo");
	// instances2.setWidth(300);
	// TreeColumn explicitAlloc2 = new TreeColumn(fTree2, SWT.LEFT);
	// explicitAlloc2.setText("Destino");
	// explicitAlloc2.setWidth(300);
	//
	// // inserirLinhasParametros(fTree);
	//
	// // fillTree(fTree, null, m_cdp.getMainInstance());
	//
	// // Turn drawing back on!
	// fTree2.setRedraw(true);
	//
	// fTree2.addSelectionListener(new SelectionAdapter() {
	//
	// @Override
	// public void widgetSelected(SelectionEvent e) {
	// if (fTree2.getSelection().length > 0) {
	// buttonExcAttr.setEnabled(true);
	// }
	// }
	// });
	//
	// Composite container5 =
	// getWidgetFactory().createFlatFormComposite(container4);
	// container5.setLayout(new GridLayout(2, false));
	//
	// getWidgetFactory().createCLabel(container5, "Salvar:");
	// this.createPushButton(container5);
	//
	// }

	// private void inserirLinhasParametros(Tree tree) {
	// ((Element) getEObject()).getOwnedComments()
	//
	//
	// TreeItem item = new TreeItem(tree, SWT.NONE);
	// }

	protected void adicionarParametro(String string) {
		String params = commentUtil.getCommentType("parameters");//Parameters
		String aux = "";
		if (params == null || params.equals("")) {
			aux = string;
		} else {
			aux += params + "," + string;
		}

		final String strParam = aux;
		TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(getEObject());
		domain.getCommandStack().execute(new RecordingCommand(domain) {

			@Override
			protected void doExecute() {

				commentUtil.saveCommentModel(strParam, TypeComment.PARAMETERS.getValue());//Parametros
			}

			@Override
			protected void postExecute() {
				super.postExecute();
				this.dispose();
			}
		});
	}


	
	protected void removerParametro() {

		String params = commentUtil.getCommentType("parameters");//Parameters
		String[] s = (String[]) ((TreeItem) fTree.getSelection()[0]).getData();
		String toRemove = s[0] + "." + s[1];
		String str = StringUtils.remove(params, toRemove);

		if (str.startsWith(",")) {
			str = str.substring(1);
		}
		if (str.endsWith(",")) {
			str = str.substring(0, str.length() - 1);
		}
		if (str.contains(",,")) {
			str = str.replace(",,",",");
		}
		//
		//
		//
		final String strParam = str;
		TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(getEObject());
		domain.getCommandStack().execute(new RecordingCommand(domain) {

			@Override
			protected void doExecute() {

				//saveCommentParametrosModel(strParam);
				commentUtil.saveCommentModel(strParam, TypeComment.PARAMETERS.getValue());//Parametros
			}

			@Override
			protected void postExecute() {
				super.postExecute();
				this.dispose();
			}
		});
		((TreeItem) fTree.getSelection()[0]).dispose();

	}
	
	// protected void adicionarAtributo(String string) {
	// String params = getCommentAttributs();
	// String aux = "";
	// if (params == null || params.equals("")) {
	// aux = string;
	// } else {
	// aux += params + "," + string;
	// }
	//
	// final String strParam = aux;
	// TransactionalEditingDomain domain =
	// TransactionUtil.getEditingDomain(getEObject());
	// domain.getCommandStack().execute(new RecordingCommand(domain) {
	//
	// @Override
	// protected void doExecute() {
	//
	// saveCommentAtributosModel(strParam);
	// }
	//
	// @Override
	// protected void postExecute() {
	// super.postExecute();
	// this.dispose();
	// }
	// });
	// }



	// protected void removerAtributo() {
	//
	// String params = getCommentAttributs();
	// String[] s = (String[]) ((TreeItem) fTree2.getSelection()[0]).getData();
	// String toRemove = s[0] + "." + s[1];
	// String str = StringUtils.remove(params, toRemove);
	//
	// if (str.startsWith(",")) {
	// str = str.substring(1);
	// }
	// if (str.endsWith(",")) {
	// str = str.substring(0, str.length() - 1);
	// }
	//
	// //
	// //
	// //
	// final String strParam = str;
	// TransactionalEditingDomain domain =
	// TransactionUtil.getEditingDomain(getEObject());
	// domain.getCommandStack().execute(new RecordingCommand(domain) {
	//
	// @Override
	// protected void doExecute() {
	//
	// saveCommentAtributosModel(strParam);
	// }
	//
	// @Override
	// protected void postExecute() {
	// super.postExecute();
	// this.dispose();
	// }
	// });
	// ((TreeItem) fTree2.getSelection()[0]).dispose();
	//
	// // String params = getCommentAttributs();
	// // String aux = "";
	// // if(params == null || params.equals("")) {
	// // aux = string;
	// // }else {
	// // aux += params+","+string;
	// // }
	// //
	// // final String strParam = aux;
	// // TransactionalEditingDomain domain =
	// // TransactionUtil.getEditingDomain(getEObject());
	// // domain.getCommandStack().execute(new RecordingCommand(domain) {
	// //
	// // @Override
	// // protected void doExecute() {
	// //
	// // saveCommentAtributosModel(strParam);
	// // }
	// // });
	// }
	
//	protected void createPushButton2(final Composite parent) {
//		
//		Button commitButton2 = new Button(parent, SWT.PUSH);
//		 commitButton2.setText("Xtext");
//		commitButton2.setToolTipText("Save the specification card in your model");
//		commitButton2
//				.setImage(new Image(Display.getDefault(), getClass().getResourceAsStream("/icons/validate.gif")));
//
//		commitButton2.addSelectionListener(new SelectionListener() {
//
//			@Override
//			public void widgetSelected(SelectionEvent e) {
//				try {
//					umlUtil.updateXtextAdapters(styledText, element);
//				}catch (Exception ex) {
//					ex.printStackTrace();
//					
//				}
//				
//			}
//
//			@Override
//			public void widgetDefaultSelected(SelectionEvent e) {
//			}
//		});
//	
//		
//	}
	

	protected void createPushButton(final Composite parent) {
		final AlfJobObserver observer = new AlfJobObserver(this.commitButton);
		this.commitButton = new Button(parent, SWT.PUSH);
		this.commitButton.setText("Save");
		this.commitButton.setToolTipText("Save the specification card in your model");
		this.commitButton
				.setImage(new Image(Display.getDefault(), getClass().getResourceAsStream("/icons/validate.gif")));
		// this.commitButton.addSelectionListener(new
		// CommitButtonSelectionListener(this));
		this.commitButton.addDisposeListener(new DisposeListener() {

			@Override
			public void widgetDisposed(DisposeEvent e) {
				Job.getJobManager().removeJobChangeListener(observer);
			}
		});

		this.commitButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				try {
					//commitButton.setEnabled(false);
					
					specifyCard();
					IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
					IEditorPart editor = page.getActiveEditor();
					page.saveEditor(editor, false /* confirm */);
					specifyCard2();
										
//					page.saveEditor(editor, false /* confirm */);
//					specifyCard2();
					//commitButton.setEnabled(true);
				}catch (Exception ex) {
					ex.printStackTrace();
					//commitButton.setEnabled(true);
				}
				
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		Job.getJobManager().addJobChangeListener(new AlfJobObserver(this.commitButton));
		
//		Button cm = new Button(parent, SWT.PUSH);
//		cm.setText("Save2");
//		cm.setToolTipText("Save the specification card in your model");
//		cm.setImage(new Image(Display.getDefault(), getClass().getResourceAsStream("/icons/validate.gif")));
//		// this.commitButton.addSelectionListener(new
//		// CommitButtonSelectionListener(this));
//		cm.addDisposeListener(new DisposeListener() {
//
//			@Override
//			public void widgetDisposed(DisposeEvent e) {
//			}
//		});
//
//		cm.addSelectionListener(new SelectionListener() {
//
//			@Override
//			public void widgetSelected(SelectionEvent e) {
//				try {
//					cm.setEnabled(false);
//					//specifyCard();
//					IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
//					IEditorPart editor = page.getActiveEditor();
//					page.saveEditor(editor, false /* confirm */);
//					specifyCard2();
//					cm.setEnabled(true);
//					IWorkbenchPage page2 = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
//					IEditorPart editor2 = page2.getActiveEditor();
//					page2.saveEditor(editor2, false /* confirm */);
//				}catch (Exception ex) {
//					ex.printStackTrace();
//				}
//				
//			}
//
//			@Override
//			public void widgetDefaultSelected(SelectionEvent e) {
//			}
//		});
	}
	
	
	

	// @Override
	// public void createControls(Composite parent, TabbedPropertySheetPage
	// aTabbedPropertySheetPage) {
	// super.createControls(parent, aTabbedPropertySheetPage);
	// ------------------------------------------

	// toolkit = new FormToolkit(parent.getDisplay());
	// toolkit.setBorderStyle(SWT.BORDER);
	// super.createControls(parent, aTabbedPropertySheetPage);
	// GridDataFactory.fillDefaults().grab(true, true).applyTo(parent);
	// parent.setLayout(new GridLayout(2, true));
	// form = toolkit.createForm(parent);
	// toolkit.decorateFormHeading(form);
	// GridDataFactory.fillDefaults().grab(true, true).applyTo(form);
	// form.getBody().setLayout(new GridLayout(3, false));
	//
	// Section section = toolkit.createSection(parent, Section.TITLE_BAR);
	// section.setText("Image General IInformations");
	// Composite composite = toolkit.createComposite(section);
	// section.setClient(composite);
	// section.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true,
	// false));
	//
	// // Place the widgets
	// Label labelColor = toolkit.createLabel(composite, "Select Color:");
	// Text textColor = toolkit.createText(composite, "OxFFFFFF", SWT.READ_ONLY);
	// Button buttonColor = toolkit.createButton(composite, "...", SWT.NONE);
	//
	// Label labelReset = toolkit.createLabel(composite, "Reset Data:");
	// Button buttonReset = toolkit.createButton(composite, "Reset", SWT.NONE);
	//
	// Label labelTrans = toolkit.createLabel(composite, "Color Gradient Order:");
	// Text comboTrans = toolkit.createText(composite, "Test");
	//
	// // Do the Layout
	// GridData gridDataBeg = new GridData(GridData.BEGINNING, GridData.CENTER,
	// false, false);
	// GridData gridDataEnd = new GridData(GridData.END, GridData.CENTER, false,
	// false);
	// GridData gridDataMid = new GridData(GridData.FILL, GridData.CENTER, true,
	// false);
	// GridData gridDataSpan2 = new GridData(GridData.FILL, GridData.CENTER, false,
	// false);
	// gridDataSpan2.horizontalSpan = 2;
	//
	// labelTrans.setLayoutData(gridDataBeg);
	// comboTrans.setLayoutData(gridDataSpan2);
	//
	// labelColor.setLayoutData(gridDataBeg);
	// textColor.setLayoutData(gridDataMid);
	// buttonColor.setLayoutData(gridDataEnd);
	//
	// labelReset.setLayoutData(gridDataBeg);
	// buttonReset.setLayoutData(gridDataSpan2);

	// ------------------------------------------------
	//
	// Composite container = getWidgetFactory().createFlatFormComposite(parent);
	// container.setLayout(new GridLayout(2,false));

	// -------------------------------

	// -------------------------------

	// --------------------dsfffd-----------

	// -------------------

	// toolkit = new FormToolkit(parent.getDisplay());
	// toolkit.setBorderStyle(SWT.BORDER);
	// super.createControls(parent, aTabbedPropertySheetPage);
	// GridDataFactory.fillDefaults().grab(true, true).applyTo(parent);
	// parent.setLayout(new GridLayout(2, true));
	// form = toolkit.createForm(parent);
	// toolkit.decorateFormHeading(form);
	// GridDataFactory.fillDefaults().grab(true, true).applyTo(form);
	// form.getBody().setLayout(new GridLayout(3, false));
	//

	// -----------------------------------------

	// Section section = toolkit.createSection(parent, Section.TITLE_BAR);
	// section.setText("Image General IInformations");
	// Composite composite = toolkit.createComposite(section);
	// section.setClient(composite);
	// section.setLayoutData(new GridData(GridData.FILL, GridData.FILL, true,
	// false));
	//
	// // Place the widgets
	// Label labelColor = toolkit.createLabel(composite, "Select Color:");
	// Text textColor = toolkit.createText(composite, "OxFFFFFF", SWT.READ_ONLY);
	// Button buttonColor = toolkit.createButton(composite, "...", SWT.NONE);
	//
	// Label labelReset = toolkit.createLabel(composite, "Reset Data:");
	// Button buttonReset = toolkit.createButton(composite, "Reset", SWT.NONE);
	//
	// Label labelTrans = toolkit.createLabel(composite, "Color Gradient Order:");
	// Text comboTrans = toolkit.createText(composite, "Test");
	//
	// // Do the Layout
	// GridData gridDataBeg = new GridData(GridData.BEGINNING, GridData.CENTER,
	// false, false);
	// GridData gridDataEnd = new GridData(GridData.END, GridData.CENTER, false,
	// false);
	// GridData gridDataMid = new GridData(GridData.FILL, GridData.CENTER, true,
	// false);
	// GridData gridDataSpan2 = new GridData(GridData.FILL, GridData.CENTER, false,
	// false);
	// gridDataSpan2.horizontalSpan = 2;
	//
	// labelTrans.setLayoutData(gridDataBeg);
	// comboTrans.setLayoutData(gridDataSpan2);
	//
	// labelColor.setLayoutData(gridDataBeg);
	// textColor.setLayoutData(gridDataMid);
	// buttonColor.setLayoutData(gridDataEnd);
	//
	// labelReset.setLayoutData(gridDataBeg);
	// buttonReset.setLayoutData(gridDataSpan2);
	//
	// ----------------------------------------------

	// GridData nameData = new GridData(SWT.BEGINNING, SWT.FILL, true, true, 1,1);
	// GridData descriptionData = new GridData(SWT.FILL, SWT.FILL, true, true, 1,1);
	// nameData.heightHint = 10;
	// nameData.widthHint = 150;
	// descriptionData.heightHint = 100;
	//
	// getWidgetFactory().createCLabel((org.eclipse.swt.widgets.Composite) parent,
	// "Name:");
	// nameText = getWidgetFactory().createText((org.eclipse.swt.widgets.Composite)
	// parent, "");
	// nameText.setLayoutData(nameData);
	//
	// createTextControl2(form.getBody());
	//

	//
	// }

	// protected void createTextControl2(final Composite parent) {
	//
	// textControl = new StyledText(parent, SWT.MULTI | SWT.BORDER
	// | SWT.V_SCROLL | SWT.WRAP);
	//
	//
	//
	// textControl.setAlwaysShowScrollBars(false);
	// GridDataFactory.fillDefaults().grab(true, true).hint(parent.getSize())
	// .applyTo(textControl);
	// textControl.addExtendedModifyListener(new ExtendedModifyListener() {
	//
	// public void modifyText(ExtendedModifyEvent event) {
	// if (isUndo) {
	// undoRedoStack.pushRedo(event);
	// } else { // is Redo or a normal user action
	// undoRedoStack.pushUndo(event);
	// if (!isRedo) {
	// undoRedoStack.clearRedo();
	// }
	// }
	// }
	// });
	//
	// textControl.addKeyListener(new KeyAdapter() {
	// @Override
	// public void keyPressed(KeyEvent e) {
	// boolean isCtrl = (e.stateMask & SWT.CTRL) > 0;
	// boolean isAlt = (e.stateMask & SWT.ALT) > 0;
	// if (isCtrl && !isAlt) {
	// boolean isShift = (e.stateMask & SWT.SHIFT) > 0;
	// if (e.keyCode == 'z') {
	// if (isShift) {
	// redo();
	// }
	// else {
	// undo();
	// }
	// }
	// }
	// }
	// });
	// }

	// protected void createTextControl(final Composite parent) {
	//
	// textControl = new StyledText(parent, SWT.MULTI | SWT.BORDER
	// | SWT.V_SCROLL | SWT.WRAP);
	//
	// textControl.setAlwaysShowScrollBars(false);
	// GridDataFactory.fillDefaults().grab(true, true).hint(parent.getSize())
	// .applyTo(textControl);
	// textControl.addExtendedModifyListener(new ExtendedModifyListener() {
	//
	// public void modifyText(ExtendedModifyEvent event) {
	// if (isUndo) {
	// undoRedoStack.pushRedo(event);
	// } else { // is Redo or a normal user action
	// undoRedoStack.pushUndo(event);
	// if (!isRedo) {
	// undoRedoStack.clearRedo();
	// }
	// }
	// }
	// });
	//
	// textControl.addKeyListener(new KeyAdapter() {
	// @Override
	// public void keyPressed(KeyEvent e) {
	// boolean isCtrl = (e.stateMask & SWT.CTRL) > 0;
	// boolean isAlt = (e.stateMask & SWT.ALT) > 0;
	// if (isCtrl && !isAlt) {
	// boolean isShift = (e.stateMask & SWT.SHIFT) > 0;
	// if (e.keyCode == 'z') {
	// if (isShift) {
	// redo();
	// } else {
	// undo();
	// }
	// }
	// }
	// }
	// });
	//
	// // textControl.addFocusListener(new EditorFocusListener(this));
	// }

	// /**
	// * Reverts the given modify event, in the way as the Eclipse text editor
	// * does it.
	// *
	// * @param event
	// */
	// private void revertEvent(ExtendedModifyEvent event) {
	// textControl.replaceTextRange(event.start, event.length, event.replacedText);
	// // (causes the modifyText() listener method to be called)
	//
	// textControl.setSelectionRange(event.start, event.replacedText.length());
	// }
	//
	//
	// protected void undo() {
	// if (undoRedoStack.hasUndo()) {
	// isUndo = true;
	// revertEvent(undoRedoStack.popUndo());
	// isUndo = false;
	// }
	// }
	//
	// protected void redo() {
	// if (undoRedoStack.hasRedo()) {
	// isRedo = true;
	// revertEvent(undoRedoStack.popRedo());
	// isRedo = false;
	// }
	// }

	// /**
	// * From a selection this methods tries to extract the underlying model element
	// *
	// * @param selectedElement
	// * an object selected in the view (e.g., a class in a diagram)
	// *
	// * @return semanticElement
	// * the model element that is under the graphical element (may be null)
	// */
	// private Element resolveSemanticElement(Object selectedElement){
	// Element semanticElement = null;
	// if (selectedElement instanceof IAdaptable) {
	// semanticElement = (Element) ((IAdaptable)
	// selectedElement).getAdapter(EObject.class);
	// }
	// else if (selectedElement instanceof GraphicalEditPart) {
	// GraphicalEditPart part = (GraphicalEditPart) selectedElement;
	// semanticElement = (Element)part.resolveSemanticElement();
	// }
	// return semanticElement;
	// }

	// protected String resolvComment(String str) {
	//
	// String ret = "";
	//// switch (typeComment) {
	//// case 1:
	// // TypeComment.PARAMETERS.getValue()
	// String aux[] = str.split("://");
	// if (aux[0] == "parameters") {
	// ret = aux[1];
	// }else
	// //break;
	//
	//// case 2:
	// // TypeComment.ELEMENTS.getValue()
	//
	//// String aux2[] = str.split("://");
	// if (aux[0] == "elements") {
	// ret = aux[1];
	// }
	// //break;
	//// }
	//
	// return ret;
	// }

	
	//
	// protected void specifyCard() {
	//
	// TransactionalEditingDomain domain =
	// TransactionUtil.getEditingDomain(getEObject());
	// domain.getCommandStack().execute(new RecordingCommand(domain) {
	//
	// @Override
	// protected void doExecute() {
	//
	// try {
	// fazerValidacao();
	//
	// saveCommentElementosModel();
	//
	// importPackageOohdmIfNotApply();
	//
	// Element modelGenerate = getModelGenerated();
	//
	// Element classGerente = generateGerenteClass(modelGenerate,
	// getGerenteAtual());
	//
	// Element operationGerente = generateGerenteOperation(classGerente);
	//
	//
	// Element behaviorOperation = generateBehaviorOperation(classGerente,
	// operationGerente);// alf
	//
	//
	// String alfCode = buildAlfCode(operationGerente); // TODO verificar se a
	// valida��o do alf ser� aqui.
	// System.out.println(alfCode);
	//
	// // deve
	// //chamar valida��o
	//
	//
	////
	//// ResourceSet resourceSet = new ResourceSetImpl();
	//// Resource resource =
	// resourceSet.createResource(URI.createURI("resource.alf"));
	//// resource.load(new URIConverter.ReadableInputStream(alfCode), null);
	////
	//// Resource original = (org.eclipse.emf.ecore.resource.Resource) resource;
	//// if(original.getErrors().size() != 0 || original.getWarnings().size() != 0
	// ||
	// Diagnostician.INSTANCE.validate(resource.getContents().get(0)).getSeverity()
	// != Diagnostic.OK){
	//// // input invalid
	//// System.out.println(original.getErrors().toString());
	//// System.out.println(Diagnostician.INSTANCE.validate(resource.getContents().get(0)).getSeverity());
	//// } else {
	//// // input valid
	//// }
	//
	//
	//
	// //executar alf
	// NamedElement semanticObject = ((NamedElement)behaviorOperation);
	// if (semanticObject != null && alfCode != null) {
	// ScenarioFactory.getInstance().createCommitScenario().execute(semanticObject,
	// alfCode);
	// }
	//
	////
	// Element elementRe = reGenerateBehaviorOperation(classGerente,
	// operationGerente);
	//
	////
	//// IWorkbenchPage page =
	// PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
	//// IEditorPart editor = page.getActiveEditor();
	//// page.saveEditor(editor, false /* confirm */);
	//
	//
	//
	//
	// } catch (Exception e) {
	// MessageBox messageDialog = new MessageBox(container.getShell(), SWT.ERROR);
	// messageDialog.setText("Erro");
	// messageDialog.setMessage("Erro ao compilar c�digo alf!");
	// messageDialog.open();
	// }
	//
	// }
	//
	// @Override
	// protected void postExecute() {
	// super.postExecute();
	// }
	//
	// });
	//
	// }

	abstract protected void specifyCard();
	 abstract protected void specifyCard2();

//	private Namespace getNamespace(Element element) {
//		if (element != null && element instanceof NamedElement) {
//			return ((NamedElement) element).getNamespace();
//		}
//		return null;
//	}


	


	
	protected Element generateBehaviorOperation(Element classGerente, Element operationGerente, Class classContext) {
		
		return umlUtil.generateBehaviorOperation(classGerente, operationGerente, classContext, commentUtil.getCommentType("parameters"), null, getEObject(),false);
	}

//	protected Element reGenerateBehaviorOperation(Element classGerente, Element opGerente) {
//		List<Activity> elementos2 = new ArrayList<Activity>();
//		String name = ((Operation) opGerente).getName() + "Impl";
//		Element retorno = null;
//
//		for (Element ele : ((Class) classGerente).getOwnedElements()) {
//			if (ele instanceof Activity) {
//				if (((Activity) ele).getName().equals(name)) {
//					((Activity) ele).setSpecification((Operation) opGerente);
//					retorno = ele;
//
//					// elementos2.add(ele);
//				} else {
//					elementos2.add((Activity) ele);
//				}
//			}
//		}
//		if (retorno == null) {
//			// novo
//			Activity act = UMLFactory.eINSTANCE.createActivity();
//			act.setName(name);// TODO CRIAR CONSTANTE
//			act.setIsActive(true);
//			act.setSpecification((Operation) opGerente);
//			elementos2.add(act);
//			retorno = act;
//		} else {
//			// modificado
//			elementos2.add((Activity) retorno);
//		}
//
//		classGerente.eSet(classGerente.eClass().getEStructuralFeature("ownedBehavior"), elementos2);
//
//		return retorno;
//	}

	protected void importPackageOohdmIfNotApply() {
		List<PackageImport> elementos2 = new ArrayList<PackageImport>();
		Element e1 = ((Element) getEObject()).getModel().getOwner();
		List<Package> elementos = ((Model) e1).getImportedPackages();

		for (Package element : elementos) {
			Model m = element.getModel();
//			String str = m.getName();
			if (m.getName() != null && m.getName().equals("oohdmModel")) { // TODO criar constante
				// if (element instanceof org.eclipse.uml2.uml.PackageImport) {
				// ("pathmap://resources/oohdm_library/oohdmModel.uml")
				return;
				// }
			} else/* if(element instanceof Model) */ {
				PackageImport pi = UMLFactory.eINSTANCE.createPackageImport();
				pi.setImportedPackage(element);
				elementos2.add(pi);
			}
		}
		PackageImport pi = UMLFactory.eINSTANCE.createPackageImport();
		pi.setImportedPackage(getOOHDMPackage());
		elementos2.add(pi);

		Element ele = ((Element) getEObject()).getModel().getOwner();
		if (ele.getModel().getName().equals(umlUtil.getNameRootModel())) {
			ele.eSet(ele.eClass().getEStructuralFeature("packageImport"), elementos2);
			// ele.eGet(ele.eClass().getEStructuralFeature("packageImport"));

		}
	}

	
	
	protected void importPackageAlfIfNotApply() {
		List<PackageImport> elementos2 = new ArrayList<PackageImport>();
		Element e1 = ((Element) getEObject()).getModel().getOwner();
		List<Package> elementos = ((Model) e1).getImportedPackages();

		for (Package element : elementos) {
			Model m = element.getModel();
//			String str = m.getName();
			if (m.getName() != null && m.getName().equals("Alf")) { // TODO criar constante
				// if (element instanceof org.eclipse.uml2.uml.PackageImport) {
				// ("pathmap://resources/oohdm_library/oohdmModel.uml")
				return;
				// }
			} else/* if(element instanceof Model) */ {
				PackageImport pi = UMLFactory.eINSTANCE.createPackageImport();
				pi.setImportedPackage(element);
				elementos2.add(pi);
			}
		}
		PackageImport pi = UMLFactory.eINSTANCE.createPackageImport();
		pi.setImportedPackage(getAlfPackage());
		elementos2.add(pi);

		Element ele = ((Element) getEObject()).getModel().getOwner();
		if (ele.getModel().getName().equals(umlUtil.getNameRootModel())) {
			ele.eSet(ele.eClass().getEStructuralFeature("packageImport"), elementos2);
			// ele.eGet(ele.eClass().getEStructuralFeature("packageImport"));

		}
	}
	
	protected void importPackageAssertionLibraryIfNotApply(URI modelUri) {
		List<PackageImport> elementos2 = new ArrayList<PackageImport>();
		Element e1 = ((Element) getEObject()).getModel().getOwner();
		List<Package> elementos = ((Model) e1).getImportedPackages();

		for (Package element : elementos) {
			Model m = element.getModel();
//			String str = m.getName();
			if (m.getName() != null && m.getName().equals("AssertionLibrary")) { // TODO criar constante
				// if (element instanceof org.eclipse.uml2.uml.PackageImport) {
				// ("pathmap://resources/oohdm_library/oohdmModel.uml")
				return;
				// }
			} else/* if(element instanceof Model) */ {
				PackageImport pi = UMLFactory.eINSTANCE.createPackageImport();
				pi.setImportedPackage(element);
				elementos2.add(pi);
			}
		}
		PackageImport pi = UMLFactory.eINSTANCE.createPackageImport();
		pi.setImportedPackage(getAssertionLibraryPackage(modelUri));
		elementos2.add(pi);

		Element ele = ((Element) getEObject()).getModel().getOwner();
		if (ele.getModel().getName().equals(umlUtil.getNameRootModel())) {
			ele.eSet(ele.eClass().getEStructuralFeature("packageImport"), elementos2);
			// ele.eGet(ele.eClass().getEStructuralFeature("packageImport"));

		}
	}
	protected void importPackageEcorePrimitiveTypesIfNotApply(URI modelUri) {
		List<PackageImport> elementos2 = new ArrayList<PackageImport>();
		Element e1 = ((Element) getEObject()).getModel().getOwner();
		List<Package> elementos = ((Model) e1).getImportedPackages();

		for (Package element : elementos) {
			Model m = element.getModel();
//			String str = m.getName();
			if (m.getName() != null && m.getName().equals("EcorePrimitiveTypes")) { // TODO criar constante
				// if (element instanceof org.eclipse.uml2.uml.PackageImport) {
				// ("pathmap://resources/oohdm_library/oohdmModel.uml")
				return;
				// }
			} else/* if(element instanceof Model) */ {
				PackageImport pi = UMLFactory.eINSTANCE.createPackageImport();
				pi.setImportedPackage(element);
				elementos2.add(pi);
			}
		}
		PackageImport pi = UMLFactory.eINSTANCE.createPackageImport();
		pi.setImportedPackage(getEcorePrimitiveTypesPackage(modelUri));
		elementos2.add(pi);

		Element ele = ((Element) getEObject()).getModel().getOwner();
		if (ele.getModel().getName().equals(umlUtil.getNameRootModel())) {
			ele.eSet(ele.eClass().getEStructuralFeature("packageImport"), elementos2);
			// ele.eGet(ele.eClass().getEStructuralFeature("packageImport"));

		}
	}
	protected void importPackageFoundationalModelLibraryIfNotApply(URI modelUri) {
		List<PackageImport> elementos2 = new ArrayList<PackageImport>();
		Element e1 = ((Element) getEObject()).getModel().getOwner();
		List<Package> elementos = ((Model) e1).getImportedPackages();

		for (Package element : elementos) {
			Model m = element.getModel();
//			String str = m.getName();
			if (m.getName() != null && m.getName().equals("FoundationalModelLibrary")) { // TODO criar constante
				// if (element instanceof org.eclipse.uml2.uml.PackageImport) {
				// ("pathmap://resources/oohdm_library/oohdmModel.uml")
				return;
				// }
			} else/* if(element instanceof Model) */ {
				PackageImport pi = UMLFactory.eINSTANCE.createPackageImport();
				pi.setImportedPackage(element);
				elementos2.add(pi);
			}
		}
		PackageImport pi = UMLFactory.eINSTANCE.createPackageImport();
		pi.setImportedPackage(getFoundationalModelLibraryPackage(modelUri));
		elementos2.add(pi);

		Element ele = ((Element) getEObject()).getModel().getOwner();
		if (ele.getModel().getName().equals(umlUtil.getNameRootModel())) {
			ele.eSet(ele.eClass().getEStructuralFeature("packageImport"), elementos2);
			// ele.eGet(ele.eClass().getEStructuralFeature("packageImport"));

		}
	}
	
	protected void importPackagePrimitiveTypesIfNotApply(URI modelUri) {
		List<PackageImport> elementos2 = new ArrayList<PackageImport>();
		Element e1 = ((Element) getEObject()).getModel().getOwner();
		List<Package> elementos = ((Model) e1).getImportedPackages();

		for (Package element : elementos) {
			Model m = element.getModel();
//			String str = m.getName();
			if (m.getName() != null && m.getName().equals("PrimitiveTypes")) { // TODO criar constante
				// if (element instanceof org.eclipse.uml2.uml.PackageImport) {
				// ("pathmap://resources/oohdm_library/oohdmModel.uml")
				return;
				// }
			} else/* if(element instanceof Model) */ {
				PackageImport pi = UMLFactory.eINSTANCE.createPackageImport();
				pi.setImportedPackage(element);
				elementos2.add(pi);
			}
		}
		PackageImport pi = UMLFactory.eINSTANCE.createPackageImport();
		pi.setImportedPackage(getPrimitiveTypesPackage(modelUri));
		elementos2.add(pi);

		Element ele = ((Element) getEObject()).getModel().getOwner();
		if (ele.getModel().getName().equals(umlUtil.getNameRootModel())) {
			ele.eSet(ele.eClass().getEStructuralFeature("packageImport"), elementos2);
			// ele.eGet(ele.eClass().getEStructuralFeature("packageImport"));

		}
	}
	
	protected void importPackageCollectionClassesImplIfNotApply(URI modelUri) {
		List<PackageImport> elementos2 = new ArrayList<PackageImport>();
		Element e1 = ((Element) getEObject()).getModel().getOwner();
		List<Package> elementos = ((Model) e1).getImportedPackages();

		for (Package element : elementos) {
			Model m = element.getModel();
//			String str = m.getName();
			if (m.getName() != null && m.getName().equals("CollectionClassesImpl")) { // TODO criar constante
				// if (element instanceof org.eclipse.uml2.uml.PackageImport) {
				// ("pathmap://resources/oohdm_library/oohdmModel.uml")
				return;
				// }
			} else/* if(element instanceof Model) */ {
				PackageImport pi = UMLFactory.eINSTANCE.createPackageImport();
				pi.setImportedPackage(element);
				elementos2.add(pi);
			}
		}
		PackageImport pi = UMLFactory.eINSTANCE.createPackageImport();
		pi.setImportedPackage(getCollectionClassesImplPackage(modelUri));
		elementos2.add(pi);

		Element ele = ((Element) getEObject()).getModel().getOwner();
		if (ele.getModel().getName().equals(umlUtil.getNameRootModel())) {
			ele.eSet(ele.eClass().getEStructuralFeature("packageImport"), elementos2);
			// ele.eGet(ele.eClass().getEStructuralFeature("packageImport"));

		}
	}


	protected void fazerValidacao() {
		// TODO Auto-generated method stub
		// tem que ter pelo menos o campo elemento
		// e um atributo com destino.
	}

	

	protected String buildAlfCodeClass(Element operationGerente) {

		
		String name = ((Class) operationGerente).getName();
				
		String alfCode = GerenteCreator.createClassText(name).toString();
				
		return alfCode;
	}
	// protected String getGerenteAtual() {
	//
	// String aux = getCommentAttributs();
	//
	// if (aux.split("\\.").length > 1) {
	// return aux.split("\\.")[0];
	// }
	//
	// return null;
	// }
	
	

	protected Element getModelGenerated() {
			
		return umlUtil.createOrSelectModelElement("Generated", getEObject());
		
	}



	protected Package getOOHDMPackage() {

		// PackageImport packageImport = UMLFactory.eINSTANCE.createPackageImport();
		Resource modelResource = umlUtil.getResourceOohdmModel(getEObject());
		return (Package) modelResource.getContents().get(0);
	}
	
	protected Package getAlfPackage() {

		// PackageImport packageImport = UMLFactory.eINSTANCE.createPackageImport();
		Resource modelResource = umlUtil.getResourceAlfModel(getEObject());
		return (Package) modelResource.getContents().get(0);
	}

	protected Package getAssertionLibraryPackage(URI modelUri) {

		// PackageImport packageImport = UMLFactory.eINSTANCE.createPackageImport();
		Resource modelResource = umlUtil.getResourceAssertionLibraryModel(getEObject(),modelUri);
		return (Package) modelResource.getContents().get(0);
	}
	protected Package getEcorePrimitiveTypesPackage(URI modelUri) {

		// PackageImport packageImport = UMLFactory.eINSTANCE.createPackageImport();
		Resource modelResource = umlUtil.getResourceEcorePrimitiveTypesModel(getEObject(),modelUri);
		return (Package) modelResource.getContents().get(0);
	}
	protected Package getFoundationalModelLibraryPackage(URI modelUri) {

		// PackageImport packageImport = UMLFactory.eINSTANCE.createPackageImport();
		Resource modelResource = umlUtil.getResourceFoundationalModelLibraryModel(getEObject(),modelUri);
		return (Package) modelResource.getContents().get(0);
	}
	protected Package getPrimitiveTypesPackage(URI modelUri) {

		// PackageImport packageImport = UMLFactory.eINSTANCE.createPackageImport();
		Resource modelResource = umlUtil.getResourcePrimitiveTypesModel(getEObject(),modelUri);
		return (Package) modelResource.getContents().get(0);
	}
	
	protected Package getCollectionClassesImplPackage(URI modelUri) {

		// PackageImport packageImport = UMLFactory.eINSTANCE.createPackageImport();
		Resource modelResource = umlUtil.getResourceCollectionClassesImplModel(getEObject(),modelUri);
		return (Package) modelResource.getContents().get(0);
	}
	// protected String buildAlfCode(Element operationGerente) {
	//
	// String in = ((Class)eObject).getName();
	// String namespace = ((Class)((Operation)
	// operationGerente).getOwner()).getModel().getName()+"::"+((Class)((Operation)
	// operationGerente).getOwner()).getName();
	// String behaviorName = ((Operation) operationGerente).getName() + "Impl";
	//
	// GerenteIndexTransition git =
	// new GerenteIndexTransition(namespace, behaviorName, in,
	// getCommentElements(), getCommentAttributs(), getCommentParameters());
	//
	// String alfCode = GerenteIndexCreator.createGerenteIndex(git).toString();
	//
	// return alfCode;
	// }



	protected void addSort(String string) {
		String params = getCommentSort();
		String aux = "";
		if (params == null || params.equals("")) {
			aux = string;
		} else {
			aux += params + "," + string;
		}

		final String strParam = aux;
		TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(getEObject());
		domain.getCommandStack().execute(new RecordingCommand(domain) {

			@Override
			protected void doExecute() {

				saveCommentSortModel(strParam);
			}

			@Override
			protected void postExecute() {
				super.postExecute();
				this.dispose();
			}
		});
	}
	
	protected void saveCommentSortModel(String strParamns) {
		List<Comment> commentList = new ArrayList<Comment>();
		commentUtil.removeComment(TypeComment.SORT.getValue());
		Comment comentA = UMLFactory.eINSTANCE.createComment();

		comentA.setBody(commentUtil.compileComment(strParamns, TypeComment.SORT.getValue()));
		commentList.add(comentA);
		((Element) getEObject()).getOwnedComments().add(comentA);

	}


	protected void removeSort() {

		String params = getCommentSort();
		String[] s = (String[]) ((TreeItem) fTree2.getSelection()[0]).getData();
		String toRemove = s[0] + "." + s[1];
		String str = StringUtils.remove(params, toRemove);

		if (str.startsWith(",")) {
			str = str.substring(1);
		}
		if (str.endsWith(",")) {
			str = str.substring(0, str.length() - 1);
		}

		if (str.contains(",,")) {
			str = str.replace(",,",",");
		}
		//
		//
		//
		final String strParam = str;
		TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(getEObject());
		domain.getCommandStack().execute(new RecordingCommand(domain) {

			@Override
			protected void doExecute() {

				saveCommentSortModel(strParam);
			}

			@Override
			protected void postExecute() {
				super.postExecute();
				this.dispose();
			}
		});
		((TreeItem) fTree2.getSelection()[0]).dispose();

	}
	

	protected String getCommentSort() {

		for (Comment co : commentUtil.getComments(TypeComment.SORT.getValue())) {

			String aux[] = co.getBody().split("://");
			if (aux[0].equals("sort")) {// TODO criar constante
				if (aux.length == 2)
					return aux[1];
			}

		}
		return null;

	}

	
	
	public Composite getContainer() {
		return container;
	}

	public void setContainer(Composite container) {
		this.container = container;
	}

//	public List<Diagram> getDiagrams(){
//		try{
//			Resource notationResource;
//			IMultiDiagramEditor editor = EditorUtils.getMultiDiagramEditor();
//			notationResource = NotationUtils.getNotationModel(editor.getServicesRegistry().getService(ModelSet.class)).getResource();
//			@SuppressWarnings("unchecked")
//			List<Diagram> list = (List<Diagram>)(List<?>) notationResource.getContents();
//			return list;
//		}catch(ServiceException e){
//			throw new RuntimeException(e);
//		}
//	}
	
	
	
}
