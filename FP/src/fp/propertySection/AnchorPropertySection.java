package fp.propertySection;

import org.apache.commons.lang.StringUtils;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.papyrus.uml.tools.utils.UMLUtil;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Element;

import fp.dialog.AnchorDialog;
import fp.util.CommentUtil;
import fp.util.TypeComment;

public class AnchorPropertySection extends AbstractPropertySectionOOHDM {

	private Tree fTreeAnchor;
	Button buttonExcAnchor;
	public AnchorPropertySection() {
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void specifyCard() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void specifyCard2() {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void setInput(IWorkbenchPart part, ISelection selection) {
		// TODO Auto-generated method stub
		super.setInput(part, selection);
		localSelection = selection;
		if (selection instanceof IStructuredSelection) {
			Object input = ((IStructuredSelection) selection).getFirstElement();

			Element semanticElement = UMLUtil.resolveUMLElement(input);
			if (semanticElement != null) {
				setEObject(semanticElement);
				commentUtil = new CommentUtil(semanticElement);
			}
		}
	//testeExecuteForum(((Element) getEObject()).getModel().getOwner());
	}
	
	@Override
	public void refresh() {
		super.refresh();
		this.refreshDataField(commentUtil.getComments(0));
	}
	
	private void refreshDataField(EList<Comment> eList) {
		for (Comment co : eList) {
			String aux[] = co.getBody().split("://");
			if (aux[0].equals("anchors") && aux.length == 2) {// TODO criar constante
				refreshAnchor(aux[1]);
			}
		}
	}
	
	private void refreshAnchor(String string) {
		TreeItem[] itens = fTreeAnchor.getItems();
		for (TreeItem ti : itens) {
			ti.dispose();
		}
		String aux[] = string.split(",");
		for (int i = 0; i < aux.length; i++) {
			TreeItem item = new TreeItem(fTreeAnchor, SWT.NONE);
			String aux2[] = aux[i].split("\\.");
			if (aux2.length == 2) {
				String aux3[] = new String[1];
				aux3[0] = aux2[0] + "." + aux2[1];
				item.setText(aux3);
				item.setData(aux3);
			}
			if (aux2.length == 1) {
				String aux3[] = new String[1];
				aux3[0] = aux2[0];
				item.setText(aux3);
				item.setData(aux3);
			}

		}
	}
	@Override
	public final void createControls(Composite parent, TabbedPropertySheetPage aTabbedPropertySheetPage) {
		super.createControls(parent, aTabbedPropertySheetPage);

		Composite container = getWidgetFactory().createFlatFormComposite(parent);
		container.setLayout(new GridLayout(3, false));
		setContainer(container);

		GridData labelParametros = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		labelParametros.heightHint = 50;
		labelParametros.widthHint = 50;
		labelParametros.verticalSpan = 7;

		GridData parametrosData = new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 5);
		parametrosData.heightHint = 50;
		parametrosData.widthHint = 350;
		parametrosData.verticalSpan = 5;

		getWidgetFactory().createCLabel(container, "Anchor:");

		// Image image = new Image(display, "yourFile.gif");
		Button button = new Button(container, SWT.TOGGLE);
		button.setToolTipText("Escolha uma �ncora!");
		button.setImage(new Image(Display.getDefault(), getClass().getResourceAsStream("/icons/Plus.gif")));
		button.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				AnchorDialog pd = new AnchorDialog(container.getShell(), getEObject());
				pd.open();
				Object[] obj = pd.getResult();
				// String[] simpleArray = new String[obj.length];
				//
				// return fdstring.toArray( simpleArray );

				String teste = obj[0].toString();
				if (commentUtil.getCommentType("anchors") != null
						&& commentUtil.getCommentType("anchors").contains(teste)) {
					// Message
					MessageBox messageDialog = new MessageBox(container.getShell(), SWT.ERROR);
					messageDialog.setText("Erro");
					messageDialog.setMessage("�ncora j� inserida!!!");
				    messageDialog.open();
				} else if (teste != null) {
					adicionarAncora(teste);
					button.setEnabled(false);
					// refressar somente os paramentros TODO
					refresh();
					// System.out.println(teste);
				}

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				
			}

			
		});

		buttonExcAnchor = new Button(container, SWT.TOGGLE);
		buttonExcAnchor.setToolTipText("Remova a �ncora!");
		buttonExcAnchor.setImage(new Image(Display.getDefault(), getClass().getResourceAsStream("/icons/remove.gif")));
		buttonExcAnchor.setEnabled(false);
		buttonExcAnchor.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				removerAncora();
				button.setEnabled(true);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

			}
		});

		// parametrosText = getWidgetFactory().createText(container, "");
		// parametrosText.setLayoutData(parametrosData);

		Composite container2 = getWidgetFactory().createFlatFormComposite(container);
		container2.setLayout(new GridLayout(1, false));

		// parametrosText = new Text(container2, SWT.MULTI | SWT.BORDER | SWT.WRAP |
		// SWT.V_SCROLL);
		// parametrosText.setLayoutData(parametrosData);

		fTreeAnchor = new Tree(container2, SWT.H_SCROLL | SWT.BORDER);
		fTreeAnchor.setHeaderVisible(true);
		GridData data = new GridData(GridData.BEGINNING);
		fTreeAnchor.setLayoutData(data);
		data.heightHint = 50;
		// data.widthHint = 200;

		// Turn off drawing to avoid flicker
		fTreeAnchor.setRedraw(false);

		TreeColumn instances = new TreeColumn(fTreeAnchor, SWT.LEFT);
		instances.setText("Anchor");
		instances.setWidth(300);


		// inserirLinhasParametros(fTreeAnchor);

		// fillTree(fTreeAnchor, null, m_cdp.getMainInstance());

		// Turn drawing back on!
		fTreeAnchor.setRedraw(true);

		fTreeAnchor.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (fTreeAnchor.getSelection().length > 0) {
					buttonExcAnchor.setEnabled(true);
				}
			}
		});

		
	}
	
	protected void adicionarAncora(String string) {
		String params = commentUtil.getCommentType("anchors");//
		String aux = "";
		if (params == null || params.equals("")) {
			aux = string;
		} else {
			aux += params + "," + string;
		}

		final String strParam = aux;
		TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(getEObject());
		domain.getCommandStack().execute(new RecordingCommand(domain) {

			@Override
			protected void doExecute() {

				commentUtil.saveCommentModel(strParam, TypeComment.ANCHORS.getValue());//Parametros
			}

			@Override
			protected void postExecute() {
				super.postExecute();
				this.dispose();
			}
		});
	}
	protected void removerAncora() {

		String params = commentUtil.getCommentType("anchors");//
		String[] s = (String[]) ((TreeItem) fTreeAnchor.getSelection()[0]).getData();
		String toRemove = "";
		if(s.length > 1) {
			 toRemove = s[0] + "." + s[1];
		}else {
			
			 toRemove = s[0];
		}
		
		String str = StringUtils.remove(params, toRemove);

		if (str.startsWith(",")) {
			str = str.substring(1);
		}
		if (str.endsWith(",")) {
			str = str.substring(0, str.length() - 1);
		}
		//
		//
		//
		final String strParam = str;
		TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(getEObject());
		domain.getCommandStack().execute(new RecordingCommand(domain) {

			@Override
			protected void doExecute() {

				//saveCommentParametrosModel(strParam);
				commentUtil.saveCommentModel(strParam, TypeComment.ANCHORS.getValue());//Parametros
			}

			@Override
			protected void postExecute() {
				super.postExecute();
				this.dispose();
			}
		});
		((TreeItem) fTreeAnchor.getSelection()[0]).dispose();

	}

}
