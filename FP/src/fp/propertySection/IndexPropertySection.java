package fp.propertySection;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gmf.runtime.notation.datatype.GradientData;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.papyrus.infra.gmfdiag.css.CSSShapeImpl;
import org.eclipse.papyrus.moka.composites.utils.handlers.Utils;
import org.eclipse.papyrus.uml.alf.transaction.commit.ScenarioFactory;
import org.eclipse.papyrus.uml.diagram.common.editparts.ClassEditPart;
import org.eclipse.papyrus.uml.diagram.common.figure.node.ClassFigure;
import org.eclipse.papyrus.uml.extensionpoints.library.IRegisteredLibrary;
import org.eclipse.papyrus.uml.extensionpoints.library.RegisteredLibrary;
import org.eclipse.papyrus.uml.tools.utils.UMLUtil;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.ParameterDirectionKind;
import org.eclipse.uml2.uml.ParameterEffectKind;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.VisibilityKind;

import fp.adapter.GerenteIndexTransition;
import fp.dialog.AttributDialog;
import fp.dialog.ParameterDialog;
import fp.dialog.SortDialog;
import fp.util.CommentUtil;
import fp.util.TypeComment;
import fp.xtext.GerenteCreator;

public class IndexPropertySection extends AbstractPropertySectionOOHDM {

	protected Activity act;
	private static IndexPropertySection ips;

	public IndexPropertySection() {
		
	}
	
	public static IndexPropertySection getInstance(EObject eo) {
		if(ips == null) {
			ips = new IndexPropertySection();
			ips.setEObject(eo);
			return ips;
		}
		ips.setEObject(eo);
		return ips;
	}

	@Override
	public void refresh() {
		super.refresh();
		this.refreshDataField(commentUtil.getComments(0));
	}

	private void refreshDataField(EList<Comment> eList) {
		elementosText.removeModifyListener(elementsListener);
		//if(eList.size() == 0) {
			limparFormulario();
	//	}
		for (Comment co : eList) {
			String aux[] = co.getBody().split("://");
			if (aux[0].equals("parameters")) {// TODO criar constante
				if(aux.length < 2) {
					refreshParametros("");
				}else {
					refreshParametros(aux[1]);
				}
				
				// parametrosText.setText(aux[1]);
			} else if (aux[0].equals("elements")) {// TODO criar constante
				if(aux.length < 2) {
					elementosText.setText("");
				}else {
					elementosText.setText(aux[1]);
				}
				
				
			} else if (aux[0].equals("attributs")) {// TODO criar constante
				if(aux.length < 2) {
					refreshAttributs("");
				}else {
					refreshAttributs(aux[1]);
				}
				
				
			} else if (aux[0].equals("sort") && aux.length == 2) {// TODO criar constante
				refreshSort(aux[1]);
			}
		}
		elementosText.addModifyListener(elementsListener);
	}

	
	private void refreshSort(String string) {
		TreeItem[] itens = fTree2Sort.getItems();
		for (TreeItem ti : itens) {
			ti.dispose();
		}
		String aux[] = string.split(",");
		for (int i = 0; i < aux.length; i++) {
			TreeItem item = new TreeItem(fTree2Sort, SWT.NONE);
			String aux2[] = aux[i].split("\\.");
			if (aux2.length == 3) {
				String aux3[] = new String[2];
				aux3[0] = aux2[0] + "." + aux2[1];
				aux3[1] = aux2[2];
				item.setText(aux3);
				item.setData(aux3);
			}

		}
	}
	private void limparFormulario() {
		TreeItem[] itens = fTree.getItems();
		for (TreeItem ti : itens) {
			ti.dispose();
		}
		elementosText.setText("");
		TreeItem[] itens2 = fTree2.getItems();
		for (TreeItem ti : itens2) {
			ti.dispose();
		}
		TreeItem[] itens3 = fTree2Sort.getItems();
		for (TreeItem ti : itens3) {
			ti.dispose();
		}
		
	}

	private void refreshAttributs(String string) {
		TreeItem[] itens = fTree2.getItems();
		for (TreeItem ti : itens) {
			ti.dispose();
		}
		String aux[] = string.split(",");
		for (int i = 0; i < aux.length; i++) {
			TreeItem item = new TreeItem(fTree2, SWT.NONE);
			String aux2[] = aux[i].split("\\.");
			if (aux2.length == 2) {
				String aux3[] = new String[2];
				aux3[0] = aux2[0] + "." + aux2[1];
				aux3[1] = "";
				item.setText(aux3);
				item.setData(aux3);
			} else if (aux2.length == 3) {
				String aux3[] = new String[2];
				aux3[0] = aux2[0] + "." + aux2[1];
				aux3[1] = aux2[2];
				item.setText(aux3);
				item.setData(aux3);
			} else if (aux2.length == 4) {
				String aux3[] = new String[2];
				aux3[0] = aux2[0] + "." + aux2[1];
				aux3[1] = aux2[2] + "." + aux2[3];
				item.setText(aux3);
				item.setData(aux3);
			}

		}
	}

	@Override
	public final void createControls(Composite parent, TabbedPropertySheetPage aTabbedPropertySheetPage) {
		super.createControls(parent, aTabbedPropertySheetPage);

		Composite container = getWidgetFactory().createFlatFormComposite(parent);
		container.setLayout(new GridLayout(3, false));
		setContainer(container);

		GridData labelParametros = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		labelParametros.heightHint = 50;
		labelParametros.widthHint = 50;
		labelParametros.verticalSpan = 7;

		GridData parametrosData = new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 5);
		parametrosData.heightHint = 50;
		parametrosData.widthHint = 350;
		parametrosData.verticalSpan = 5;

		getWidgetFactory().createCLabel(container, "Par�metros:");

		// Image image = new Image(display, "yourFile.gif");
		Button button = new Button(container, SWT.TOGGLE);
		button.setToolTipText("Escolha um par�metro!");
		button.setImage(new Image(Display.getDefault(), getClass().getResourceAsStream("/icons/Plus.gif")));
		button.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				ParameterDialog pd = new ParameterDialog(container.getShell(), getEObject());
				pd.open();
				Object[] obj = pd.getResult();
				// String[] simpleArray = new String[obj.length];
				//
				// return fdstring.toArray( simpleArray );

				String teste = obj[0].toString();
				if (commentUtil.getCommentType("parameters") != null
						&& commentUtil.getCommentType("parameters").contains(teste)) {
					// Message
					MessageBox messageDialog = new MessageBox(container.getShell(), SWT.ERROR);
					messageDialog.setText("Erro");
					messageDialog.setMessage("Par�metro j� inserido!!!");
				    messageDialog.open();
				} else if (teste != null) {
					adicionarParametro(teste);
					// refressar somente os paramentros TODO
					refresh();
					// System.out.println(teste);
				}

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

			}
		});

		buttonExcParam = new Button(container, SWT.TOGGLE);
		buttonExcParam.setToolTipText("Remova o par�metro!");
		buttonExcParam.setImage(new Image(Display.getDefault(), getClass().getResourceAsStream("/icons/remove.gif")));
		buttonExcParam.setEnabled(false);
		buttonExcParam.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				removerParametro();
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

			}
		});

		// parametrosText = getWidgetFactory().createText(container, "");
		// parametrosText.setLayoutData(parametrosData);

		Composite container2 = getWidgetFactory().createFlatFormComposite(container);
		container2.setLayout(new GridLayout(1, false));

		// parametrosText = new Text(container2, SWT.MULTI | SWT.BORDER | SWT.WRAP |
		// SWT.V_SCROLL);
		// parametrosText.setLayoutData(parametrosData);

		fTree = new Tree(container2, SWT.H_SCROLL | SWT.BORDER);
		fTree.setHeaderVisible(true);
		GridData data = new GridData(GridData.BEGINNING);
		fTree.setLayoutData(data);
		data.heightHint = 50;
		// data.widthHint = 200;

		// Turn off drawing to avoid flicker
		fTree.setRedraw(false);

		TreeColumn instances = new TreeColumn(fTree, SWT.LEFT);
		instances.setText("Classe");
		instances.setWidth(300);
		TreeColumn explicitAlloc = new TreeColumn(fTree, SWT.LEFT);
		explicitAlloc.setText("Par�metro");
		explicitAlloc.setWidth(300);

		// inserirLinhasParametros(fTree);

		// fillTree(fTree, null, m_cdp.getMainInstance());

		// Turn drawing back on!
		fTree.setRedraw(true);

		fTree.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (fTree.getSelection().length > 0) {
					buttonExcParam.setEnabled(true);
				}
			}
		});

		// --------------------------------------------------
		// getWidgetFactory().createCLabel(container, "");
		// getWidgetFactory().createCLabel(container, "");
		// getWidgetFactory().createCLabel(container, "onde est�");

		GridData labelElementos = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1);
		labelElementos.heightHint = 80;
		labelElementos.verticalSpan = 5;
		labelElementos.widthHint = 200;

		GridData elementosData = new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 5);
		elementosData.heightHint = 80;
		elementosData.widthHint = 350;
		elementosData.verticalSpan = 5;

		getWidgetFactory().createCLabel(container2, "Elementos:");
		elementosText = new Text(container2, SWT.MULTI | SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
		elementosText.setLayoutData(elementosData);
		elementosText.addModifyListener(elementsListener);

		Composite container3 = getWidgetFactory().createFlatFormComposite(container2);
		container3.setLayout(new GridLayout(3, false));

		GridData labelAtributos = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		labelAtributos.heightHint = 50;
		labelAtributos.widthHint = 50;
		labelAtributos.verticalSpan = 7;

		GridData atributosData = new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 5);
		atributosData.heightHint = 50;
		atributosData.widthHint = 350;
		atributosData.verticalSpan = 5;

		getWidgetFactory().createCLabel(container3, "Atributos:");

		// Image image = new Image(display, "yourFile.gif");
		Button button2 = new Button(container3, SWT.TOGGLE);
		button2.setToolTipText("Choose the attributs!");
		button2.setImage(new Image(Display.getDefault(), getClass().getResourceAsStream("/icons/Plus.gif")));
		button2.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				AttributDialog pd = new AttributDialog(container3.getShell(), getEObject());
				pd.open();
				Object[] obj = pd.getResult();
				// String[] simpleArray = new String[obj.length];
				//
				// return fdstring.toArray( simpleArray );

				String teste = obj != null ? obj[0].toString() : null;
				
				
				if (teste != null && getCommentAttributs() != null && getCommentAttributs().contains(teste)) {
					// Message
					MessageBox messageDialog = new MessageBox(container3.getShell(), SWT.ERROR);
					messageDialog.setText("Erro");
					messageDialog.setMessage("Atributo j� inserido!!!");
					messageDialog.open();
				} else if (teste != null) {
					adicionarAtributo(teste);
					// refressar somente os atributos TODO
					refresh();
					// System.out.println(teste);
				}

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

			}
		});
		
		

		buttonExcAttr = new Button(container3, SWT.TOGGLE);
		buttonExcAttr.setToolTipText("Remove the attributs!");
		buttonExcAttr.setImage(new Image(Display.getDefault(), getClass().getResourceAsStream("/icons/remove.gif")));
		buttonExcAttr.setEnabled(false);
		buttonExcAttr.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				removerAtributo();

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

			}
		});

		// parametrosText = getWidgetFactory().createText(container, "");
		// parametrosText.setLayoutData(parametrosData);

		Composite container4 = getWidgetFactory().createFlatFormComposite(container3);
		container4.setLayout(new GridLayout(1, false));

		// parametrosText = new Text(container2, SWT.MULTI | SWT.BORDER | SWT.WRAP |
		// SWT.V_SCROLL);
		// parametrosText.setLayoutData(parametrosData);

		fTree2 = new Tree(container4, SWT.H_SCROLL | SWT.BORDER);
		fTree2.setHeaderVisible(true);
		GridData data2 = new GridData(GridData.BEGINNING);
		fTree2.setLayoutData(data2);
		data2.heightHint = 50;
		// data.widthHint = 200;

		// Turn off drawing to avoid flicker
		fTree2.setRedraw(false);

		TreeColumn instances2 = new TreeColumn(fTree2, SWT.LEFT);
		instances2.setText("Atributo");
		instances2.setWidth(300);
		TreeColumn explicitAlloc2 = new TreeColumn(fTree2, SWT.LEFT);
		explicitAlloc2.setText("Destino");
		explicitAlloc2.setWidth(300);

		// inserirLinhasParametros(fTree);

		// fillTree(fTree, null, m_cdp.getMainInstance());

		// Turn drawing back on!
		fTree2.setRedraw(true);

		fTree2.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (fTree2.getSelection().length > 0) {
					buttonExcAttr.setEnabled(true);
				}
			}
		});
		
		
		//mudanca akiii !!!!!!!!111
		
		Composite container5 = getWidgetFactory().createFlatFormComposite(container4);
		container5.setLayout(new GridLayout(3, false));

		GridData labelAtributos1 = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		labelAtributos1.heightHint = 50;
		labelAtributos1.widthHint = 50;
		labelAtributos1.verticalSpan = 7;

		GridData atributosData1 = new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 1, 5);
		atributosData1.heightHint = 50;
		atributosData1.widthHint = 350;
		atributosData1.verticalSpan = 5;

		getWidgetFactory().createCLabel(container5, "Ordena��o:");

		// Image image = new Image(display, "yourFile.gif");
		Button button21 = new Button(container5, SWT.TOGGLE);
		button21.setToolTipText("Choose the sort!");
		button21.setImage(new Image(Display.getDefault(), getClass().getResourceAsStream("/icons/Plus.gif")));
		button21.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				SortDialog pd = new SortDialog(container5.getShell(), getEObject());
				pd.open();
				Object[] obj = pd.getResult();

				String teste = obj != null ? obj[0].toString() : null;
				if (getCommentSort() != null && getCommentSort().contains(teste)) {
					// Message
					MessageBox messageDialog = new MessageBox(container5.getShell(), SWT.ERROR);
					messageDialog.setText("Erro");
					messageDialog.setMessage("Ordena��o j� inserida!!!");
					messageDialog.open();
				} else if (teste != null) {
					addSort(teste);
					refresh();
				}

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

			}
		});

		buttonExcSort = new Button(container5, SWT.TOGGLE);
		buttonExcSort.setToolTipText("Remove the Sort!");
		buttonExcSort.setImage(new Image(Display.getDefault(), getClass().getResourceAsStream("/icons/remove.gif")));
		buttonExcSort.setEnabled(false);
		buttonExcSort.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {

				removeSort();

			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {

			}
		});

		// parametrosText = getWidgetFactory().createText(container, "");
		// parametrosText.setLayoutData(parametrosData);

		Composite container6 = getWidgetFactory().createFlatFormComposite(container5);
		container6.setLayout(new GridLayout(1, false));

		// parametrosText = new Text(container2, SWT.MULTI | SWT.BORDER | SWT.WRAP |
		// SWT.V_SCROLL);
		// parametrosText.setLayoutData(parametrosData);

		fTree2Sort = new Tree(container6, SWT.H_SCROLL | SWT.BORDER);
		fTree2Sort.setHeaderVisible(true);
		GridData data21Sort = new GridData(GridData.BEGINNING);
		fTree2Sort.setLayoutData(data21Sort);
		data21Sort.heightHint = 50;
		// data.widthHint = 200;

		// Turn off drawing to avoid flicker
		fTree2Sort.setRedraw(false);

		TreeColumn instances2Sort = new TreeColumn(fTree2Sort, SWT.LEFT);
		instances2Sort.setText("Atributo");
		instances2Sort.setWidth(300);
		TreeColumn explicitAlloc2Sort = new TreeColumn(fTree2Sort, SWT.LEFT);
		explicitAlloc2Sort.setText("Tipo de ordena��o");
		explicitAlloc2Sort.setWidth(300);

		// inserirLinhasParametros(fTree);

		// fillTree(fTree, null, m_cdp.getMainInstance());

		// Turn drawing back on!
		fTree2Sort.setRedraw(true);

		fTree2Sort.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (fTree2Sort.getSelection().length > 0) {
					buttonExcSort.setEnabled(true);
				}
			}
		});
		
		
		
		Composite container7 = getWidgetFactory().createFlatFormComposite(container6);
		container7.setLayout(new GridLayout(2, false));

		getWidgetFactory().createCLabel(container7, "Salvar:");	
//		this.createPushButton2(container5);
		this.createPushButton(container7);
		
//		textControl = new StyledText(container5, SWT.MULTI | SWT.BORDER
//				| SWT.V_SCROLL | SWT.WRAP);
		//textControl.setVisible(false);
		

//		textControl.setAlwaysShowScrollBars(false);
//		GridDataFactory.fillDefaults().grab(true, true).hint(parent.getSize())
//				.applyTo(textControl);
//		textControl.addExtendedModifyListener(new ExtendedModifyListener() {
//
//			public void modifyText(ExtendedModifyEvent event) {
//				if (isUndo) {
//					undoRedoStack.pushRedo(event);
//				} else { // is Redo or a normal user action
//					undoRedoStack.pushUndo(event);
//					if (!isRedo) {
//						undoRedoStack.clearRedo();
//						// TODO Switch to treat consecutive characters as one event?
//					}
//				}
//			}
//		});

//		textControl.addKeyListener(new KeyAdapter() {
//			@Override
//			public void keyPressed(KeyEvent e) {
//				boolean isCtrl = (e.stateMask & SWT.CTRL) > 0;
//				boolean isAlt = (e.stateMask & SWT.ALT) > 0;
//				if (isCtrl && !isAlt) {
//					boolean isShift = (e.stateMask & SWT.SHIFT) > 0;
//					if (e.keyCode == 'z') {
//						if (isShift) {
//							redo();
//						} else {
//							undo();
//						}
//					}
//				}
//			}
//		});

		

	}
	
	@Override
	public void setInput(IWorkbenchPart part, ISelection selection) {
		// TODO Auto-generated method stub
		super.setInput(part, selection);
		localSelection = selection;
		this.ips = this;
		if (selection instanceof IStructuredSelection) {
			Object input = ((IStructuredSelection) selection).getFirstElement();

			Element semanticElement = UMLUtil.resolveUMLElement(input);
			if (semanticElement != null) {
				setEObject(semanticElement);
				commentUtil = new CommentUtil(semanticElement);
			}
		}
		ClassEditPart cep = (ClassEditPart) ((IStructuredSelection) selection).getFirstElement();
		ClassFigure cf =  (ClassFigure) cep.getPrimaryShape();
		//cf.setGradientData(10265827, -1, 0);
		
		cf.setGradientData(1, 1, 0);
		//CSSShapeImpl css = (CSSShapeImpl) cep.getFigure();
		//css.setGradient(new GradientData(10265827, -1, 0));
	//testeExecuteForum(((Element) getEObject()).getModel().getOwner());
	}

	protected void adicionarAtributo(String string) {
		String params = getCommentAttributs();
		String aux = "";
		if (params == null || params.equals("")) {
			aux = string;
		} else {
			aux += params + "," + string;
		}

		final String strParam = aux;
		TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(getEObject());
		domain.getCommandStack().execute(new RecordingCommand(domain) {

			@Override
			protected void doExecute() {

				saveCommentAtributosModel(strParam);
			}

			@Override
			protected void postExecute() {
				super.postExecute();
				this.dispose();
			}
		});
	}

	protected void removerAtributo() {

		String params = getCommentAttributs();
		String[] s = (String[]) ((TreeItem) fTree2.getSelection()[0]).getData();
		String toRemove = s[0] + "." + s[1];
		String str = StringUtils.remove(params, toRemove);

		if (str.startsWith(",")) {
			str = str.substring(1);
		}
		if (str.endsWith(",")) {
			str = str.substring(0, str.length() - 1);
		}
		if (str.contains(",,")) {
			str = str.replace(",,",",");
		}

		//
		//
		//
		final String strParam = str;
		TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(getEObject());
		domain.getCommandStack().execute(new RecordingCommand(domain) {

			@Override
			protected void doExecute() {

				saveCommentAtributosModel(strParam);
			}

			@Override
			protected void postExecute() {
				super.postExecute();
				this.dispose();
			}
		});
		((TreeItem) fTree2.getSelection()[0]).dispose();

	}
	protected Element createModelTest() {
		TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(getEObject());
		domain.getCommandStack().execute(new RecordingCommand(domain) {

			@Override
			protected void doExecute() {
				
				testeModel = umlUtil.createOrSelectModelElement("Tests", getEObject());// TODO criar contante
				
			}

			@Override
			protected void postExecute() {
				super.postExecute();
				this.dispose();
				System.out.println("passou");
			}
		});
		return testeModel;

	}
	
	protected Element createDaoTest(Class clasRetorno) {
		TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(getEObject());
		domain.getCommandStack().execute(new RecordingCommand(domain) {

			@Override
			
			protected void doExecute() {
				
				
						
				Element daoClass = umlUtil.generateClass(testeModel, clasRetorno.getName()+"Dao");
				//Element daoClassProperty = 
						umlUtil.generateAttributs(umlUtil.generateClass(testeModel, "Dao"), clasRetorno,
						"lista", getEObject(), true);// TODO criar contante
						
			   Element daoOperation = umlUtil.generateOperation(daoClass, clasRetorno, "",
								getEObject(), "getObjects", true);// TODO criar contante
				//??verificar prq o de baixo t� estragando o de cima.
				daoBehavior = umlUtil.generateBehaviorOperation(umlUtil.generateClass(testeModel, clasRetorno.getName()+"Dao"), daoOperation,
						clasRetorno, null, "getObjectsImpl", getEObject(), true);// TODO criar contante
			}

			@Override
			protected void postExecute() {
				super.postExecute();
				this.dispose();
				System.out.println("passou");
			}
		});
		return daoBehavior;

	}
	
	protected Element createBehaviorTest() {
		TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(getEObject());
		domain.getCommandStack().execute(new RecordingCommand(domain) {

			@Override
			protected void doExecute() {
				testBehavior = umlUtil.generateBehaviorModel(testeModel, null, null, "ActivityOOHDMTest",getEObject(),false);// TODO criar
			}

			@Override
			protected void postExecute() {
				super.postExecute();
				this.dispose();
				System.out.println("passou");
			}
		});
		return testBehavior;

	}

public Class findNavigationalClass(String sr) {
		
		String aux = sr;
		Element root = null;
		if(getEObject() instanceof Class) {
			root = ((Class)getEObject()).getModel().getModel().getModel().getOwner();
		}else if(getEObject() instanceof Property) {
			root = ((Property)getEObject()).getModel().getModel().getModel().getOwner();
		}
		 
		for(Element ele :root.getOwnedElements()) {
			if(ele instanceof Model) {
				if(((Model)ele).getName().equals("Navigational")) {
					for(Element ele2 : ((Model)ele).getOwnedElements()) {
						if(ele2 instanceof Class) {
							if(((Class)ele2).getName().equals(aux)) {
								return (Class)ele2;
							}
						}
					}
				}
				
			}
		}
		return null;
	}

public Class findConceptualClass(String sr) {
	
	String aux = sr;
	Element root = null;
	if(getEObject() instanceof Class) {
		root = ((Class)getEObject()).getModel().getModel().getModel().getOwner();
	}else if(getEObject() instanceof Property) {
		root = ((Property)getEObject()).getModel().getModel().getModel().getOwner();
	}
	 
	for(Element ele :root.getOwnedElements()) {
		if(ele instanceof Model) {
			if(((Model)ele).getName().equals("Conceptual")) {
				for(Element ele2 : ((Model)ele).getOwnedElements()) {
					if(ele2 instanceof Class) {
						if(((Class)ele2).getName().equals(aux)) {
							return (Class)ele2;
						}
					}
				}
			}
			
		}
	}
	return null;
}


	protected void specifyCard() {

		TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(getEObject());
		domain.getCommandStack().execute(new RecordingCommand(domain) {

			@Override
			protected void doExecute() {
				System.out.println("inicio execute");
				try {
					fazerValidacao();

					// saveCommentElementosModel();

					importPackageOohdmIfNotApply();
					importPackageAlfIfNotApply();
					
					for (IRegisteredLibrary library : RegisteredLibrary.getRegisteredLibraries()) {
						if (library.getName() != null) {
							if (library.getName().equals("FoundationalModelLibrary")) {
								importPackageFoundationalModelLibraryIfNotApply(library.getUri());
							} else if (library.getName().equals("CollectionClassesImpl")) {
								importPackageCollectionClassesImplIfNotApply(library.getUri());
							} else if (library.getName().equals("PrimitiveTypes")) {
								importPackagePrimitiveTypesIfNotApply(library.getUri());
							} else if (library.getName().equals("EcorePrimitiveTypes")) {
								importPackageEcorePrimitiveTypesIfNotApply(library.getUri());
							} else if (library.getName().equals("AssertionLibrary")) {
								importPackageAssertionLibraryIfNotApply(library.getUri());
							}
						}
					}
					//new org.eclipse.papyrus.uml.alf.Model();
//					importPackageCollectionClassesImplIfNotApply();
//					importPackagePrimitiveTypesIfNotApply();
//					importPackageFoundationalModelLibraryIfNotApply();
//					importPackageEcorePrimitiveTypesIfNotApply();
//					importPackageAssertionLibraryIfNotApply();
					
					Element modelGenerate = getModelGenerated();
					
					//Element modelTest = 
							createModelTest();
					//Element behaviorDaoTest = 
							createDaoTest( findConceptualClass(getGerenteAtual()));
					
					
					classGerente = generateGerenteClass(modelGenerate, getGerenteAtual());
					//String name = ((Class) getEObject()).getName();
					operationGerente = generateGerenteOperation(classGerente,getEObject());

					behaviorOperation = generateBehaviorOperation(classGerente, operationGerente, umlUtil.getClassIndex(getEObject()));// alf

					// behaviorOperation = Utils.getMethod((Class)classGerente,
					// (Operation)operationGerente);

					// String alfCode = buildAlfCodeBehavior(operationGerente); // TODO verificar se
					// a valida��o do alf ser� aqui.
					// System.out.println(alfCode);

					// String alfCode = buildAlfCodeBehavior(operationGerente); // TODO verificar se
					// a valida��o do alf ser� aqui.
					// System.out.println(alfCode);
					//
					// executeAlfCode(behaviorOperation, alfCode);

					// behaviorOperation = setSpecification(classGerente, operationGerente);

					// executeAlfCode(behaviorOperation, alfCode);

					System.out.println("fim execute");
				} catch (Exception e) {
					MessageBox messageDialog = new MessageBox(getContainer().getShell(), SWT.ERROR);
					messageDialog.setText("Erro");
					messageDialog.setMessage("Erro ao compilar c�digo alf!");
					messageDialog.open();
					e.printStackTrace();
				}

			}

			@Override
			protected void postExecute() {
				super.postExecute();
				System.out.println("inicio post execute");
			}

		});

		//
		// GenerateMethodHandler gmh = new GenerateMethodHandler();
		// //gmh.start((Class) classGerente);
		// try {
		// gmh.execute(localSelection);
		// } catch (ExecutionException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// //gmh.getUpdateCommand((Class) classGerente, domain);

	}

	
	
	protected void specifyCard2() {
//
		TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(getEObject());
	domain.getCommandStack().execute(new RecordingCommand(domain) {

			@Override
			protected void doExecute() {
//				System.out.println("inicio  execute2");
				try {

					String alfCode = buildAlfCodeBehavior(operationGerente); // TODO verificar se a valida��o do alf
																				// ser� aqui.
					System.out.println(alfCode);
					
					umlUtil.recursoAlfXtext(behaviorOperation);
					
					umlUtil.executeAlfCode(behaviorOperation, alfCode);
//					umlUtil.executeAlfCode(getEObject(), alfCode);
					
					
								  
//					Job job = new Job("My Job") {
//					    @Override
//					    protected IStatus run(IProgressMonitor monitor) {
//					        // convert to SubMonitor and set total number of work units
//					        SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
//					        subMonitor.setWorkRemaining(70);
//					        UISynchronize sync = new UISynchronize() {
//								
//								@Override
//								public void syncExec(Runnable runnable) {
//									System.out.println("sincronizado");
//									 Element behaviorOperationWithSpecification = setSpecification(classGerente,
//											 operationGerente);
//								}
//								@Override
//								public void asyncExec(Runnable runnable) {
//									System.out.println("a sincronizado");
//									 Element behaviorOperationWithSpecification = setSpecification(classGerente,
//											 operationGerente);
//								}
//							};
//					        return Status.OK_STATUS;
//					    }
//					};
//					job.setPriority(Job.SHORT);
//					job.schedule();
//					

				} catch (Exception e) {
					umlUtil.executeSpecificationJob(domain, classGerente, operationGerente);
					MessageBox messageDialog = new MessageBox(getContainer().getShell(), SWT.ERROR);
					messageDialog.setText("Erro");
					messageDialog.setMessage("Erro ao compilar c�digo alf!");
					messageDialog.open();
					e.printStackTrace();
				}
				
				//DESCOMENTAR
			umlUtil.executeSpecificationJob(domain, classGerente, operationGerente);
				System.out.println("fim execute2");

			}
//
			@Override
			protected void postExecute() {
				super.postExecute();
				System.out.println("inicio post execute2");
			}
//
		});
	


		//
		// TransactionalEditingDomain domain2 =
		// TransactionUtil.getEditingDomain(getEObject());
		// domain2.getCommandStack().execute(new RecordingCommand(domain2) {
		//
		//
		// @Override
		// protected void doExecute() {
		// try {
		//
		// Element behaviorOperationWithSpecification = setSpecification(classGerente,
		// operationGerente);
		//
		//
		// } catch (Exception e) {
		// MessageBox messageDialog = new MessageBox(getContainer().getShell(),
		// SWT.ERROR);
		// messageDialog.setText("Erro");
		// messageDialog.setMessage("Erro ao vincular m�todo!");
		// messageDialog.open();
		// commitButton.setEnabled(true);
		// e.printStackTrace();
		// }
		// }
		//
		// @Override
		// protected void postExecute() {
		// super.postExecute();
		// }
		//
		// });

	}

	protected Element generateGerenteOperation(Element cGerente, EObject eObjectSelected) {

		return umlUtil.generateOperation(cGerente, umlUtil.getClassIndex(getEObject()), commentUtil.getCommentType("parameters"), eObjectSelected,"",false);
	}

	

	protected void fazerValidacao() {
		// TODO Auto-generated method stub
		// tem que ter pelo menos o campo elemento
		// e um atributo com destino.
	}

	protected Element generateGerenteClass(Element mGenerate, String gerenteAtual) {
		String name = "GerenteIndex" + gerenteAtual;// TODO criar constante
		return umlUtil.generateClass(mGenerate, name);

	}

	protected String getGerenteAtual() {

		String aux = getCommentAttributs();

		if (aux.split("\\.").length > 1) {
			return aux.split("\\.")[0];
		}

		return null;
	}

	protected String buildAlfCodeBehavior(Element operationGerente) {
		String in = "";
		if(eObject instanceof Class) {
			in = ((Class) eObject).getName();
		}else if(eObject instanceof org.eclipse.uml2.uml.Property) {
			in = ((Property) eObject).getName();
		}
		
		Class c = ((Class) ((Operation) operationGerente).getOwner());
		Model m = c.getModel();
		
		String namespace = m.getName() + "::"
				+ c.getName();
		String behaviorName = ((Operation) operationGerente).getName() + "Impl";

		GerenteIndexTransition git = new GerenteIndexTransition(namespace, behaviorName, in,
				commentUtil.getCommentType("elements"), getCommentAttributs(), commentUtil.getCommentType("parameters"));

		String alfCode = GerenteCreator.createGerenteIndex(git).toString();

		return alfCode;
	}

	protected void saveCommentAtributosModel(String strParamns) {
		List<Comment> commentList = new ArrayList<Comment>();
		commentUtil.removeComment(TypeComment.ATTRIBUTS.getValue());
		Comment comentA = UMLFactory.eINSTANCE.createComment();

		comentA.setBody(commentUtil.compileComment(strParamns, TypeComment.ATTRIBUTS.getValue()));
		commentList.add(comentA);
		((Element) getEObject()).getOwnedComments().add(comentA);

	}

	protected String getCommentAttributs() {

		for (Comment co : commentUtil.getComments(TypeComment.ATTRIBUTS.getValue())) {

			String aux[] = co.getBody().split("://");
			if (aux[0].equals("attributs")) {// TODO criar constante
				if (aux.length == 2)
					return aux[1];
			}

		}
		return null;

	}

	
	public void testeExecuteForum(EObject eObj) {
		
		//final Activity act = null;
		TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(eObj);
		domain.getCommandStack().execute(new RecordingCommand(domain) {

		

			@Override
			protected void doExecute() {
				
				
				List<Element> elements = new ArrayList<Element>();
				
				//elements.addAll(((Element)eObj).getOwnedElements());
				org.eclipse.uml2.uml.Class cGen = UMLFactory.eINSTANCE.createClass();
				cGen.setName("Test");
				elements.add(cGen);

				eObj.eSet(eObj.eClass().getEStructuralFeature("packagedElement"), elements);
				elements.clear();

				org.eclipse.uml2.uml.Operation opGen = UMLFactory.eINSTANCE.createOperation();
				opGen.setName("operation");
				elements.add(opGen);
				cGen.eSet(cGen.eClass().getEStructuralFeature("ownedOperation"), elements);
				elements.clear();
				
				org.eclipse.uml2.uml.Parameter opOut = UMLFactory.eINSTANCE.createParameter();
				opOut.setName("stringOut");
				opOut.setDirection(ParameterDirectionKind.RETURN_LITERAL);
				opOut.setEffect(ParameterEffectKind.CREATE_LITERAL);
				opOut.setVisibility(VisibilityKind.PUBLIC_LITERAL);
				opOut.setIsUnique(true);
				opOut.setIsException(false);
				opOut.setIsOrdered(false);
				opOut.setIsStream(false);
				opOut.setType(umlUtil.getClassStringUml(eObj));
				opGen.getOwnedParameters().add(opOut);
				
				org.eclipse.uml2.uml.Parameter opIn = UMLFactory.eINSTANCE.createParameter();
				opIn.setName("stringAux");
				opIn.setDirection(ParameterDirectionKind.IN_LITERAL);
				opIn.setEffect(ParameterEffectKind.CREATE_LITERAL);
				opIn.setVisibility(VisibilityKind.PUBLIC_LITERAL);
				opIn.setIsUnique(true);
				opIn.setIsException(false);
				opIn.setIsOrdered(false);
				opIn.setIsStream(false);
				opIn.setType(umlUtil.getClassStringUml(eObj));
				opGen.getOwnedParameters().add(opIn);
				
				org.eclipse.uml2.uml.Parameter opIn2 = UMLFactory.eINSTANCE.createParameter();
				opIn2.setName("stringAux2");
				opIn2.setDirection(ParameterDirectionKind.IN_LITERAL);
				opIn2.setEffect(ParameterEffectKind.CREATE_LITERAL);
				opIn2.setVisibility(VisibilityKind.PUBLIC_LITERAL);
				opIn2.setIsUnique(true);
				opIn2.setIsException(false);
				opIn2.setIsOrdered(false);
				opIn2.setIsStream(false);
				opIn2.setType(umlUtil.getClassStringUml(eObj));
				opGen.getOwnedParameters().add(opIn2);

				act = Utils.getMethod((Class) cGen, (Operation) opGen);
				elements.add(act);
				cGen.eSet(cGen.eClass().getEStructuralFeature("ownedBehavior"), elements);
				
				
				
				
			}

			@Override
			protected void postExecute() {
				super.postExecute();

			}

		});
		
		String alfCode = "namespace Test; "
				+ "private import Alf::Library::BasicInputOutput::WriteLine; "
				+ "activity operationImpl(in stringAux: String, in StringAux2 : String) : String { "
				+ "WriteLine(stringAux); "
				+ "}";
		
		NamedElement semanticObject = ((NamedElement) act);
		if (semanticObject != null && alfCode != null) {
			ScenarioFactory.getInstance().createCommitScenario().execute(semanticObject, alfCode);
		}
	}
	
}
