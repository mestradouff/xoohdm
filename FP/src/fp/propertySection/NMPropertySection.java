package fp.propertySection;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.ui.editparts.DiagramEditPart;
import org.eclipse.gmf.runtime.diagram.ui.requests.DropObjectsRequest;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.papyrus.editor.PapyrusMultiDiagramEditor;
import org.eclipse.papyrus.infra.core.sasheditor.editor.ISashWindowsContainer;
import org.eclipse.papyrus.infra.core.services.ServiceException;
import org.eclipse.papyrus.infra.core.services.ServicesRegistry;
import org.eclipse.papyrus.infra.core.utils.ServiceUtils;
import org.eclipse.papyrus.infra.ui.editor.IMultiDiagramEditor;
import org.eclipse.papyrus.infra.ui.util.EditorUtils;
import org.eclipse.papyrus.uml.diagram.clazz.UmlClassDiagramForMultiEditor;
import org.eclipse.papyrus.uml.tools.utils.UMLUtil;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorRegistry;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.views.properties.tabbed.AbstractPropertySection;
import org.eclipse.ui.views.properties.tabbed.TabbedPropertySheetPage;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;

import fp.dialog.AnchorDialog;
import fp.util.CommentUtil;

public class NMPropertySection extends AbstractPropertySectionOOHDM {

	Tree tree;
	Element clNew;
	Element ppNew;
	List<Property> listProp = new ArrayList<Property>();
	List<Association> listAssoc = new ArrayList<Association>();
	private TabbedPropertySheetPage aTabbedPropertySheetPage;
	 
	public NMPropertySection() {
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void specifyCard() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void specifyCard2() {
		// TODO Auto-generated method stub

	}

	@Override
	public void setInput(IWorkbenchPart part, ISelection selection) {
		// TODO Auto-generated method stub
		super.setInput(part, selection);
		localSelection = selection;
		if (selection instanceof IStructuredSelection) {
			Object input = ((IStructuredSelection) selection).getFirstElement();

			Element semanticElement = UMLUtil.resolveUMLElement(input);
			if (semanticElement != null) {
				setEObject(semanticElement);
				commentUtil = new CommentUtil(semanticElement);
			}
		}
		this.refresh();
	}

	@Override
	public void refresh() {
		super.refresh();
		this.refreshDataField();
	}

	private void refreshDataField() {

		try {
			TreeItem[] itens = tree.getItems();
			for (TreeItem ti : itens) {
				ti.dispose();
			}
			
			if(this.getEObject() instanceof Class) {
				Class cl = (Class) this.getEObject();
				TreeItem item = new TreeItem(tree, SWT.NONE);
				
				item.setText(cl.getName());
//				item.setData(cl);
				item.setChecked(getClassNavigationalExists(cl.getName()));
				item.setExpanded(false);
				int sz = cl.getOwnedElements().size();
				for (int l = 0; l < sz; l++) {
					if(cl.getOwnedElements().get(l) instanceof Property) {
						TreeItem litem = new TreeItem(item, SWT.NONE);
						
						litem.setText(((Property)cl.getOwnedElements().get(l)).getName());
						litem.setChecked(
								getPropertyClassNavigationalExists(cl.getName(), ((Property)cl.getOwnedElements().get(l)).getName()));
						litem.setExpanded(true);
						listProp.add(((Property)cl.getOwnedElements().get(l)));
					}
					
				}
			}else if(this.getEObject() instanceof Association) {
				Association cl = (Association) this.getEObject();
				TreeItem item = new TreeItem(tree, SWT.NONE);
				if(cl.getName() == null || cl.getName().equals("")) {
					String na = cl.getMemberEnds().get(0).getName() +"-"+ cl.getOwnedEnds().get(0).getName();
					item.setText(na);
				}else {
					item.setText(cl.getName());
				}
				//item.setText(cl.getName());
				
				//item.setChecked(getClassNavigationalExists(cl.getName())); //verificar associação existe
				item.setExpanded(false);
				item.setData(cl);
				listAssoc.add(cl);
//				int sz = cl.getOwnedAttributes().size();
//				for (int l = 0; l < sz; l++) {
//					if(cl.getOwnedAttributes().get(l) instanceof Property) {
//						TreeItem litem = new TreeItem(item, SWT.NONE);
//						
//						litem.setText(cl.getOwnedAttributes().get(l).getName());
//						litem.setChecked(
//								getPropertyClassNavigationalExists(cl.getName(), cl.getOwnedAttributes().get(l).getName()));
//						litem.setExpanded(true);
//						listProp.add(cl.getOwnedAttributes().get(l));
//					}
//					
//				}
			}
			
			
			tree.layout();
			tree.layout(true, true);
			tree.getParent().layout();
			tree.getParent().layout(true, true);
			
			aTabbedPropertySheetPage.resizeScrolledComposite();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	private boolean getPropertyClassNavigationalExists(String nameClass, String nameProperty) {
		Model mNav = (Model) umlUtil.createOrSelectModelElement("Navigational", getEObject());

		List<Element> elementos = mNav.getOwnedElements();

		for (Element element : elementos) {
			if (element instanceof Class) {
				Class cl = (Class) element;
				if (cl.getName().equals(nameClass)) {

					for (Element elementProp : cl.getOwnedElements()) {
						if (elementProp instanceof Property) {
							Property pp = (Property) elementProp;
							if (pp.getName().equals(nameProperty)) {
								return true;
							}
						}
					}
				}
			}
		}

		return false;
	}

	@Override
	public final void createControls(Composite parent, TabbedPropertySheetPage aTabbedPropertySheetPage) {
		super.createControls(parent, aTabbedPropertySheetPage);
		this.aTabbedPropertySheetPage = aTabbedPropertySheetPage;
		Composite container = getWidgetFactory().createFlatFormComposite(parent);
		container.setLayout(new GridLayout(1, false));
		setContainer(container);

		getWidgetFactory().createCLabel(container, "Classe Conceitual:");

		GridData layoutTree = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		layoutTree.heightHint = 200;
		layoutTree.widthHint = 200;
		layoutTree.verticalSpan = 7;

		tree = new Tree(container, SWT.CHECK | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);

		tree.setSize(5000, 5000);
		tree.setLayoutData(layoutTree);
		
		tree.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				
				
				TreeItem ti = (TreeItem) e.item;
//				if(ti.getChecked()) {
				
				if(ti.getParentItem() != null) {
					ti.getParentItem().setChecked(true);					
				}
				
				for (TreeItem tiFilhos : ti.getItems()) {
					if(ti.getChecked()) {
						tiFilhos.setChecked(true);
					}else {
						tiFilhos.setChecked(false);
					}
				}

//				}else {
//					ti.getParentItem().setChecked(false);
//				}
				
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		container.layout();
		container.layout(true, true);
		container.getParent().layout();
		container.getParent().layout(true, true);
		// tree.addListener(SWT.Selection, new Listener( )
		// {
		// public void handleEvent(Event event)
		// {
		// if (event.detail == SWT.CHECK)
		// {
		// text.setText(event.item + " was checked.");
		// } else
		// {
		// text.setText(event.item + " was selected");
		// }
		// }
		// });

		Composite container5 = getWidgetFactory().createFlatFormComposite(container);
		container5.setLayout(new GridLayout(2, false));

		getWidgetFactory().createCLabel(container5, "Salvar:");
		Button saveButton = new Button(container5, SWT.PUSH);
		saveButton.setText("Save");
		saveButton.setToolTipText("Save the Navigational Migration in your model");
		saveButton.setImage(new Image(Display.getDefault(), getClass().getResourceAsStream("/icons/validate.gif")));

		saveButton.addSelectionListener(new SelectionListener() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				
				try {
					Element cl = null;
					
					if(getEObject() instanceof Class) {
						for (TreeItem item : tree.getItems()) {
							if(item.getChecked()) {
								cl  = createClassNavigationalIfNotExists(item.getText());
							}
							int cont =0;
							for (TreeItem item2 : item.getItems()) {
								if(item2.getChecked()) {
									createPropriedadeIfNotExists((Class)cl,item2.getText(), listProp.get(cont).getType());
								}

						cont++;
							}
						}
					}else if(getEObject() instanceof Association) {
						cl = (Model) umlUtil.createOrSelectModelElement("Navigational", getEObject());
						 createAssociationIfNotExists(listAssoc.get(0));
					}
					
					IWorkbenchPage page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
					IEditorPart editor = page.getActiveEditor();
					page.saveEditor(editor, false /* confirm */);
					
					dmUtil.openNavigationalDiagram(cl);
					
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				
				
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
	}

	protected void createAssociationIfNotExists(Association association) {
		
		Model mNav = (Model) umlUtil.createOrSelectModelElement("Navigational", getEObject());
	//	Model mNav = association.getOwner().getModel();
		
		List<Element> elementos = mNav.getOwnedElements();
		
		for (Element element : elementos) {
			if (element instanceof Association) {
				Association pp = (Association) element;
				if (pp.getName().equals(association.getName())
						&& pp.getMemberEnds().get(0) == association.getMemberEnds().get(0)
							&& pp.getOwnedEnds().get(0) == association.getOwnedEnds().get(0)) {
					return;				
				}
			}
		}
		
		TransactionalEditingDomain ted = TransactionUtil.getEditingDomain(mNav);
		ted.getCommandStack().execute(new RecordingCommand(ted) {
			

			protected void doExecute() {
				ppNew = umlUtil.generateAssociation(association, getEObject());

			}
		});
	}

	protected void createPropriedadeIfNotExists(Class cl, String nomeProp, Type tp) {
		Model mNav = cl.getOwner().getModel();
		
		List<Property> elementos = cl.getOwnedAttributes();
		
		for (Element element : elementos) {
			if (element instanceof Class) {
				Property pp = (Property) element;
				if (pp.getName().equals(nomeProp)) {
					return;				
				}
			}
		}
		
		TransactionalEditingDomain ted = TransactionUtil.getEditingDomain(mNav);
		ted.getCommandStack().execute(new RecordingCommand(ted) {
			

			protected void doExecute() {
				ppNew = umlUtil.generateAttributs((Element)cl, tp, nomeProp, getEObject(),false);

			}
		});
		
		
//		/*
//		 * Generate the Class diagram containing all the newly created
//		 * interfaces.
//		 */
//		// Get the model package
//		final org.eclipse.uml2.uml.Package modelpackage = (org.eclipse.uml2.uml.Package) mNav;
//				
//		DropObjectsRequest dropObjectsRequest = new DropObjectsRequest();
//		ArrayList list = new ArrayList();
//		list.add((org.eclipse.uml2.uml.Element)ppNew);
//		EList<Element> elementsInModel = modelpackage.allOwnedElements();
//		for (int i=0; i<elementsInModel.size(); i++) {
////			if (elementsInModel.get(i).eClass().getName().compareTo("Interface") == 0)
//			//	list.add((org.eclipse.uml2.uml.Element)elementsInModel.get(i));
//		}
//		dropObjectsRequest.setObjects(list);
//		dropObjectsRequest.setLocation(new Point(20,100));
//		Command commandDrop = getDiagramEditPart().getCommand(dropObjectsRequest);
//		
//		((UmlClassDiagramForMultiEditor) EditorUtils.getMultiDiagramEditor().getActiveEditor()).getDiagram();
//		
//		getDiagramEditPart().getDiagramEditDomain().getDiagramCommandStack().execute(commandDrop);
		
	}

	protected Class createClassNavigationalIfNotExists(String nameClass) {
		Model mNav = (Model) umlUtil.createOrSelectModelElement("Navigational", getEObject());
		List<Element> elementos = mNav.getOwnedElements();

		for (Element element : elementos) {
			if (element instanceof Class) {
				Class cl = (Class) element;
				if (cl.getName().equals(nameClass)) {
					return cl;				
				}
			}
		}
		
		
		TransactionalEditingDomain ted = TransactionUtil.getEditingDomain(mNav);
		ted.getCommandStack().execute(new RecordingCommand(ted) {
			protected void doExecute() {
				clNew = umlUtil.generateClass(mNav, nameClass);

			}
		});

//		//page = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
//		IEditorRegistry desc = PlatformUI.getWorkbench().getEditorRegistry();
//		
////		.getDefaultEditor(file.getName());
////		papyrusEditor = (IMultiDiagramEditor)page.openEditor(new FileEditorInput(file), desc.getId());
//
////		IMultiDiagramEditor papyrusEditor = null;
////		ISashWindowsContainer container = null;
////		try {
////			container = papyrusEditor.getServicesRegistry().getService(ISashWindowsContainer.class);
////		} catch (ServiceException e) {
////			// TODO Auto-generated catch block
////			e.printStackTrace();
////		}
////		container.getActiveSashWindowsPage();
//		
//		/*
//		 * Generate the Class diagram containing all the newly created
//		 * interfaces.
//		 */
//		// Get the model package
//		final org.eclipse.uml2.uml.Package modelpackage = (org.eclipse.uml2.uml.Package) mNav;
//				
//		DropObjectsRequest dropObjectsRequest = new DropObjectsRequest();
//		ArrayList list = new ArrayList();
//		list.add((org.eclipse.uml2.uml.Element)clNew);
//		EList<Element> elementsInModel = modelpackage.allOwnedElements();
//		for (int i=0; i<elementsInModel.size(); i++) {
////			if (elementsInModel.get(i).eClass().getName().compareTo("Interface") == 0)
//				//list.add((org.eclipse.uml2.uml.Element)elementsInModel.get(i));
//		}
//		dropObjectsRequest.setObjects(list);
//		dropObjectsRequest.setLocation(new Point(20,100));
//		Command commandDrop = getDiagramEditPart().getCommand(dropObjectsRequest);
//		
//		((UmlClassDiagramForMultiEditor) EditorUtils.getMultiDiagramEditor().getActiveEditor()).getDiagram();
//		
//		getDiagramEditPart().getDiagramEditDomain().getDiagramCommandStack().execute(commandDrop);
		
		return (Class)clNew;
		// TODO Auto-generated method stub
		
	}
	
	protected DiagramEditPart getDiagramEditPart(){
		/** The papyrus editor. */
		
		PapyrusMultiDiagramEditor papyrusEditor;
		IEditorPart editorPart = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		papyrusEditor =((PapyrusMultiDiagramEditor)editorPart);
		
		
		
		UmlClassDiagramForMultiEditor diagramEditor = (UmlClassDiagramForMultiEditor)papyrusEditor.getActiveEditor();
		
		
		Map m = diagramEditor.getGraphicalViewer().getEditPartRegistry();

		DiagramEditPart clazzdiagrameditPart = (DiagramEditPart)m.get(diagramEditor.getDiagram());
		return clazzdiagrameditPart;

	}

	private boolean getClassNavigationalExists(String nameClass) {
		Model mNav = (Model) umlUtil.createOrSelectModelElement("Navigational", getEObject());

		List<Element> elementos = mNav.getOwnedElements();

		for (Element element : elementos) {
			if (element instanceof Class) {
				Class cl = (Class) element;
				if (cl.getName().equals(nameClass)) {

					return true;
				}
			}
		}

		return false;
	}

}
