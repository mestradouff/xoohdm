package fp.adapter;

import java.util.ArrayList;
import java.util.List;

import fp.util.Parameter;
import fp.util.Sort;

public class GerenteContextTransition {

	
	public GerenteContextTransition(String namespace,String behaviorName, String contextName,String elements, String sort, String parameters, String navigation ) {
		adapter( namespace, behaviorName,  contextName, elements,  sort,  parameters, navigation );
	}

	
	private void adapter(String namespace,String behaviorName, String contextName,String elements, String sort, String parameters,String navigation  ) {
		
		this.behaviorName = behaviorName;
		this.contextName = contextName;
		
		this.namespace = namespace;
		
		this.elements = elements;
		
		this.sort = adapterSort(sort);
		this.parameters = adapterParameters(parameters);
		this.navigation = navigation.toLowerCase();
		
		
	}
	private List<Parameter> adapterParameters(String parameters2) {
		List<Parameter> listRetonro = new ArrayList<Parameter>();
		try {
			
		String[] aux = parameters2.split(",");
		
		
		for(int i = 0;i < aux.length; i++) {
			String[] aux2 = aux[i].split("\\.");
			if(aux2.length != 2) {
				return listRetonro;
			}
			Parameter p = new Parameter();
			p.setParam(aux2[1]);
			
			p.setClassParam(aux2[0]);
			p.setOrder(i);
			listRetonro.add(p);
		}
		
		} catch (Exception e) {
			return listRetonro;
		}
		
		return listRetonro;
	}


	private List<Sort> adapterSort(String attributs2) {
		List<Sort> listRetonro = new ArrayList<Sort>();
		try {
		String[] aux = attributs2.split(",");
		
		for(int i = 0;i < aux.length; i++) {
			String[] aux2 = aux[i].split("\\.");
			Sort p = new Sort();
			String aux3[] = new String[2];
			aux3[0] = /*aux2[0].toLowerCase().substring(0, 1) + "." + */aux2[1];
			this.classOfContext = aux2[0];
			aux3[1] = aux2[2];
			p.setTypeSort(aux3[1]);
			p.setAttr(aux3[0]);
			p.setOrder(i);
			listRetonro.add(p);
		}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return listRetonro;
	}
	private String namespace;
	private String navigation;
	
	private String behaviorName;
	
	private String contextName;
	
	private List<Parameter> parameters;
	
	private String elements;
	
	private List<Sort> sort;
	
	private String classOfContext;

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public String getBehaviorName() {
		return behaviorName;
	}

	public void setBehaviorName(String behaviorName) {
		this.behaviorName = behaviorName;
	}

	

	public String getElements() {
		return elements;
	}

	public void setElements(String elements) {
		this.elements = elements;
	}

	



	public List<Parameter> getParameters() {
		return parameters;
	}


	public void setParameters(List<Parameter> parameters) {
		this.parameters = parameters;
	}


	public String getClassOfContext() {
		return classOfContext;
	}


	public void setClassOfContext(String classOfIndex) {
		this.classOfContext = classOfIndex;
	}


	public String getNavigation() {
		return navigation;
	}


	public void setNavigation(String navigation) {
		this.navigation = navigation;
	}


	public String getContextName() {
		return contextName;
	}


	public void setContextName(String contextName) {
		this.contextName = contextName;
	}


	public List<Sort> getSort() {
		return sort;
	}


	public void setSort(List<Sort> sort) {
		this.sort = sort;
	}

	
	
}
