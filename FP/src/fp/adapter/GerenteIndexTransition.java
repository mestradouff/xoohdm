package fp.adapter;

import java.util.ArrayList;
import java.util.List;

import fp.util.Attribut;
import fp.util.Parameter;

public class GerenteIndexTransition {
	
	public GerenteIndexTransition(String namespace,String behaviorName, String indexName,String elements, String attributs, String parameters ) {
		adapter( namespace, behaviorName,  indexName, elements,  attributs,  parameters );
	}

	
	private void adapter(String namespace,String behaviorName, String indexName,String elements, String attributs, String parameters ) {
		
		this.behaviorName = behaviorName;
		this.indexName = indexName;
		
		this.namespace = namespace;
		
		this.elements = elements;
		
		this.attributs = adapterAttributs(attributs);
		this.parameters = adapterParameters(parameters);
		
		
		
	}
	private List<Parameter> adapterParameters(String parameters2) {
		List<Parameter> listRetonro = new ArrayList<Parameter>();
		try {
			
		String[] aux = parameters2.split(",");
		
		
		for(int i = 0;i < aux.length; i++) {
			String[] aux2 = aux[i].split("\\.");
			
			if(aux2.length != 2) {
				return listRetonro;
			}
			Parameter p = new Parameter();
			p.setParam(aux2[1]);
			
			p.setClassParam(aux2[0]);
			p.setOrder(i);
			listRetonro.add(p);
		}
		
		} catch (Exception e) {
			return listRetonro;
		}
		
		return listRetonro;
	}


	private List<Attribut> adapterAttributs(String attributs2) {
		List<Attribut> listRetonro = new ArrayList<Attribut>();
		try {
		String[] aux = attributs2.split(",");
		
		for(int i = 0;i < aux.length; i++) {
			String[] aux2 = aux[i].split("\\.");
			Attribut p = new Attribut();
			String aux3[] = new String[2];
			aux3[0] = /*aux2[0].toLowerCase().substring(0, 1) + "." + */aux2[1];
			this.classOfIndex = aux2[0];
			if(aux2.length == 4) {//contexto
				aux3[1] = aux2[2] + "." + aux2[3];
			}else if(aux2.length == 3) {//indice
				aux3[1] = aux2[2];
			}else if(aux2.length == 2) {//vazio
				aux3[1] = "";
			}
			
			p.setTarget(aux3[1]);
			p.setAttr(aux3[0]);
			p.setOrder(i);
			listRetonro.add(p);
		}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return listRetonro;
	}
	private String namespace;
	
	private String behaviorName;
	
	private String indexName;
	
	private List<Parameter> parameters;
	
	private String elements;
	
	private List<Attribut> attributs;
	
	private String classOfIndex;

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	public String getBehaviorName() {
		return behaviorName;
	}

	public void setBehaviorName(String behaviorName) {
		this.behaviorName = behaviorName;
	}

	public String getIndexName() {
		return indexName;
	}

	public void setIndexName(String indexName) {
		this.indexName = indexName;
	}

	public String getElements() {
		return elements;
	}

	public void setElements(String elements) {
		this.elements = elements;
	}

	

	public List<Attribut> getAttributs() {
		return attributs;
	}

	public void setAttributs(List<Attribut> attributs) {
		this.attributs = attributs;
	}


	public List<Parameter> getParameters() {
		return parameters;
	}


	public void setParameters(List<Parameter> parameters) {
		this.parameters = parameters;
	}


	public String getClassOfIndex() {
		return classOfIndex;
	}


	public void setClassOfIndex(String classOfIndex) {
		this.classOfIndex = classOfIndex;
	}

	
	
	
}
