package br.ufrj.macae.projetos.xOOHDM.wizard;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.papyrus.uml.diagram.wizards.wizards.CreateModelWizard;
import org.eclipse.ui.IWorkbench;

public class NewModelWizard extends CreateModelWizard {

	/**
	 * @see org.eclipse.papyrus.wizards.CreateModelWizard#init(org.eclipse.ui.IWorkbench,
	 *      org.eclipse.jface.viewers.IStructuredSelection)
	 *
	 * @param workbench
	 * @param selection
	 */

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		super.init(workbench, selection);
		setWindowTitle("New xOOHDM Model");
	}

	
	@Override
	public String getModelKindName() {
		return "OOHDM";
	}
	
	@Override
	protected String[] getSelectedContexts() {
		return new String[] { "br.ufrj.macae.projetos.xOOHDM.architecture.id" };
	}
	
	@Override
	protected String[] getSelectedViewpoints() {
		return new String[] { "br.ufrj.macae.projetos.xOOHDM.architecture.id3" };
	}
	
	
	
	@Override
	public boolean isPapyrusRootWizard() {
		return false;
	}
	
	@Override
	protected void saveDiagramCategorySettings() {
		// do nothing
		// here SysML is the only available category
	}
	

//	@Override
//	protected String[] getSelectedViewpoints() {
//		return new String[] { "br.ufrj.macae.projetos.xOOHDM.architecture.id3" };
//	}
	
//	protected String[] getSelectedViewpoints() {
//		SelectArchitectureContextPage page = getSelectArchitectureContextPage();
//		if (page != null) {
//			return page.getSelectViewpoints();
//		}
//		return null;
//	}
}
