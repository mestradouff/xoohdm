package br.ufrj.macae.projetos.xOOHDM.wizard;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.papyrus.infra.architecture.ArchitectureDescriptionUtils;
import org.eclipse.papyrus.uml.diagram.wizards.wizards.NewPapyrusProjectWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.dialogs.WizardNewProjectCreationPage;


public class NewProjectWizard extends NewPapyrusProjectWizard {

	
	public NewProjectWizard() {
		super();
	}
	/**
	 * @see org.eclipse.papyrus.wizards.NewPapyrusProjectWizard#init(org.eclipse.ui.IWorkbench,
	 *      org.eclipse.jface.viewers.IStructuredSelection)
	 *
	 * @param workbench
	 * @param selection
	 */
	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		super.init(workbench, selection);
		setWindowTitle("New xOOHDM Project");
	//	ArchitectureDescriptionUtils.getSetContextCommand("br.ufrj.macae.projetos.xOOHDM.architecture.id");
	
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected WizardNewProjectCreationPage createNewProjectCreationPage() {
		WizardNewProjectCreationPage newProjectPage = super.createNewProjectCreationPage();
		newProjectPage.setTitle("xOOHDM Project");
		newProjectPage.setDescription("Create a New xOOHDM Project");
		//newProjectPage.
		return newProjectPage;
	}

	
//	protected String[] getDiagramCategoryIds() {
//		return new String[] { "br.ufrj.macae.projetos.xOOHDM.architecture.conceptual" };
//	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void saveDiagramCategorySettings() {
		// do nothing
		// here SysML is the only available category
	}

	/**
	 * {@inheritDoc}
	 */
	

	@Override
	public boolean isPapyrusRootWizard() {
		return false;
	}
	
	@Override
	protected String[] getSelectedContexts() {
		return new String[] { "br.ufrj.macae.projetos.xOOHDM.architecture.id" };
	}
	
	@Override
	protected String[] getSelectedViewpoints() {
		
		return new String[] { "br.ufrj.macae.projetos.xOOHDM.architecture.id3"/*,"br.ufrj.macae.projetos.xOOHDM.architecture.idUMLVP" */};
	}
	
	
	
}
