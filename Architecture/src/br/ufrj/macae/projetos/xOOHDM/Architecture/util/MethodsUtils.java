package br.ufrj.macae.projetos.xOOHDM.Architecture.util;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.infra.core.resource.NotFoundException;
import org.eclipse.papyrus.uml.tools.model.UmlUtils;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.UMLFactory;

public class MethodsUtils {
	public static Element createOrSelectModelElement(String modelName, EObject rootEObject) {
		List<Element> elementos2 = new ArrayList<Element>();
		Element e1;
		List<Element> elementos = null;

		if (((Element) rootEObject).getModel().getOwner() != null) {
			e1 = ((Element) rootEObject).getModel().getOwner();
			elementos = e1.getOwnedElements();

			for (Element element : elementos) {
				if (element.getModel().getName().equals(modelName)) { // TODO criar constante
					if (element instanceof org.eclipse.uml2.uml.Model) {
						return element;
					}
				} else if (element instanceof Model) {
					elementos2.add(element);
				}
			}
		} else if (((Element) rootEObject).getModel().getOwner() == null) {
			e1 = ((Element) rootEObject);
			if (e1.getOwnedElements() != null) {

				elementos = e1.getOwnedElements();
				for (Element element : elementos) {
					if (element.getModel().getName().equals(modelName)) { // TODO criar constante
						if (element instanceof org.eclipse.uml2.uml.Model) {
							return element;
						}
					} else if (element instanceof Model) {
						elementos2.add(element);
					}
				}
			}

		}

		Model mGen = UMLFactory.eINSTANCE.createModel();
		mGen.setName(modelName);

		elementos2.add(mGen);

		Element ele = ((Element) rootEObject);

		ele.eSet(ele.eClass().getEStructuralFeature("packagedElement"), elementos2);

		return mGen;
	}

	public static String getNameRootModel() {
		EObject model = null;
		try {
			model = UmlUtils.getUmlModel().lookupRoot();
			return ((Element) model).getModel().getName();
		} catch (NotFoundException e) {
			e.printStackTrace();
		}

		return "";
	}
}
