package br.ufrj.macae.projetos.xOOHDM.Architecture;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;

import org.eclipse.papyrus.uml.diagram.common.commands.ModelCreationCommandBase;
import org.eclipse.papyrus.uml.tools.utils.PackageUtil;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.UMLFactory;

public class CreateModelCommand extends ModelCreationCommandBase {

	public static final String COMMAND_ID = "br.ufrj.macae.projetos.xOOHDM.wizard.createmodel.command";
	public static final String PROFILES_PATHMAP = "pathmap://resources/oohdm_profile/"; //$NON-NLS-1$
	public static final String PROFILE_URI = PROFILES_PATHMAP + "OOHDM.profile.uml"; //$NON-NLS-1$

	/**
	 * @see org.eclipse.papyrus.core.extension.commands.ModelCreationCommandBase#createRootElement()
	 *
	 * @return
	 */

	@Override
	protected EObject createRootElement() {
		return UMLFactory.eINSTANCE.createModel();
	}

	/**
	 * @see org.eclipse.papyrus.core.extension.commands.ModelCreationCommandBase#initializeModel(org.eclipse.emf.ecore.EObject)
	 *
	 * @param owner
	 */

	@Override
	protected void initializeModel(EObject owner) {
		super.initializeModel(owner);
		try {
			((org.eclipse.uml2.uml.Package) owner).setName(getModelName());

			org.eclipse.uml2.uml.Package profile = PackageUtil.loadPackage(URI.createURI(PROFILE_URI), owner.eResource().getResourceSet());
			if ((profile != null) && (profile instanceof Profile)) {
				PackageUtil.applyProfile(((org.eclipse.uml2.uml.Package) owner), (Profile) profile, true);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * Gets the model name.
	 *
	 * @return the model name
	 */
	protected String getModelName() {
		return "model";
	}

}
