package br.ufrj.macae.projetos.xOOHDM.Architecture;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.gmf.runtime.diagram.core.preferences.PreferencesHint;
import org.eclipse.gmf.runtime.diagram.core.services.ViewService;
import org.eclipse.gmf.runtime.notation.CanonicalStyle;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.NotationPackage;
import org.eclipse.papyrus.infra.gmfdiag.common.AbstractPapyrusGmfCreateDiagramCommandHandler;
import org.eclipse.papyrus.infra.gmfdiag.common.helper.DiagramPrototype;
import org.eclipse.papyrus.infra.gmfdiag.common.utils.DiagramUtils;
import org.eclipse.papyrus.uml.diagram.clazz.edit.parts.ModelEditPart;
import org.eclipse.papyrus.uml.diagram.clazz.part.UMLDiagramEditorPlugin;
import org.eclipse.uml2.uml.Element;

import br.ufrj.macae.projetos.xOOHDM.Architecture.util.MethodsUtils;


public class CreateDiagramNavigationalCommand extends AbstractPapyrusGmfCreateDiagramCommandHandler {

	@Override
	protected String getDiagramNotationID() {
		return ModelEditPart.MODEL_ID;
	}

	@Override
	protected PreferencesHint getPreferenceHint() {
		PreferencesHint adfd = UMLDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT;
		return adfd;
	}

	@Override
	protected String getDefaultDiagramName() {
		return "NavigationalDiagram";
	}
	
	@Override
	protected Diagram doCreateDiagram(Resource diagramResource, EObject owner, EObject element, DiagramPrototype prototype, String name) {
		Element ele = MethodsUtils.createOrSelectModelElement("Navigational", owner);
			
//		Diagram diagram = ViewService.createDiagram(ele, getDiagramNotationID(), getPreferenceHint());
//		if (diagram != null) {
//			diagram.setName(name);
//			diagram.setElement(ele);
//			DiagramUtils.setOwner(diagram, owner);
//			DiagramUtils.setPrototype(diagram, prototype);
//			diagramResource.getContents().add(diagram);
//			initializeDiagram(diagram);
//
//			CanonicalStyle canonicalStyle=(CanonicalStyle)diagram.createStyle(NotationPackage.eINSTANCE.getCanonicalStyle());
//			canonicalStyle.setCanonical(true);
//			
//		}
//		return diagram;
//		
		//return super.doCreateDiagram(diagramResource, ele, ele, prototype, name);
//	}
//
//}
		
		
		return super.doCreateDiagram(diagramResource, ele, ele, prototype, name);
	}

}

