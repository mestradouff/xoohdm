package br.ufrj.macae.projetos.xOOHDM.perspective;

import org.eclipse.papyrus.uml.perspective.PapyrusPerspective;
import org.eclipse.ui.IPageLayout;

public class XOOHDMPerspective extends PapyrusPerspective {

	//protected static final String ID_CHEAT_SHEETS = "org.eclipse.papyrus.robotml.doc.cheatsheet1051923325";

	@Override
	public void createInitialLayout(IPageLayout layout) {
		// TODO Auto-generated method stub
		super.createInitialLayout(layout);
		//new OpenCheatSheetAction(ID_CHEAT_SHEETS).run();
	}

	@Override
	public void defineActions(IPageLayout layout) {
		// TODO Auto-generated method stub
		// super.defineActions(layout);
		layout.addNewWizardShortcut("org.eclipse.papyrus.wizards.createoohdmwizard");
		layout.addNewWizardShortcut("org.eclipse.papyrus.wizards.createoohdmmodel");
		
		layout.addNewWizardShortcut("org.eclipse.ui.wizards.new.folder");
		
		// Add "show views".
		layout.addShowViewShortcut(IPageLayout.ID_PROJECT_EXPLORER);
		layout.addShowViewShortcut(IPageLayout.ID_OUTLINE);
		layout.addShowViewShortcut(IPageLayout.ID_PROP_SHEET);

		
		layout.addActionSet("org.eclipse.debug.ui.launchActionSet");

		// add perspectives
		layout.addPerspectiveShortcut("org.eclipse.ui.resourcePerspective");
		layout.addPerspectiveShortcut("org.eclipse.papyrus.moka.ui.debug.perspectives.moka.debug");
		//layout.addPerspectiveShortcut("org.eclipse.papyrus.infra.core.perspective");

	}

	@Override
	public void defineLayout(IPageLayout layout) {

		// Editors are placed for free.
		String editorArea = layout.getEditorArea();
		layout.addView(IPageLayout.ID_PROJECT_EXPLORER, IPageLayout.LEFT, 0.1f, editorArea);
		layout.addView(ID_MODELEXPLORER, IPageLayout.BOTTOM, 0.1f, IPageLayout.ID_PROJECT_EXPLORER);
		layout.addView(IPageLayout.ID_PROP_SHEET, IPageLayout.BOTTOM, (float) 0.70, editorArea);

		// bottom.addView("org.eclipse.pde.runtime.LogView");
	}

}
