/**
 */
package OohdmProfile;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.uml2.uml.Realization;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Link</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link OohdmProfile.Link#getBase_Realization <em>Base Realization</em>}</li>
 * </ul>
 *
 * @see OohdmProfile.OohdmProfilePackage#getLink()
 * @model
 * @generated
 */
public interface Link extends EObject {
	/**
	 * Returns the value of the '<em><b>Base Realization</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Realization</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Realization</em>' reference.
	 * @see #setBase_Realization(Realization)
	 * @see OohdmProfile.OohdmProfilePackage#getLink_Base_Realization()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Realization getBase_Realization();

	/**
	 * Sets the value of the '{@link OohdmProfile.Link#getBase_Realization <em>Base Realization</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Realization</em>' reference.
	 * @see #getBase_Realization()
	 * @generated
	 */
	void setBase_Realization(Realization value);

} // Link
