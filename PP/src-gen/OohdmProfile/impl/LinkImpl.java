/**
 */
package OohdmProfile.impl;

import OohdmProfile.Link;
import OohdmProfile.OohdmProfilePackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.uml2.uml.Realization;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Link</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link OohdmProfile.impl.LinkImpl#getBase_Realization <em>Base Realization</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LinkImpl extends MinimalEObjectImpl.Container implements Link {
	/**
	 * The cached value of the '{@link #getBase_Realization() <em>Base Realization</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBase_Realization()
	 * @generated
	 * @ordered
	 */
	protected Realization base_Realization;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LinkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OohdmProfilePackage.Literals.LINK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Realization getBase_Realization() {
		if (base_Realization != null && base_Realization.eIsProxy()) {
			InternalEObject oldBase_Realization = (InternalEObject)base_Realization;
			base_Realization = (Realization)eResolveProxy(oldBase_Realization);
			if (base_Realization != oldBase_Realization) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OohdmProfilePackage.LINK__BASE_REALIZATION, oldBase_Realization, base_Realization));
			}
		}
		return base_Realization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Realization basicGetBase_Realization() {
		return base_Realization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBase_Realization(Realization newBase_Realization) {
		Realization oldBase_Realization = base_Realization;
		base_Realization = newBase_Realization;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OohdmProfilePackage.LINK__BASE_REALIZATION, oldBase_Realization, base_Realization));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OohdmProfilePackage.LINK__BASE_REALIZATION:
				if (resolve) return getBase_Realization();
				return basicGetBase_Realization();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OohdmProfilePackage.LINK__BASE_REALIZATION:
				setBase_Realization((Realization)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OohdmProfilePackage.LINK__BASE_REALIZATION:
				setBase_Realization((Realization)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OohdmProfilePackage.LINK__BASE_REALIZATION:
				return base_Realization != null;
		}
		return super.eIsSet(featureID);
	}

} //LinkImpl
