import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.model.LaunchConfigurationDelegate;

public class XOOHDMConfigurationDelegate  extends LaunchConfigurationDelegate{

	
	public static final String CONSOLE_TEXT = "com.vogella.custom.launcher.console.text";
	
	 @Override
	    public void launch(ILaunchConfiguration configuration, String mode, ILaunch launch, IProgressMonitor monitor)
	            throws CoreException {
	        String attribute = configuration.getAttribute(XOOHDMConfigurationDelegate.CONSOLE_TEXT, "Simon says \"RUN!\"");
	        System.out.println(attribute);
	    }
	
}
